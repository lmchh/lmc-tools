package pers.lmc.tools2.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-04-04 16:28
 * @version: 1.0
 */
@SpringBootApplication
@EnableDiscoveryClient
public class Gateway2Application {

    public static void main(String[] args) {
        SpringApplication.run(Gateway2Application.class, args);
    }
}
