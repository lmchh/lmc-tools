package pers.lmc.tools2.gateway.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-04-05 17:36
 * @version: 1.0
 */
@RestController
@RefreshScope
public class TestController {

    @Value("${lmc.test:lmc}")
    private String test;
    @Value("${lmc.public:pub}")
    private String pub;

    @RequestMapping("test")
    public String test() {
        return test;
    }

    @RequestMapping("pub")
    public String pub() {
        return pub;
    }
}
