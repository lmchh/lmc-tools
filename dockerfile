from java:latest
volume /tmp
arg jar_file
copy ${jar_file} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]