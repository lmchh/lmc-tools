# Spring Boot Admin入门

# 一，了解Spring Boot Admin

​	codecentric 的 Spring Boot Admin 是一个社区项目，用于管理和监控Spring Boot 应用程序。应用程序向我们的 Spring Boot Admin Client 注册（通过 HTTP）或使用 Spring Cloud （例如 Eureka、Consul）被发现。UI 只是 Spring Boot Actuator 端点之上的 Vue.js 应用程序

# 二，使用Spring Boot Admin

## 2.1 创建Admin服务端

### 2.1.1 pom.xml

```xml
<dependency>
    <groupId>de.codecentric</groupId>
    <artifactId>spring-boot-admin-starter-server</artifactId>
    <version>2.4.5</version>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

### 2.1.2 application.yml

```yml
server:
  port: 8762
spring:
  application:
    name: tools-admin
```

### 2.1.3 启动类

```java
package com.lmc.admin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-03-13 19:19
 * @version: 1.0
 */
@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@EnableAdminServer
public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }
}

```

配置完毕后启动项目， 访问http:localhost:8762，可以看到Spring Boot Admin管理页面。

## 2.2 客户端配置

以下，我使用springcloud config服务作为spring boot admin客户端来配置

### 2.2.1 pom.xml

```xml
		<dependency>
            <groupId>de.codecentric</groupId>
            <artifactId>spring-boot-admin-client</artifactId>
            <version>2.4.4</version>
        </dependency>
```

### 2.2.2 application.yml

```yml
spring:
  boot:
    admin:
      client:
        url: http://localhost:8762

management:
  endpoints:
    web:
      exposure:
        include: "*"
```

### 2.2.3 启动类

启动类没有做任何修改

```java
package com.lmc.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-10-12 23:14
 * @version: 1.0
 */
@SpringBootApplication
@EnableConfigServer
@EnableEurekaClient
public class ConfigServerApplication3344 {
    public static void main(String[] args) {
        SpringApplication.run(ConfigServerApplication3344.class, args);
    }
}

```

配置完毕后启动服务，继续访问 http://localhost:8762， 查看Spring Boot Admin管理页面页面，可以看到tools-config已经存在。

具体代码请参考 https://gitee.com/lmchh/lmc-tools 的tools-admin模块和tools-config模块