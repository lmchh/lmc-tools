package com.lmc.echart.common;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author lmc
 * @Description: TODO Echart返回结果
 * @Create 2021-06-24 19:57
 * @version: 1.0
 */
@Data
@AllArgsConstructor
public class EchartResult {

    private int code;

    private Object data;



}
