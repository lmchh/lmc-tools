package com.lmc.echart.common;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-25 22:08
 * @version: 1.0
 */
@Data
@AllArgsConstructor
public class EchartPaths {

    private int code;

    private List<EchartPath> data;

}
