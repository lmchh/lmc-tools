package com.lmc.echart.common;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author lmc
 * @Description: TODO echart数据源路径
 * @Create 2021-06-25 22:10
 * @version: 1.0
 */
@Data
@AllArgsConstructor
public class EchartPath {

    private String url;

    private String title;

    private List<String> params;


}
