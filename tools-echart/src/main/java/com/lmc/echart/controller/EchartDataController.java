package com.lmc.echart.controller;

import com.lmc.echart.common.EchartPath;
import com.lmc.echart.common.EchartPaths;
import com.lmc.echart.common.EchartResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-24 19:53
 * @version: 1.0
 */
@RestController
@RequestMapping("magicalcoder/echart")
public class EchartDataController {

    @RequestMapping("data01")
    public EchartResult data01() {
        List<EchartData01> data = new ArrayList<>();
        data.add(new EchartData01("lmc", 22));
        data.add(new EchartData01("lmchh", 24));
        data.add(new EchartData01("hh", 23));
        data.add(new EchartData01("sb", 17));
        return new EchartResult(0, data);
    }

    @RequestMapping("data02")
    public EchartResult data02() {
        List<EchartData02> data = new ArrayList<>();
        data.add(new EchartData02("Chinese", 109, 105));
        data.add(new EchartData02("Math", 89, 100));
        data.add(new EchartData02("English", 102, 100));
        data.add(new EchartData02("Other", 168.5, 190));
        return new EchartResult(0, data);
    }

    @RequestMapping("data03")
    public EchartResult data03() {
        List<EchartData01> data = new ArrayList<>();
        Random random = new Random();
        data.add(new EchartData01("lmc", random.nextInt(20)));
        data.add(new EchartData01("lmchh", random.nextInt(20)));
        data.add(new EchartData01("hh", random.nextInt(20)));
        data.add(new EchartData01("sb", random.nextInt(20)));
        return new EchartResult(0, data);
    }

    @RequestMapping("data04")
    public EchartResult data04(String startTime, String endTime, String product, String station) {
        List<EchartData01> data = new ArrayList<>();
        if (StringUtils.isEmpty(startTime) || StringUtils.isEmpty(endTime) || StringUtils.isEmpty(product) || StringUtils.isEmpty(station)) {
            return new EchartResult(-1, null);
        }
        data.add(new EchartData01(startTime, 22));
        data.add(new EchartData01(product, 21));
        data.add(new EchartData01(endTime, 23));
        data.add(new EchartData01(station, 20));
        return new EchartResult(0, data);
    }

    @RequestMapping("dataPath")
    public EchartPaths dataPath() {
        List<EchartPath> echartPathList = new ArrayList<>();
        echartPathList.add(new EchartPath("echart/data01", "后台数据01", null));
        echartPathList.add(new EchartPath("echart/data02", "后台数据02", null));
        echartPathList.add(new EchartPath("echart/data03", "后台数据03", null));
        echartPathList.add(new EchartPath("echart/data04", "后台数据04", Arrays.asList("startTime", "endTime", "product", "station")));
        return new EchartPaths(0, echartPathList);
    }

}
@Data
@AllArgsConstructor
class EchartData01 {
    private String name;
    private int age;
}

@Data
@AllArgsConstructor
class EchartData02 {
    private String course;
    private double score;
    private double expect;
}
