package com.lmc.echart.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-22 23:16
 * @version: 1.0
 */
@Controller
@RequestMapping("view/echart")
public class ViewController {

    @RequestMapping("index")
    public String index() {
        return "index";
    }

    @RequestMapping("side")
    public String side() {
        return "magicalcoder/iframe-layui-2.5.4";
    }

    @RequestMapping("active")
    @ResponseBody
    public String active() {
        return "2363006100061026302250023084846361250006184420024230";
    }

}
