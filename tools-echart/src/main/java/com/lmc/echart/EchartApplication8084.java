package com.lmc.echart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-22 23:17
 * @version: 1.0
 */
@SpringBootApplication
public class EchartApplication8084 {

    public static void main(String[] args) {
        SpringApplication.run(EchartApplication8084.class, args);
    }

}
