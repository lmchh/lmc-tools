/*test组件:MagicalCoder参考lib\mc\magicalcoder.js*/
MagicalCoder.install({
    /*左侧可拖拽的源*/
    dragItems:[
        {
            name:"热力图",
            icon:"ri-pie-chart-2-line",
            search:true,
            children:[
                {
                    name:"热力图",
                    children:[
                        {
                            name:"笛卡尔坐标系上的热力图",
                            icon:"ri-pie-chart-2-line",
                            html:"<div class='mc-echarts-heatmap-cartesian' style='width:60%;height:400px;'></div>"
                        }
                    ]
                }

            ]
        }
   ],
    /*自定义组件和属性*/
    components:[
        {
            identifier:"mc-echarts-heatmap-cartesian",
            properties:{
                name:"笛卡尔坐标系上的热力图",
                dragInto:false,
                duplicate:true,
                assistDuplicate:true,
                copy:true,
                paste:false,
                canDelete:true,
                assistDelete:true,
                afterDragDrop:{refresh:true},
                afterResize:{refresh:true}
            },
            attributes:[
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {
                            type:"html",
                            category:"数据",
                            callback:{
                                htmlCallback:function(param){
                                    return ''+
                                        '<div class="layui-row layui-col-space3" id="mcEchartsDataManagerApp">'+
                                            '<template>'+
                                            '<div class="layui-col-xs12">'+
                                                '<el-row>'+
                                                    '<el-col :span="24">'+
                                                        '<el-radio-group v-model="echarts.dataManager.dataType"  size="mini" style="margin-bottom:10px">'+
                                                          '<el-radio-button :label="1">已知数据源</el-radio-button>'+
                                                          '<el-radio-button :label="2">未知数据源</el-radio-button>'+
                                                          '<el-radio-button :label="3">自定义数据</el-radio-button>'+
                                                        '</el-radio-group>'+
                                                         '<el-form v-if="echarts.dataManager.dataType==1" label-position="left" size="mini" label-width="60px">'+
                                                              '<el-form-item label="数据源">'+
                                                                '<el-select v-model="echarts.dataManager.choose1.sourceApi" @change="changeSourceApi">'+
                                                                    '<el-option v-for="(item,idx) in sourceApiList" :key="item.title" :label="item.title" :value="item.url"></el-option>'+
                                                                '</el-select>'+
                                                            '</el-form-item>'+
                                                            '<el-form-item label="X轴">'+
                                                                '<el-select v-model="echarts.dataManager.choose1.x">'+
                                                                      '<el-option v-for="(item,idx) in xList" :key="item.name" :label="item.title" :value="item.name"></el-option>'+
                                                                '</el-select>'+
                                                            '</el-form-item>'+
                                                            '<el-form-item label="Y轴">'+
                                                                '<el-select v-model="echarts.dataManager.choose1.y">'+
                                                                    '<el-option v-for="(item,idx) in yList" :key="item.name" :label="item.title" :value="item.name"></el-option>'+
                                                                '</el-select>'+
                                                            '</el-form-item>'+
                                                            '<el-form-item label="坐标值">'+
                                                                '<el-select v-model="echarts.dataManager.choose1.z">'+
                                                                    '<el-option v-for="(item,idx) in zList" :key="item.name" :label="item.title" :value="item.name"></el-option>'+
                                                                '</el-select>'+
                                                            '</el-form-item>'+
                                                        '</el-form>'+
                                                        '<el-form v-if="echarts.dataManager.dataType==2" label-position="left" size="mini" label-width="60px">'+
                                                            '<el-form-item label="数据源">'+
                                                                '<el-input v-model="echarts.dataManager.choose2.sourceApi"></el-input>'+
                                                            '</el-form-item>'+
                                                            '<el-form-item label="X轴">'+
                                                                '<el-input v-model="echarts.dataManager.choose2.x"></el-input>'+
                                                            '</el-form-item>'+
                                                            '<el-form-item label="Y轴">'+
                                                                '<el-input v-model="echarts.dataManager.choose2.y"></el-input>'+
                                                            '</el-form-item>'+
                                                            '<el-form-item label="坐标值">'+
                                                                '<el-input v-model="echarts.dataManager.choose2.z"></el-input>'+
                                                            '</el-form-item>'+
                                                        '</el-form>'+
                                                        '<el-form v-if="echarts.dataManager.dataType==3" label-position="left" size="mini" label-width="0px">'+
                                                            '<el-form-item>'+
                                                                '<mc-static-edit-table :fit="true" :show-header="true" border :row-size="rowSizeX" :data="choose3DataX">'+
                                                                     '<mc-static-edit-table-column prop="x" label="X轴" readonly="false" edit-type="editable-textarea"></mc-static-edit-table-column>'+
                                                                     '<!--<mc-static-edit-table-column prop="y" label="Y轴" readonly="false" edit-type="editable-textarea"></mc-static-edit-table-column>-->'+
''+
                                                                     '<el-table-column label="操作">'+
                                                                            '<template slot-scope="scope">'+
                                                                                '<el-button mc-type="column-el-button" size="mini" type="danger" @click="deleteRowX(scope.$index, scope.row)">删除</el-button>'+
                                                                            '</template>'+
                                                                     '</el-table-column>'+
                                                                '</mc-static-edit-table>'+
                                                            '</el-form-item>'+
''+
                                                             '<el-form-item>'+
                                                                '<el-button-group>'+
                                                                    '<el-button @click="addRowX" size="mini">增加一行X轴</el-button>'+
''+
                                                                    '<!--<el-button @click="saveDataY" size="mini">保存</el-button>-->'+
                                                                '</el-button-group>'+
                                                            '</el-form-item>'+
''+
                                                            '<el-form-item>'+
                                                                '<mc-static-edit-table :fit="true" :show-header="true" border :row-size="rowSizeY" :data="choose3DataY">'+
                                                                     '<!--<mc-static-edit-table-column prop="x" label="X轴" readonly="false" edit-type="editable-textarea"></mc-static-edit-table-column>-->'+
                                                                     '<mc-static-edit-table-column prop="y" label="Y轴" readonly="false" edit-type="editable-textarea"></mc-static-edit-table-column>'+
''+
                                                                     '<el-table-column label="操作">'+
                                                                            '<template slot-scope="scope">'+
                                                                                '<el-button mc-type="column-el-button" size="mini" type="danger" @click="deleteRowY(scope.$index, scope.row)">删除</el-button>'+
                                                                            '</template>'+
                                                                     '</el-table-column>'+
                                                                '</mc-static-edit-table>'+
                                                            '</el-form-item>'+
''+
                                                             '<el-form-item>'+
                                                                '<el-button-group>'+
                                                                    '<el-button @click="addRowY" size="mini">增加一行Y轴</el-button>'+
''+
                                                                    '<!--<el-button @click="saveData" size="mini">保存</el-button>-->'+
                                                                '</el-button-group>'+
                                                            '</el-form-item>'+
''+
                                                            '<el-form-item>'+
                                                                '<mc-static-edit-table :fit="true" :show-header="true" border :row-size="rowSizeZ" :data="choose3DataZ">'+
                                                                     '<mc-static-edit-table-column prop="x" label="X轴坐标" readonly="false" edit-type="editable-textarea"></mc-static-edit-table-column>'+
                                                                     '<mc-static-edit-table-column prop="y" label="Y轴坐标" readonly="false" edit-type="editable-textarea"></mc-static-edit-table-column>'+
                                                                     '<mc-static-edit-table-column prop="z" label="坐标值" readonly="false" edit-type="editable-textarea"></mc-static-edit-table-column>'+
                                                                     '<el-table-column label="操作">'+
                                                                            '<template slot-scope="scope">'+
                                                                                '<el-button mc-type="column-el-button" size="mini" type="danger" @click="deleteRowZ(scope.$index, scope.row)">删除</el-button>'+
                                                                            '</template>'+
                                                                     '</el-table-column>'+
                                                                '</mc-static-edit-table>'+
                                                            '</el-form-item>'+
                                                            '<el-form-item>'+
                                                                '<el-button-group>'+
                                                                    '<el-button @click="addRowZ" size="mini">增加一行坐标值</el-button>'+
''+
                                                                    '<el-button @click="saveData" size="mini">保存</el-button>'+
                                                                '</el-button-group>'+
                                                            '</el-form-item>'+
                                                        '</el-form>'+
                                                    '</el-col>'+
                                                '</el-row>'+
                                            '</div>'+
                                            '</template>'+
                                        '</div>'+
'';
                                },
                                render:function (param) {
                                    setTimeout(function(){
                                        //从magicalCoder获取当前聚焦节点的属性值 准备传入vue里
                                        var config = param.config,focusNode=param.focusNode;
                                        var mcEchartsAttr = focusNode.attributes['mc-echarts-data-manager'];
                                        var rowSize = 0,rowSizeX = 0,rowSizeY=0,rowSizeZ=0,choose3Data=[],choose3DataX=[],choose3DataY=[],choose3DataZ=[];
                                        if(!mcEchartsAttr){
                                            mcEchartsAttr = {
                                                 dataManager:{
                                                    dataType:1,
                                                    choose1:{x:"",y:"",z:"",sourceApi:""},
                                                    choose2:{x:"",y:"",z:"",sourceApi:""},
                                                    choose3:{data:[{x:[],y:[],z:[]}]}
                                                 }
                                            }
                                        }else{
                                            mcEchartsAttr = {dataManager:JSON.parse(mcEchartsAttr.replace(/&quot;/g,"\"").replace(/&lt;/g,"<").replace(/&gt;/g,">"))};
                                            rowSize = mcEchartsAttr.dataManager.choose3.data.length;
                                            if(mcEchartsAttr.dataManager.choose3.data.x) {
                                                rowSizeX=mcEchartsAttr.dataManager.choose3.data.x.length;
                                                choose3DataX = mcEchartsAttr.dataManager.choose3.data.x;
                                            }
                                            if(mcEchartsAttr.dataManager.choose3.data.y) {
                                                rowSizeY=mcEchartsAttr.dataManager.choose3.data.y.length;
                                                choose3DataY = mcEchartsAttr.dataManager.choose3.data.y;
                                            }
                                            if(mcEchartsAttr.dataManager.choose3.data.z) {
                                                rowSizeZ=mcEchartsAttr.dataManager.choose3.data.z.length;
                                                choose3DataZ = mcEchartsAttr.dataManager.choose3.data.z;
                                            }
                                            // debugger;
                                            //rowSizeZ = mcEchartsAttr.dataManager.choose3.dataZ.length;
                                            choose3Data = JSON.parse(JSON.stringify(mcEchartsAttr.dataManager.choose3.data));
                                            
                                        }
                                        new Vue({
                                                el: '#mcEchartsDataManagerApp',
                                                data: {
                                                    echarts: mcEchartsAttr,
                                                    sourceApiList:[],
                                                    sourceApiMap :{},
                                                    xList:[],
                                                    yList:[],
                                                    zList:[],
                                                    rowSize:rowSize,
                                                    rowSizeX:rowSizeX,
                                                    rowSizeY:rowSizeY,
                                                    rowSizeZ:rowSizeZ,
                                                    choose3Data:choose3Data,
                                                    choose3DataX:choose3DataX,
                                                    choose3DataY:choose3DataY,
                                                    choose3DataZ:choose3DataZ
                                                },
                                                watch:{
                                                    echarts:{
                                                        handler: function(newVal, oldVal) {
                                                            //监听到属性发生变化 那么修改属性 同时触发渲染
                                                            var triggerChange = false;
                                                            if(this.echarts.dataManager.dataType==1){
                                                                triggerChange = this.echarts.dataManager.choose1.sourceApi && this.echarts.dataManager.choose1.x && this.echarts.dataManager.choose1.y &&  this.echarts.dataManager.choose1.z;
                                                            }else if(this.echarts.dataManager.dataType==2){
                                                                triggerChange = this.echarts.dataManager.choose2.sourceApi && this.echarts.dataManager.choose2.x && this.echarts.dataManager.choose2.y && this.echarts.dataManager.choose2.z;
                                                            }else if(this.echarts.dataManager.dataType==3){
                                                                triggerChange = true;
                                                            }
                                                            MAGICAL_CODER_API.changeAttr({node:focusNode,name:"mc-echarts-data-manager",value:JSON.stringify(newVal.dataManager),triggerChange:triggerChange})
                                                        },
                                                        deep: true
                                                    }
                                                },
                                                methods:{
                                                    changeSourceApi:function(value){
                                                        this.echarts.dataManager.choose1.x="";
                                                        this.echarts.dataManager.choose1.y="";
                                                        this.echarts.dataManager.choose1.z="";
                                                        this.xList = this.sourceApiMap[value].x;
                                                        this.yList = this.sourceApiMap[value].y;
                                                        this.zList = this.sourceApiMap[value].z;
                                                    },
                                                    addRow:function(){
                                                        this.rowSize++;
                                                    },
                                                    deleteRow:function(idx,rowData){
                                                        this.choose3Data.splice(idx,1)
                                                        this.rowSize--;
                                                    },
                                                    saveData:function(){
                                                        
                                                        var data3 = {x:this.choose3DataX,y:this.choose3DataY,z:this.choose3DataZ}
                                                        //同步一下
                                                        // this.echarts.dataManager.choose3.data = JSON.parse(JSON.stringify([data3]));
                                                        this.echarts.dataManager.choose3.data = JSON.parse(JSON.stringify(data3));
                                                        
                                                    },
                                                    addRowX:function(){
                                                        this.rowSizeX++;
                                                    },
                                                    deleteRowX:function(idx,rowData){
                                                        // debugger;
                                                        this.choose3DataX.splice(idx,1)
                                                        this.rowSizeX--;
                                                    },
                                                    addRowY:function(){
                                                        this.rowSizeY++;
                                                    },
                                                    deleteRowY:function(idx,rowData){
                                                        // debugger;
                                                        this.choose3DataY.splice(idx,1)
                                                        this.rowSizeY--;
                                                    },
                                                    addRowZ:function(){
                                                        this.rowSizeZ++;
                                                    },
                                                    deleteRowZ:function(idx,rowData){
                                                        // debugger;
                                                        this.choose3DataZ.splice(idx,1)
                                                        this.rowSizeZ--;
                                                    }
                                                },
                                                mounted:function(){
                                                    var _t = this;
                                                    // 这里边现在获取的还是 xy下边的数据 本地json数据
                                                    MagicalCoder.ajax(true,{},"assets/drag/js/data/echarts/cartesian/api.json",function(originParam,data){
                                                        if(data.code==0){
                                                            var list = data.data;
                                                            for(var i=0;i<list.length;i++){
                                                                var item = list[i];
                                                                _t.sourceApiList.push(item);
                                                                _t.sourceApiMap[item.url] = item.fields;
                                                            }
                                                            var sourceApi = _t.echarts.dataManager.choose1.sourceApi;
                                                            if(sourceApi){
                                                                _t.xList = _t.sourceApiMap[sourceApi].x;
                                                                _t.yList = _t.sourceApiMap[sourceApi].y;
                                                                _t.zList = _t.sourceApiMap[sourceApi].z;
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                    },5)
                                }
                            }
                        },
                    ]
                }
            ]
        }
    ]
});
