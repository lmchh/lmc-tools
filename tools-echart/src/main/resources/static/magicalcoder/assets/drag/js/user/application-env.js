/*环境变量 所有变化的东西 统一放在此处*/
function ApplicationEnv(){
    this.env = {
        version:"3.1.0",
        security : {
            /*软件运行允许的域名对：防破解 具体配置在application-env.js*/
            keys:[
                 {
                     domain: "127.0.0.1",
                     secret: "5208868604686464623632344010886"
                 },{
                     //允许的域名
                     domain:"localhost",
                     //发放的加密串 请咨询www.magicalcoder.com
                     secret:"52088686046864646272304442084",
                 },
                 {
                     //允许的域名
                     domain:".magicalcoder.com",
                     //发放的加密串 请咨询www.magicalcoder.com
                     secret:"00886465246864672840638602123004002352084",
                 }
             ],
            /*是否允许调试：false 加强破解难度*/
            debug:true,
            /*是否允许控制台输出：false 加强破解难度*/
            console:true,
            /*请求解密前 可以自己构造header*/
            buildHeader:function(){return {}},
            /*防别人下载后直接运行客户端 此地址是解析加密算法的地址 */
            activeUrl:"/drag_rest/active",
            /*请随机给36位长度的纯字母数字组合*/
            random:"923456789012345678901234567890123459",
            /*加密解析的返回结果您可以再处理一番*/
            parseActiveCallback:function(result){
                return result;
            },
            //ip不校验 直接通过 不配置则不通过
            ipPass:true,
            /*过期时间 如果不设置则永久不过期 跟您申请的key是绑定的:配置示例2020/06/01*/
            expireDate:""
        },
        /*constant.js有很多通用的 常量配置 统一提前到这里*/
        constant:{
            settings:{
                /*导航树*/
                navigateTree:{
                    /*是否启用*/
                    enable:true,
                },
                /*样式工具箱*/
                styleTool:{
                    /*是否启用*/
                    enable:true,
                    /*每当拖拽修改时 是否强制更新*/
                    forceUpdateWhileDrag:false
                },
                /*文件上传*/
                file:{
                    /*上传地址*/
                    action:"/web/common_file_rest/upload",
                    name:"file",
                    header:{},
                    /*返回数据处理*/
                    callback:function (res) {
                        if(res.flag){
                            var url = res.data.src;
                            if(url.startsWith("http")){
                                return url;
                            }
                            return "/"+url;
                        }
                        return "";
                    }
                },
                /*javascript脚本 <script>标签是否开启 尽量别开format:true 可能有点问题*/
                javascript:{
                    enable:true,
                    format:false
                },
                /*html <html>标签是否开启*/
                html:{
                    enable:true,
                    format:true,
                    /*html里的注释标签 您想要在此处声明*/
                    commentList:[{start:"<!--",end:"-->"},{start:"<%",end:"%>"}],
                    /*标签名称正则：如果您发现自己的标签名总是被隔断 可以试试这里兼容此标签名*/
                    tagNameReg:'([a-zA-Z_][\\w\\-\\.\\:]*)'
                },
                /*css样式*/
                css:{
                    enable:true,
                    format:true
                },
                /*代码调试 采用打开新连接方式 输出一个新页面，把用户的html放在新页面 这样就可以利用浏览器的调试功能 */
                debug:{
                    //必须是post接口 请自行接收head body参数 然后redirect到一个新页面 处理 切记将"&lt;"转成"<" 因为浏览器检查发现脚本就报xss攻击
                    // action:"http://localhost/debug_form",
                    action:"/debug_form",
                    notify:true
                },
                /*缓存 比如刷新浏览器 可以自动恢复*/
                cache:{
                    enable:false,/*是否启用本地缓存 注意初始化后会自动读取本地浏览器缓存数据 初始化到布局器*/
                    /*存储数据到本地缓存位置*/
                    storeLocation:'localStorage',//localStorage(刷新或关闭重启浏览器可恢复) sessionStorage(仅刷新可恢复)
                },
                //布局器右上角有个预览按钮 在弹窗打开 预览一下实际情况 因不同ui依赖资源不同 所以采用配置方式来加载资源
                view:{
                    //预览地址
                    url:"view.html",
                    //layui弹窗配置 area[0]宽如果不写则自动根据当前选中的设备模式宽 area[1]高
                    layerExtra:{title:"预览",area: [, '584px']},
                },
                workspace:{
                    /*iframe 根节点html 如果您希望初始初始状态就使用自由定位 可以配置成 <div class='mc-root mc-ui-absolute-pane' id='magicalDragScene'></div>*/
                    rootHtml:"<div class='mc-root mc-ui-absolute-pane' id='magicalDragScene'></div>",
                    /*中间拖拽线*/
                    dragLineWucha:{
                        /*当间距在误差范围内,自动显示对齐线 误差越大 越容易出现对齐线*/
                        align:1
                    }
                },
                other:{
                    top:{
                        /*视图模式*/
                        mode:{
                            /*拖拽宽高是否开启*/
                            resize:true,
                            /*是否可拖到元素*/
                            drag:true
                        },
                        /*预览模式*/
                        review:{
                            /*拖拽宽高是否开启*/
                            resize:true,
                            /*是否可以拖动元素*/
                            drag:true
                        },
                        skin:{
                            /*默认皮肤*/
                            defaultValue:'mc-skin-darcula'
                        }
                    },
                    /*脚本编辑器*/
                    code:{
                        /*当出现错误时鼠标悬停是否聚焦当前结构的右侧属性面板*/
                        hoverFocusWhileError:false
                    },
                    left:{
                        // 当{}左侧组件时，自动追加到中间面板的事件名称 click dblclick click,dblclick 逗号分隔 多个
                        addToCenterEvent:'click',
                        /*当从左侧拖拽完 或者点击控件 添加到中间区域后，是否立即聚焦此控件 @2.2.9.2*/
                        afterDropIfFocus:true,
                        /*左侧点击组件是否自动追加到根节点*/
                        leftClickToRoot:false,
                    },
                    right:{
                        /*复选框 属性配置 是否弹出 帮助提示*/
                        tooltip:false
                    },
                    /*性能配置*/
                    speed:{
                        /*拖拽*/
                        drag:{
                            /*对齐线 这块要遍历孩子节点 如果孩子节点太多
                            拖拽就显得有点卡 针对界面太大情况 请自行调整一个合适参数
                            当要对齐的节点数少就用 当太多了就可以禁用 或者缩减对齐个数
                            */
                            align:{
                                /*是否启用 不启用速度很快*/
                                enable:true,
                                /*当所在父节点的孩子超过多少时 则停止对齐线功能*/
                                disableMaxChildrenSize:300,
                                /*当所在父节点的孩子超过多少时 则对齐线功能只对齐一个孩子*/
                                singleAlignMaxChildrenSize:50
                            }
                        },
                        /*工作区的渲染 是否采用极速渲染：true|false
                            极速渲染只能保障组件内部，无法保障与外部组件的事件交互
                            当是vue时 事件的交互就没了 谨慎开启，此种情况对极其要求
                            性能的场景，不希望整个页面全盘刷新时使用 还要自己处理特殊场景
                            可以联系我们，针对您的业务场景 共同商量怎么做
                            这个是实验室功能，暂时还不完备 谨慎开启
                        */
                        fastRender : false,
                        /*
                            针对magicalCoder定制的组件渲染方式 速度很快
                        */
                        magicalCoderRender : false,
                        /*
                            哪些组件即将被magicalCoderRender渲染:这是一个正则集 只要当前组件identifier在此集合内便可命中
                        */
                        magicalCoderRenderPrefixArray:["mc-echarts-.*","mc-ui-geometry-.*"],
                        //返回的数据种是否开启截屏功能
                        screenShot : true,
                    },
                    center:{
                        //标尺
                        ruler:{
                            //是否隐藏 如果设置true 就需要自己处理下工作区的样式问题 目前是往右写死了偏移
                            hide : false
                        }
                    }
                }
            },
            type:{
                TEXT:"text",
                TEXTAREA:"textarea",
                SELECT:"select",
                CHECKBOX:"checkbox",
                CHECKBOX_ARRAY:"checkbox_array",//一般用于['a','b'] 或者[1,2]这种数组配置 暂时配合chang:ATTR使用 样式还不支持
                SWITCH:"switch",
                COLOR_PICKER:"colorpicker",
                FILEUPLOAD:"fileupload",
                HTML:"html",
                SLIDER:"slider",
            },
            change:{
                ATTR:"attr",
                CLASS:"class",
                TEXT:"text",
                STYLE:"style",
                /*我们把一些不希望存在标签属性上的配置统一放在mcstyle了，设置值方式完全跟STYLE一致*/
                MCSTYLE:"mcstyle"
            }
        },
        /*index-page相关配置*/
        page:{
            remote:false,
        }
    }
    //日志
    this.log = {
        debug:false,//true|false 详细打印布局器加载过程日志信息

    }
 }
ApplicationEnv.prototype.getEnv = function () {
     return this.env;
 }
 ApplicationEnv.prototype.addTmpShadeWhileIe=function(tagClassMapping,identifiers){
    var ieVersion = this.ieVersion();
    if(ieVersion>0){//ie系列
        for(var i=0;i<identifiers.length;i++){
            var item = tagClassMapping[identifiers[i]];
            if(item){
                item.tmpWrapTag='div';
                item.tmpWrapShade=true;
            }
        }
    }
 }
/*获取ie版本*/
ApplicationEnv.prototype.ieVersion=function() {
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
    var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; //判断是否IE<11浏览器
    var isEdge = userAgent.indexOf("Edge") > -1 && !isIE; //判断是否IE的Edge浏览器
    var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1;
    if(isIE) {
        var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
        reIE.test(userAgent);
        var fIEVersion = parseFloat(RegExp["$1"]);
        if(fIEVersion == 7) {
            return 7;
        } else if(fIEVersion == 8) {
            return 8;
        } else if(fIEVersion == 9) {
            return 9;
        } else if(fIEVersion == 10) {
            return 10;
        } else {
            return 6;//IE版本<=7
        }
    } else if(isEdge) {
        return 100;//edge
    } else if(isIE11) {
        return 11; //IE11
    }else{
        return -1;//不是ie浏览器
    }
}

 //请不要改此处全局变量名
var APPLICATION_ENV = new ApplicationEnv();
