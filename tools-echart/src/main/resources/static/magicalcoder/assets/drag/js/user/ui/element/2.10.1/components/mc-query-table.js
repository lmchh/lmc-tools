/*test组件:MagicalCoder参考lib\mc\magicalcoder.js 闭包的写法有利于内部变量不随意暴漏出去*/
(function(){
    var callbackUtil = {
        /*左侧新增到工作区:咱们来动态控制js的生成*/
        insertIntoWorkspace:function(param){
            var mcBusiness = param.dragNode.attributes['mc-business'];
            if(mcBusiness){
                switch(mcBusiness){
                    case 'mc-query-table':
                        var submitBtn = MAGICAL_CODER_API.searchNodes({fromNode:param.dragNode,query:{identifier:"el-button"}})[0];
                        var elTable = MAGICAL_CODER_API.searchNodes({fromNode:param.dragNode,query:{identifier:"el-table"}})[0];
                        var dataVariable = MagicalCoder.uuid();
                        elTable.attributes[":data"] = dataVariable;
                        var queryMethod = MagicalCoder.uuid();
                        submitBtn.attributes["@click"]=queryMethod+"()";
                        var fn = ''+
                            'function(){'+
                                'vueData.'+dataVariable+'=[{"name":"您好","id":1}]'+
                            '}'+
'';
                        MAGICAL_CODER_API.getIframeUi().vueApi().setVueMethod(queryMethod,fn)
                        break;
                }
            }
        }
    }
    MagicalCoder.install({
        /*左侧可拖拽的源*/
        dragItems:[
            {
               name:"测试组件",
               icon:"layui-icon layui-icon-face-smile",
               children:[
                   {
                       name:"查询列表",
                       icon:"ri-edit-box-line",
                       html:
                       ''+
                           '<el-row mc-business="mc-query-table">'+
                               '<el-col :span="24">'+
                                   '<el-form label-width="85px" :inline="true">'+
                                       '<el-form-item label="单行标题">'+
                                           '<el-input v-model="input" placeholder="请输入内容"></el-input>'+
                                       '</el-form-item>'+
                                       '<el-form-item >'+
                                           '<el-button>查询</el-button>'+
                                       '</el-form-item>'+
                                   '</el-form>'+
                               '</el-col>'+
                               '<el-col :span="24">'+
                                   '<el-table :data="tableList" :fit="true" :show-header="true">'+
                                       '<el-table-column prop="id" label="列1"></el-table-column>'+
                                       '<el-table-column prop="name" label="列2"></el-table-column>'+
                                       '<el-table-column label="操作" fixed="right" width="200px">'+
                                           '<template slot-scope="scope">'+
                                               '<el-button mc-type="column-el-button" size="mini" type="primary">编辑</el-button>'+
                                               '<el-button mc-type="column-el-button" size="mini" type="danger">删除</el-button>'+
                                           '</template>'+
                                       '</el-table-column>'+
                                   '</el-table>'+
                               '</el-col>'+
                           '</el-row>'+
''
                   },
               ]
           }
       ],
       /*自定义组件和属性*/
        components:[

        ],
        /*回调处理*/
        callbacks:[
            {
                /*callback.js中的回调函数名：当从左侧往工作区拖放控件*/
                before_drop_left_to_center:function(param){
                    callbackUtil.insertIntoWorkspace(param);
                    return true;
                },
                /*执行顺序*/
                order:1
            },
            {
              before_click_left_component_to_center:function(param){
                  callbackUtil.insertIntoWorkspace(param);
                  return true;
              },
              order:1
           },
           /*{
                after_start:function(param){
                   let background_img = 'https://dss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2073439784,2147418910&fm=26&gp=0.jpg'
                   MAGICAL_CODER_API.insertCanvasStyle({canvasStyle: {"background": 'url(${background_img}) no-repeat'}})
                },
                order:1
           }*/
        ],
        /*处理环境变量application-env.js*/
        changeEnv:function(appEnv){

        },
        styles:[
            {
                order:1,
                css:
                ''
            }
        ]
    });
})();
