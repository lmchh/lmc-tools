/*每一种Ui的个性处理 比如各种组件初始化 重绘 注意剩余接口在iframe-ui-end.js中*/
var $ = jQuery;
function IframeUi() {
    this.vueMethod = {};
    this.defaultCss = "";//默认样式
    this.css = this.defaultCss;
    this.javascript="";
    //是否开启javascript调试
    this.debug = false;
    this.vueMethodReg = new RegExp("//functions-begin\n\\{([\\s\\S]*?)\\};\n?//functions-end","g");
    this.splitStr = ",/*别删我*/\n";
    //此正在匹配函数
    this.functionReg = new RegExp("function\\s+(\\w+)\\((\\w+)\\)\\s*\\{([\\s\\S]*)\\}","g");
    /*脚本编辑器的数据functionName:html 方法名:脚本编辑器的可恢复数据*/
    this.magicalJsCodeData = {}
    /*工作区画布基础样式设置 key:样式名 value:样式值*/
    this.canvasStyle = {}
    this.construct();

}
IframeUi.prototype.inject = function(SINGLETON){
    this.preInject(SINGLETON);
}
//提供一系列动态操作脚本的接口 您可以调用API自由控制脚本内容
IframeUi.prototype.api = function(){
    return this.jqueryApi();//具体有哪些可参考ifram-ui-end.js的IframeUi.prototype.jqueryApi
}
/*此方法不要改名 初始化组件的方法 每次代码重绘 会调用此方法 来重新初始化组件*/
IframeUi.prototype.render=function (param) {
//    console.log(param.trigger.triggerChange)
    var c = param.trigger.triggerChange;
    var focusNode = this.magicalApi.getFocusNode();

    param.globalConstant.settings.other.speed.fastRender = false;
    if(param.globalConstant.settings.other.speed.fastRender &&
        ( c =='changeAttr' || c == 'centerDrop' || c == 'leftDrop'|| c== 'changeText' || c == 'appendText'
        ||c == 'delete' || c == 'changeStyles' /*|| c == 'duplicate'*/)
    ){
        this.fastRender(param);
    }else{
        if(this.useMagicalCoderRender(param)){
            this.magicalCoderComponentsRender(param);
        }else{
            this.slowRender(param);
        }
    }
    //别管了 调用即可
    param.jsonFactory.resetTrigger();
}
/*把所有html拿出来重绘一遍，简单粗暴，但是当界面结构里有很多ajax请求，就可能会很慢 但是很保险 页面出错率0*/
IframeUi.prototype.slowRender = function(param){
//    console.log("全量刷新")
   var html=param.html,jsonFactory=param.jsonFactory,globalConstant=param.globalConstant;
    if(html==null){
        return;
    }
    var magicalCoderCss = this.autoCreateMagicalCss();
    magicalCoderCss.innerHTML = this.getCss();//设置样式内容

    var magicalDragScene = this.autoCreateRootDom();
    magicalDragScene.innerHTML = html;

    var javascript = this.getJavascript();
    try {
        if(this.debug){
            javascript = "debugger\n" + javascript;
        }
        //使用eval才行 使用全局eval 是全局作用域，更有利于window对象可用
        window.eval(javascript);
    }catch (e) {
        var msgHtml = '<div class="layui-row"><div class="layui-col-xs12" style="font-size: 17px; font-weight: bolder;">'+e.message+'</div><div class="layui-col-xs12" style="color: rgb(221, 32, 32);">'+e.stack+'</div></div>';
        parent.window.layui.layer.open({
            type:1,
            title:"您编写的脚本编译错误-非布局器报错",
            area: ['800px', '400px'],
            shadeClose:true,
            content:msgHtml,
        })
        console.log(e);
    }
    //当渲染完注意以下方式
    // mini.parse();
    this.fixDynamicDomAfterRender({container:$("#magicalDragScene")});
}
/*
    快速render极致性能：局部更新的方式 建个临时结构来接管 渲染完替换原始位置
    虽然很快，但是针对于界面上有
    @ echarts组件 可以给固定宽度 那就不会空白了
 */
IframeUi.prototype.fastRender=function (param) {
//    console.log("局部刷新")
    var focusNode = this.magicalApi.getFocusNode();
    var html = focusNode!=null ? this.magicalApi.nodeToHtml({node:focusNode,pure:true}):"";

    var c = param.trigger.triggerChange,triggerOperateNodes=param.trigger.triggerOperateNodes;
    //原始结构
    var originElem = null;
    if(c == 'leftDrop'){
        originElem = $("#magicalDragScene").find(".magicalcoder-page-drag-item-label");
    }else if(c == 'delete'){
        if(triggerOperateNodes){
            for(var i=0;i<triggerOperateNodes.length;i++){
                var elem = this.magicalApi.getElemByMagicalCoderId({id:triggerOperateNodes[i].magicalCoder.id});
                if(elem){
                    elem.remove();
                }
            }
        }
        return;
    }else{
        originElem = this.magicalApi.getElemByMagicalCoderId({id:focusNode.magicalCoder.id});
    }


    //body里增加一个临时结构
    $("body").append("<div id='magicalDragSceneGhost'></div>");

    var magicalDragScene = $("#magicalDragSceneGhost");
    //由于vue框架不支持body做根 并且<template></template>标签在html里会自动变成虚无 所以我们正则替换一下 自动加上让vue渲染
    var html = "<template>"+html+"</template>";
    magicalDragScene[0].innerHTML=html;

    var javascript = this.getJavascript();

    try {
        //使用eval才行
        var newJs = javascript.replace("new Ctor().$mount('#magicalDragScene')","new Ctor().$mount('#magicalDragSceneGhost')");
        eval(newJs);
    }catch (e) {
        var msgHtml = '<div class="layui-row"><div class="layui-col-xs12" style="font-size: 17px; font-weight: bolder;">'+e.message+'</div><div class="layui-col-xs12" style="color: rgb(221, 32, 32);">'+e.stack+'</div></div>';
        parent.window.layui.layer.open({
            type:1,
            title:"您编写的脚本编译错误-非布局器报错",
            area: ['800px', '400px'],
            shadeClose:true,
            content:msgHtml,
        })
        console.log(e);
        eval("var Ctor = Vue.extend({});new Ctor().$mount('#magicalDragSceneGhost');");//兼容一下js错误 给出渲染界面
    }
    //当渲染完注意以下方式
    // mini.parse();
    //做一些优化 比如删除一些不需要的结构
    this.fixDynamicDomAfterRender({container:$("#magicalDragSceneGhost")});
    var children =  $("#magicalDragSceneGhost").children();
    originElem.replaceWith(children);
    //删除临时结构即可
    $("#magicalDragSceneGhost").remove();
}

//修复一下动态创建的结构跑偏问题 有些ui设计的比较差 动态没用的结构很多，我们设置简介标签上的属性样式无法继承 所以就需要我们修复一下
IframeUi.prototype.fixDynamicDomAfterRender = function(param){
    var container = param.container;
    var dragMcPane = this.jsonFactory.api().pubGetDragMcPaneName();
    container.find(".mini-panel-body").addClass(dragMcPane);
    container.find(".mini-grid-rows").removeClass(dragMcPane);
    container.find(".mini-panel").removeClass(dragMcPane);

    container.find(".mini-splitter-pane").addClass(dragMcPane)



    container.find(".mini-layout-region-body").addClass(dragMcPane)

    //这个就厉害了
    container.find(".mini-tabs-body").addClass(dragMcPane);
    container.find(".mini-outlookbar-groupBody").addClass(dragMcPane)


    //只能是手动对应了
    var magicalCoderIdAttrName = this.jsonFactory.api().getMagicalCoderIdAttrName();
    var tabsNodesCache = {};

    var jsonApi = this.jsonFactory.api();
    container.find(".mini-tabs-bodys").each(function (idx,item) {
        var tabs = $(this).parents(".mini-tabs");//最外层的孩子
         var id = tabs.attr(magicalCoderIdAttrName);
         var value = tabsNodesCache[''+id];
         if(!value){
             var list = jsonApi.pubSearchNodes(null,{id:id});
             if(list && list.length>0){
                 value = list[0];
                 tabsNodesCache[''+id] = value;
             }
         }
         if(value){
             //找孩子
             var children = value.magicalCoder.children;
             $(this).children().each(function (idx,item) {
                 $(this).attr(magicalCoderIdAttrName,children[idx].magicalCoder.id);
             });
         }
    })

    //mini-layout 本来期望的地方能加上drag-mc-pane 并且有data-magical_coder_id 无奈miniui未按照预期放置位置 咱们来修复一下
    var cacheData = {};
    //不应该在这些结构上添加dragMcPane
    container.find(".mini-layout-region-north").removeClass(dragMcPane);
    container.find(".mini-layout-region-west").removeClass(dragMcPane);
    container.find(".mini-layout-region-east").removeClass(dragMcPane);
    container.find(".mini-layout-region-south").removeClass(dragMcPane);
    container.find(".mini-layout-region-center").removeClass(dragMcPane);
    container.find(".mini-layout-border").each(function (idx,item) {
        var parent = $(this).parents(".mini-layout");//最外层父节点
        var id = parent.attr(magicalCoderIdAttrName);
        var value = cacheData[''+id];
        if(!value){
            var list = jsonApi.pubSearchNodes(null,{id:id});
            if(list && list.length>0){
                value = list[0];
                cacheData[''+id] = value;
            }
        }
        if(value){
            //找孩子
            var children = value.magicalCoder.children;
            var cache = {};
            for(var i=0;i<children.length;i++){
                var child = children[i];
                var key = child.attributes['region'];
                cache[key]=child.magicalCoder.id;
            }
            //追加magicalCoderId
            $(this).find(".mini-layout-region-north > .mini-layout-region-body:first").attr(magicalCoderIdAttrName,cache['north']);
            $(this).find(".mini-layout-region-south > .mini-layout-region-body:first").attr(magicalCoderIdAttrName,cache['south']);
            $(this).find(".mini-layout-region-east > .mini-layout-region-body:first").attr(magicalCoderIdAttrName,cache['east']);
            $(this).find(".mini-layout-region-west > .mini-layout-region-body:first").attr(magicalCoderIdAttrName,cache['west']);
            $(this).find(".mini-layout-region-center > .mini-layout-region-body:first").attr(magicalCoderIdAttrName,cache['center']);
        }
    })

    //outlookbar
    container.find(".mini-outlookbar-border").each(function (idx,item) {
        var parent = $(this).parents(".mini-outlookbar");//最外层父节点
        var id = parent.attr(magicalCoderIdAttrName);
        var value = null;
        if(!value){
            var list = jsonApi.pubSearchNodes(null,{id:id});
            if(list && list.length>0){
                value = list[0];
            }
        }
        if(value){
            //找孩子
            var children = value.magicalCoder.children;
            //追加magicalCoderId
            $(this).children().each(function(idx,item){
                var item = $(this).find(".mini-outlookbar-groupBody:first");
                item.attr(magicalCoderIdAttrName,children[idx].magicalCoder.id);
            })
        }
    })

    //splitter
    var spliterCache
    container.find(".mini-splitter-border").each(function (idx,item) {
        var bars = $(this).parents(".mini-splitter");//最外层的父亲
        var id = bars.attr(magicalCoderIdAttrName);
        var value = null;
        var list = jsonApi.pubSearchNodes(null,{id:id});
        if(list && list.length>0){
            value = list[0];
        }
        if(value){
            //找孩子
            var children = value.magicalCoder.children;
            $(this).children().each(function (idx,item) {
                if($(this).hasClass("mini-splitter-pane")){
                    $(this).attr(magicalCoderIdAttrName,children[idx].magicalCoder.id);
                }
            });
        }
    })


}

IframeUi.prototype.getJavascript = function(){
    return this.getJqueryScript();
}
IframeUi.prototype.setJavascript = function(javascript){
    this.setJqueryScript(javascript);
}
/*扩展可选图标icon*/
IframeUi.prototype.iconList = function(){
    if(window.location.href.indexOf("from=icon_list")!=-1){
        $("body").css("overflow-y","scroll")
        var util = this.util();
        var iconArr = []
        var html = [];
        html.push('<ul class="magicalcoder-extend-icons">')
        for(var i=0;i<iconArr.length;i++){
            html.push("<li><i class='layui-icon "+iconArr[i]+"'></i></li>")
        }
        html.push('</ul>')
        document.body.innerHTML = html.join('');

        var lis = document.getElementsByTagName("li");
        for(var i=0;i<lis.length;i++){
            lis[i].addEventListener('click',function () {
                var icon = this.childNodes[0]
                var active = true;
                if(icon.className.indexOf("active")==-1){
                    active = false;
                }
                var actives = document.getElementsByClassName("active");
                for(var j=0;j<actives.length;j++){
                    util.removeClass(actives[j],"active");
                }
                if(!active){
                    icon.className = icon.className +" active";
                }

            })
        }
        return true;
    }
    return false;
}
//下载按钮下载的内容
IframeUi.prototype.download = function(html){
    var source = [
        '<meta charset="UTF-8">','<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">',
        '<title>layui-由www.magicalcoder.com可视化布局器生成</title>',
        '<script type="text/javascript" src="assets/drag/js/user/iframe/miniui/3.9.6/components/import.js"></script>'
    ]
    //设置样式
    var style = [];
    for(var key in this.canvasStyle){
        style.push(key+":"+this.canvasStyle[key]);
    }
    var css = '\n<style>\n'+this.getCss()+'\n</style>\n';
    var head = '<head>'+source.join('\n')+css+'\n</head>\n';
    var body = '<body style="'+style.join(";").replace(/\"/g,"'")+'">\n'+html+'\n<script>\n'+this.getJavascript()+'\n</script>\n</body>\n';
    return {
        htmlBefore:"<!DOCTYPE html>\n<html><!--代码由www.magicalcoder.com可视化布局器拖拽生成 资源请自行下载到本地-->\n",
        head:head,
        body:body,
        htmlEnd:"\n</html>",
    }
}
