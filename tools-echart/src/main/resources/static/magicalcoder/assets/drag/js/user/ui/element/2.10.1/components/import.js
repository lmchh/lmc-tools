/*
    引入被安装的组件:为了把constant.js涉及的组件解耦，特拆出components目录
    如果您有新增组件 可以引入到此处
    如果您追求性能，也可以把components的文件合并拷贝替换此文件 这样就只加载一个组件了
*/
MagicalCoder.import([

   //自定义组件
   "assets/drag/js/user/ui/element/2.10.1/components/lmc-echarts-line.js",
   "assets/drag/js/user/ui/element/2.10.1/components/lmc-echarts-bar.js",
   "assets/drag/js/user/ui/element/2.10.1/components/lmc-echarts-bar1.js",



   //測試組件
   // "assets/drag/js/user/ui/element/2.10.1/components/mc-query-table.js",
   // "assets/drag/js/user/ui/element/2.10.1/components/mc-test-tag.js",

   "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-line.js",
      /*yy*/
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-line1.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-line2.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-line3.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-line4.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-line5.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-line6.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-line7.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-line8.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-bar.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-pie.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-pie1.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-pie2.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-pie3.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-pie4.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-pie5.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-pie6.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-radar.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-dashboard.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-scatter.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-scatter1.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-scatter2.js",
      /*lj*/
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-map.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-hkmap.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-tree.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-lines.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-treemap.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-sunburst.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-parallel.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-sankey.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-themeriver.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-simple-gauge.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-speed-gauge.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-grogress-gauge.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-stage-gauge.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-grade-gauge.js",
      "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-barometer-gauge.js",
       /*yjj*/
       "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-funnel.js",
       "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-relation.js",
       "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-heatmap.js",


   //基礎組件（容器，表單控件，高級組件，導覽，高級編程，圖標，形狀，）
   /*由于各个ui都可以复用下面这几个通用的配置，所以我们提取出来放入magicalcoder目录*/
   "assets/drag/js/user/ui/element/2.10.1/components/mc-ui-components.js",
   // 右邊屬性
   "assets/drag/js/user/ui/magicalcoder/components/mc-geometry.js",
   //其他（音頻，視頻，斜體，下劃線等等）
   "assets/drag/js/user/ui/magicalcoder/components/mc-other.js",


])
