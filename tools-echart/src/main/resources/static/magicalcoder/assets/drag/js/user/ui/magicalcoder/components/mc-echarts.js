/*test组件:MagicalCoder参考lib\mc\magicalcoder.js*/
var ___echartsAttr = function(){
    var data =
    {
           title:"属性",
           active:true,
           width:"100%",
           content:[
                {type:"select",clearAttr:true       ,oneLine:true,change:"attr",attrName:'mc-attr-unknown-api',title:'数据源',tooltip:"",options:[{"api1":"数据源自己定义"},{"api2":"这只是个示例"},{"api3":"数据源可以来自后端"}]},
                {type:"colorpicker"    ,clearAttr:true         ,oneLine:true ,change:"attr"     ,title:'背景色'  ,attrName:'mc-attr-background_color' },
                {type:"html",html:'<fieldset class="layui-elem-field layui-elem-title"><legend>标题(title)</legend></fieldset>'},
                {type:"checkbox",clearAttr:true,oneLine:true,change:"attr",title:'标题状态',options:[{"c":"true","n":"mc-attr-bool-title-show","t":"显示标题","u":"false","dv":"true"}]},
                {type:"text"      ,clearAttr:true       ,oneLine:true     ,change:"attr"	,title:'主标题文本',attrName:'mc-attr-str-title-text',placeholder:"主标题文本"               },
                {type:"text"      ,clearAttr:true       ,oneLine:true     ,change:"attr"	,title:'主标题文本超链接',attrName:'mc-attr-str-title-link',placeholder:"主标题文本超链接"               },
                {type:"select",clearAttr:true       ,oneLine:true,change:"attr",attrName:'mc-attr-str-title-target',title:'超链接打开方式',tooltip:"",options:[{"self":"当前窗口打开"},{"blank":"新窗口打开"}]},
                {type:"colorpicker"    ,clearAttr:true         ,oneLine:true ,change:"attr"     ,title:'主标题文字颜色'  ,attrName:'mc-attr-title-text_style-color' },
                {type:"select",clearAttr:true       ,oneLine:true,change:"attr",attrName:'mc-attr-title-text_style-font_style',title:'主标题字体风格',tooltip:"",options:[{"normal":"普通"},{"italic":"斜体1"},{"oblique":"斜体2"}]},
                {type:"select",clearAttr:true       ,oneLine:true,change:"attr",attrName:'mc-attr-title-text_style-font_weight',title:'字体粗细',tooltip:"",options:[{"normal":"普通"},{"bold":"粗体"},{"bolder":"加粗"},{"lighter":"细"}]},
                {type:"text"      ,clearAttr:true       ,oneLine:true     ,change:"attr"	,title:'副标题文本',attrName:'mc-attr-str-title-subtext',placeholder:"副标题文本"               },
                {type:"text"      ,clearAttr:true       ,oneLine:true     ,change:"attr"	,title:'副标题文本超链接',attrName:'mc-attr-str-title-sublink',placeholder:"副标题文本超链接"               },
                {type:"select",clearAttr:true       ,oneLine:true,change:"attr",attrName:'mc-attr-str-title-subtarget',title:'超链接打开方式',tooltip:"",options:[{"self":"当前窗口打开"},{"blank":"新窗口打开"}]},
                {type:"colorpicker"    ,clearAttr:true         ,oneLine:true ,change:"attr"     ,title:'副标题文字颜色'  ,attrName:'mc-attr-title-subtext_style-color' },
                {type:"select",clearAttr:true       ,oneLine:true,change:"attr",attrName:'mc-attr-str-title-left',title:'左间距',tooltip:"",options:[{"left":"左对齐"},{"center":"居中"},{"right":"右对齐"}]},
                {type:"select",clearAttr:true       ,oneLine:true,change:"attr",attrName:'mc-attr-str-title-left',title:'上间距',tooltip:"",options:[{"top":"顶对齐"},{"middle":"居中"},{"bottom":"底对齐"}]},
                {type:"html",html:'<fieldset class="layui-elem-field layui-elem-title"><legend>图例(legend)</legend></fieldset>'},
                {type:"select",clearAttr:true       ,oneLine:true,change:"attr",attrName:'mc-attr-str-legend-left',title:'左间距',tooltip:"",options:[{"left":"左对齐"},{"center":"居中"},{"right":"右对齐"}]},
                {type:"select",clearAttr:true       ,oneLine:true,change:"attr",attrName:'mc-attr-str-legend-left',title:'上间距',tooltip:"",options:[{"top":"顶对齐"},{"middle":"居中"},{"bottom":"底对齐"}]},
        ]
    }
    return data;
}
MagicalCoder.install({
    /*左侧可拖拽的源*/
    dragItems:[
        {
            name:"图表",
            icon:"ri-line-chart-line",
            search:true,
            children:[
                {
                    name:"线性",
                    children:[
                        {
                            name:"线性图",
                            icon:"ri-line-chart-line",
                            html:"<div class='mc-echarts-line' style='height:400px;'></div>"
                        }
                    ]
                },
                {
                    name:"条形",
                    children:[
                        {
                            name:"条形图",
                            icon:"ri-bar-chart-2-line",
                            html:"<div class='mc-echarts-bar' style='height:400px;'></div>"
                        }
                    ]
                },
                {
                    name:"饼图",
                    children:[
                        {
                            name:"饼图",
                            icon:"ri-pie-chart-2-line",
                            html:"<div class='mc-echarts-pie' style='height:400px;'></div>"
                        }
                    ]
                },
                {
                    name:"散点",
                    children:[
                        {
                            name:"散点图",
                            icon:"ri-bubble-chart-line",
                            html:"<div class='mc-echarts-scatter' style='height:400px;'></div>"
                        }
                    ]
                },{
                    name:"其他",
                    children:[
                        {
                            name:"雷达图",
                            icon:"ri-radar-line",
                            html:"<div class='mc-echarts-radar' style='height:400px;'></div>"
                        },{
                            name:"K线图",
                            icon:"ri-stock-line",
                            html:"<div class='mc-echarts-k' style='height:400px;'></div>"
                        },{
                            name:"树图",
                            icon:"ri-organization-chart",
                            html:"<div class='mc-echarts-tree' style='height:400px;'></div>"
                        },{
                            name:"漏斗图",
                            icon:"ri-filter-3-line",
                            html:"<div class='mc-echarts-funnel' style='height:400px;'></div>"
                        },{
                            name:"仪表盘",
                            icon:"ri-dashboard-3-line",
                            html:"<div class='mc-echarts-gauge' style='height:400px;'></div>"
                        },{
                            name:"盒须图",
                            icon:"ri-t-box-line",
                            html:"<div class='mc-echarts-boxplot' style='height:400px;'></div>"
                        },{
                            name:"旭日图",
                            icon:"ri-lifebuoy-fill",
                            html:"<div class='mc-echarts-sunburst' style='height:400px;'></div>"
                        },{
                            name:"矩形树图",
                            icon:"ri-collage-line",
                            html:"<div class='mc-echarts-treemap' style='height:400px;'></div>"
                        },{
                            name:"象形柱图",
                            icon:"ri-flight-takeoff-line",
                            html:"<div class='mc-echarts-pictorialbar' style='height:400px;'></div>"
                        },{
                            name:"桑基图",
                            icon:"ri-git-commit-line",
                            html:"<div class='mc-echarts-sankey' style='height:400px;'></div>"
                        },{
                            name:"平行坐标",
                            icon:"ri-line-height",
                            html:"<div class='mc-echarts-parallel' style='height:400px;width:50%'></div>"
                        },{
                            name:"主题河流",
                            icon:"ri-voiceprint-line",
                            html:"<div class='mc-echarts-themeriver' style='height:400px;'></div>"
                        },{
                            name:"数据集图",
                            icon:"ri-bar-chart-grouped-line",
                            html:"<div class='mc-echarts-dataset' style='height:400px;'></div>"
                        },{
                            name:"二维地图",
                            icon:"ri-road-map-line",
                            html:"<div class='mc-leaflet-map' style='height:400px;width:50%;'></div>"
                        }
                    ]
                }

            ]
        }
   ],
    /*自定义组件和属性*/
    components:[
        {
            identifier:"mc-echarts-bar",
            properties:{name:"条形图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-line",
            properties:{name:"线性图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,duplicateAttr:['id'],afterDragDrop:{refresh:true},afterResize:{refresh:true} },
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-pie",
            properties:{name:"饼图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-scatter",
            properties: {name:"散点图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-radar",
            properties: {name:"雷达图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-k",
            properties: {name:"K线图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-tree",
            properties: {name:"树图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-funnel",
            properties: {name:"漏斗图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-gauge",
            properties: {name:"仪表盘",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-boxplot",
            properties:{name:"盒须图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-sunburst",
            properties:{name:"旭日图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-treemap",
            properties:{name:"矩阵树图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-pictorialbar",
            properties:{name:"象形柱图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-sankey",
            properties:{name:"桑基图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-parallel",
            properties:{name:"平行坐标",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true},tmpWrapTag:"div",tmpWrapShade:true},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-themeriver",
            properties:{name:"主题河流",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-echarts-dataset",
            properties:{name:"数据集图",dragInto:false,duplicate:true,assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true}},
            attributes:[
                ___echartsAttr()
            ]
        },
        {
            identifier:"mc-leaflet-map",
            properties:{name:"二维地图",dragInto:false,duplicate:true,duplicateAttr:["id"],assistDuplicate:true, copy:true,      paste:false,    canDelete:true,assistDelete:true,afterDragDrop:{refresh:true},afterResize:{refresh:true},tmpWrapTag:"div",tmpWrapShade:true},
            attributes:[
                ___echartsAttr()
            ]
        },
    ]
});
