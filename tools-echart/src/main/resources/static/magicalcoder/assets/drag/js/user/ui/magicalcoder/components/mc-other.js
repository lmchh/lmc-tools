MagicalCoder.install({
    dragItems:[
        {
            name:"其他",
            icon:"ri-bold",
            children:[
                {
                    name:"静态表格",
                    icon:"ri-table-line",
                    html:"<table class='layui-table'><thead><tr><th>列1</th><th>列2</th><th>列3</th></tr></thead><tbody><tr><td></td><td></td><td></td></tr></tbody></table>"
                },
                {
                    name:"音频",
                    icon:"ri-file-music-line",
                    html:"<audio controls='controls'></audio>"
                },
                {
                    name:"视频",
                    icon:"ri-video-line",
                    html:"<video controls='controls'></video>"
                },{
                    name:"网页",
                    icon:"ri-window-2-line",
                    html:"<iframe src='http://www.magicalcoder.com' frameborder='0'></iframe>"
                },
                {
                    name:"徽章",
                    icon:"ri-notification-badge-line",
                    html:"<span class='layui-badge-dot'></span>"
                },{
                    name:"超链接",
                    icon:"ri-link",
                    html:"<a href='http://www.magicalcoder.com'>超链接</a>"
                },{
                    name:"图标文字",
                    icon:"ri-remixicon-line",
                    html:"<i>*</i>"
                },{
                    name:"段落",
                    icon:"ri-align-right",
                    html:"<p>这是一段落</p>"
                },{
                    name:"小字",
                    icon:"ri-character-recognition-line",
                    html:"<small>小字</small>"
                },{
                    name:"高亮",
                    icon:"ri-character-recognition-fill",
                    html:"<mark>高亮</mark>"
                },{
                    name:"中划线",
                    icon:"ri-strikethrough",
                    html:"<del>中划线</del>"
                },{
                    name:"下划线",
                    icon:"ri-underline",
                    html:"<u>下划线</u>"
                },{
                    name:"斜体",
                    icon:"ri-italic",
                    html:"<em>斜体</em>"
                },{
                    name:"加粗",
                    icon:"ri-bold",
                    html:"<strong>加粗</strong>"
                },{
                    name:"标题",
                    icon:"ri-stop-line",
                    html:"<span class='mc-title'>标题</span>"
                },
                {
                    name:"填空输入",
                    icon:"ri-stop-line",
                    html:"<span class='mc-form-tk'><input type='text' autocomplete='off'/></span>"
                },
            ]
        }
    ],
    components:[
        {
            identifier:"a",
            properties:{primary:0,name:"超链接",treeExtraName:{attr:['id','name'],text:true},dragInto:false,  duplicate:true,   copy:true,      paste:false,  canDelete:true,dblClickChange:{type:"text"}    },
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"text"      ,clearAttr:true       ,oneLine:true     ,change:"text"	,title:'文本'                },
                        {type:"text",clearAttr:true,oneLine:false,change:"attr",attrName:'href',title:'跳转地址'},
                        {type:"select",clearAttr:true       ,oneLine:true,change:"attr",attrName:'target',title:'打开方式',tooltip:"target",options:[{"_blank":"新窗口"},{"_self":"当前窗口"},{"_parent":"父窗口"},{"_top":"顶部窗口"}]},
                    ]
                }
            ]
        },
        {
            identifier:"i",
            properties:{primary:0,name:"图标文字",dragInto:false,  duplicate:true,duplicateAttr:[],       copy:true,      paste:false,  canDelete:true,dblClickChange:{type:"text"}    },
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"text"      ,clearAttr:true       ,oneLine:true     ,change:"text"	,title:'文本'                },
                        {type:"slider"    ,clearAttr:true       ,oneLine:false    ,change:"style",attrName:'font-size',title:'大小',tooltip:'font-size',extra:{min:0,max:500,suffix:"px"}},
                        {type:"text"      ,clearAttr:false      ,oneLine:true     ,change:"attr",attrName:'class'   ,title:'图标',extendKey:"icon",extend:true    }
                    ]
                }
            ],
        },
        {
            identifier:"span",
            properties:{primary:0,name:"行内",   dragInto:false, duplicate:true,duplicateAttr:[],         copy:true,      paste:false,    canDelete:true ,dblClickChange:{type:"text"}    },
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"text"      ,clearAttr:true       ,oneLine:true     ,change:"text"	,title:'文本'                },
                    ]
                }
            ],
        },
        {
            identifier:"p",
            properties: {name:"段落1",dragInto:false,  duplicate:true,duplicateAttr:[],   copy:true,       paste:true,  canDelete:true,dblClickChange:{type:"text"}    },
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"textarea"      ,clearAttr:true       ,oneLine:true     ,change:"text"	,title:'文本'                },
                    ]
                }
            ],
        },
        {
            identifier:"small",
            properties:  {name:"小字",dragInto:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:true,  canDelete:true,dblClickChange:{type:"text"}    },
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"textarea"      ,clearAttr:true       ,oneLine:true     ,change:"text"	,title:'文本'                },
                    ]
                }
            ],
        },
        {
            identifier:"mark",
            properties: {name:"高亮",dragInto:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:true,  canDelete:true,dblClickChange:{type:"text"}    },
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"textarea"      ,clearAttr:true       ,oneLine:true     ,change:"text"	,title:'文本'                },
                    ]
                }
            ],
        },
        {
            identifier:"del",
            properties: {name:"中划线",dragInto:false,  duplicate:true,duplicateAttr:[],   copy:true,    paste:true,  canDelete:true,dblClickChange:{type:"text"}    },
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"textarea"      ,clearAttr:true       ,oneLine:true     ,change:"text"	,title:'文本'                },
                    ]
                }
            ],
        },
        {
            identifier:"u",
            properties: {name:"下划线",dragInto:false,  duplicate:true,duplicateAttr:[],   copy:true,    paste:true,  canDelete:true,dblClickChange:{type:"text"}    },
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"textarea"      ,clearAttr:true       ,oneLine:true     ,change:"text"	,title:'文本'                },
                    ]
                }
            ],
        },
        {
            identifier:"em",
            properties: {name:"斜体",dragInto:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:true,  canDelete:true,dblClickChange:{type:"text"}    },
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"textarea"      ,clearAttr:true       ,oneLine:true     ,change:"text"	,title:'文本'                },
                    ]
                }
            ],
        },
        {
            identifier:"strong",
            properties: {name:"加粗",dragInto:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:true,  canDelete:true,dblClickChange:{type:"text"}    },
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"textarea"      ,clearAttr:true       ,oneLine:true     ,change:"text"	,title:'文本'                },
                    ]
                }
            ],
        },
        {
            identifier:"table",
            properties: {name:"静态表格",dragInto:false,canZoom:true,assistZoom:true,assistDelete:true,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true    },
            attributes: [],
        },
        {
            identifier:"thead",
            properties: {name:"表格头部",dragInto:false,assistDelete:true,  duplicate:false,duplicateAttr:[],   copy:true,      paste:true,  canDelete:true,onlyChildren:['tr']    },
            attributes: [],
        },
        {
            identifier:"tbody",
            properties: {name:"表格体",dragInto:false,assistDelete:true,assistAdd:true,  duplicate:false,duplicateAttr:[],   copy:true,      paste:true,  canDelete:true,onlyChildren:['tr']    },
            attributes: [],
        },
        {
            identifier:"tfoot",
            properties: {name:"表格脚",dragInto:false,assistDelete:true,assistAdd:true, duplicate:false,duplicateAttr:[],   copy:true,      paste:true,  canDelete:true,onlyChildren:['tr']    },
            attributes: [],
        },
        {
            identifier:"tr",
            properties: {name:"表格行",dragInto:false,assistDelete:true,assistAdd:true,assistDuplicate:true,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true,onlyChildren:['th','td']},
            attributes: [],
        },
        {
            identifier:"th",
            properties: {name:"表格标题",dragInto:true,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true,onlyParents:['tr'],dblClickChange:{type:"text"}},
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"text"  ,clearAttr:true       ,oneLine:true     ,change:"text"    ,title:'标题'                },
                    ]
                }
            ],
        },
        {
            identifier:"td",
            properties: {name:"单元格",dragInto:true,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true,onlyParents:['tr'],dblClickChange:{type:"text"}},
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"text"  ,clearAttr:true       ,oneLine:true     ,change:"text"    ,title:'文本'                },
                        {type:"checkbox",clearAttr:true,oneLine:true,change:"class",title:'自由定位',options:[{"c":"mc-ui-absolute-pane","n":"","t":"是否开启","u":""}]},
                        {type:"select",clearAttr:true,oneLine:false ,change:"mcstyle",attrName:'ab-unit'         ,title:'自由定位单位：默认'    ,tooltip:"百分比有利于自适应屏幕大小场景,其他值则适合固定屏幕",options:[{"%":"百分比"},{"px":"px"}] },
                        {type:"select",clearAttr:true,oneLine:false ,change:"mcstyle",attrName:'ab-unit-left'    ,title:'自由定位单位:左' ,tooltip:"优先级高于默认自由定位单位",options:[{"%":"百分比"},{"px":"px"}] },
                        {type:"select",clearAttr:true,oneLine:false ,change:"mcstyle",attrName:'ab-unit-top'     ,title:'自由定位单位：上',tooltip:"优先级高于默认自由定位单位",options:[{"%":"百分比"},{"px":"px"}] },
                        {type:"select",clearAttr:true,oneLine:false ,change:"mcstyle",attrName:'ab-unit-width'   ,title:'自由定位单位：宽',tooltip:"优先级高于默认自由定位单位",options:[{"%":"百分比"},{"px":"px"}] },
                        {type:"select",clearAttr:true,oneLine:false ,change:"mcstyle",attrName:'ab-unit-height'   ,title:'自由定位单位：高',tooltip:"优先级高于默认自由定位单位",options:[{"%":"百分比"},{"px":"px"}] },
                    ]
                }
            ],
        },
        {
            identifier:"audio",
            properties: {name:"音频",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true ,tmpWrapTag:'div',tmpWrapType:0,tmpWrapShade:true  },
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"text"  ,clearAttr:true       ,oneLine:true,change:"attr",attrName:'src',title:'播放资源',tooltip:"",placeholder:""},
                        {type:"checkbox",clearAttr:true     ,oneLine:true     ,change:"attr"     ,title:'状态'    ,options:[{"c":"autoplay","n":"autoplay","t":"自动播放","u":false,dv:false},{"c":"loop","n":"loop","t":"循环播放","u":false,dv:false},{"c":"muted","n":"muted","t":"静音","u":false,dv:false}]},
                        {type:"select",clearAttr:true       ,oneLine:true,change:"attr",attrName:'preload',title:'预加载',tooltip:"属性规定是否在页面加载后载入音频。",options:[{"auto":"自动"},{"meta":"元数据"},{"none":"否"}]},
                    ]
                }
            ],
        },
        {
            identifier:"video",
            properties: {name:"视频",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true ,tmpWrapTag:'div',tmpWrapType:0,tmpWrapShade:true  },
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"text"  ,clearAttr:true       ,oneLine:true,change:"attr",attrName:'src',title:'播放资源',tooltip:"",placeholder:""},
                        {type:"checkbox",clearAttr:true     ,oneLine:true     ,change:"attr"     ,title:'状态'    ,options:[{"c":"autoplay","n":"autoplay","t":"自动播放","u":false,dv:false},{"c":"loop","n":"loop","t":"循环播放","u":false,dv:false},{"c":"muted","n":"muted","t":"静音","u":false,dv:false}]},
                        {type:"select",clearAttr:true       ,oneLine:true,change:"attr",attrName:'preload',title:'预加载',tooltip:"属性规定是否在页面加载后载入音频。",options:[{"auto":"自动"},{"meta":"元数据"},{"none":"否"}]},
                        {type:"fileupload",clearAttr:true,oneLine:false,change:"attr",attrName:'poster',title:'加载时等待图片'},
                    ]
                }
            ],
        },
        {
            identifier:"iframe",
            properties: {name:"网页",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true ,tmpWrapTag:'div',tmpWrapType:0,tmpWrapShade:true  },
            attributes: [
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {type:"text"  ,clearAttr:true       ,oneLine:true,change:"attr",attrName:'src',title:'网址',tooltip:"",placeholder:""},
                        {type:"select",clearAttr:true       ,oneLine:true,change:"attr",attrName:'scrolling',title:'滚动条',tooltip:"",options:[{"yes":"是"},{"no":"否"},{"auto":"自动"}]},
                        {type:"checkbox",clearAttr:true     ,oneLine:true     ,change:"attr"     ,title:'状态'    ,options:[{"c":"1","n":"frameborder","t":"边框","u":"0",dv:"1"}]},
                    ]
                }
            ],
        },
    ]
})
