/*test组件:MagicalCoder参考lib\mc\magicalcoder.js*/
MagicalCoder.install({
    /*左侧可拖拽的源*/
    dragItems:[
        {
           name:"测试组件",
           icon:"layui-icon layui-icon-face-smile",
           children:[
               {
                   name:"测试",
                   icon:"ri-edit-box-line",
                   html:"<div class='mc-test-tag'>我是测试属性的</div>"
               },
           ]
       }
   ],
   /*自定义组件和属性*/
    components:[
        {
            /*组件命名空间*/
            identifier:"mc-test-tag",
            /*组件功能特点*/
            properties:{
                name:"测试组件",
                dragInto:false,
                assistDelete:false,
                assistAdd:false,
                assistDuplicate:false,
                duplicate:true,
                duplicateAttr:[],
                copy:true,
                paste:false,
                canDelete:true,
                dblClickChange:{
                    type:"text"
                }
            },
            /*组件属性*/
            attributes:[
                /*以下配置无意义 仅用来测试属性端自定义的扩展性 可以忽略*/
                [
                    /*下拉颜色组*/
                    /*采用自定义HTML:vue的组件嵌入到属性侧 index-layui.html已经引入了vue和element所以这里可以直接用*/
                    {type:"html",
                        category:"下拉颜色",
                        callback:{
                            htmlCallback:function(param){
                                return '<div class="layui-row layui-col-space3" id="mcTestColorApp"><div class="layui-col-xs3">颜色组</div><div class="layui-col-xs9"><el-select @change="changeMe" v-model="mcTestColor" placeholder="请选择颜色"><el-option v-for="item in colorOpts" :key="item.k" :label="item.l" :value="item.k" v-html="item.v"></el-option></el-select></div></div>'
                            },
                            render:function (param) {
                                //从magicalCoder获取当前聚焦节点的属性值 准备传入vue里
                                var config = param.config,focusNode=param.focusNode;
                                var mcTestColor = focusNode.attributes['my-test-color'];
                                new Vue({
                                    el: '#mcTestColorApp',
                                    data: {
                                        mcTestColor:mcTestColor,
                                        /*建议自己做个样式 引入到index-layui.html*/
                                        colorOpts:[
                                            {k:"",l:"请选择",v:"请选择"},
                                            {k:"red,blue,black",l:"红蓝黑",v:"<div class='mc-test-color-panel'><span style='background-color:red;'></span><span style='background-color:blue;'></span><span style='background-color:black;'></span></div>"},
                                            {k:"red,green,white",l:"红绿白",v:"<div class='mc-test-color-panel'><span style='background-color:red;'></span><span style='background-color:green;'></span><span style='background-color:white;'></span></div>"}
                                        ]
                                    },
                                    methods:{
                                        changeMe:function (newValue) {
                                            MAGICAL_CODER_API.changeAttr({node:focusNode,name:'my-test-color',value:newValue,triggerChange:false});
                                        }
                                    }
                                })
                            }
                        }
                    },
                    {type:"html",
                        category:"可编辑表格",
                        callback:{
                            htmlCallback:function(param){
                                var elHtml = "<mc-static-edit-table  @change-data='changeValue' :data='mcDataList' :fit='true' :show-header='true' border :row-size='rowSize'><mc-static-edit-table-column prop='x' label='地区' edit-type='editable-textarea'></mc-static-edit-table-column><mc-static-edit-table-column format-type='number' prop='y' label='销量' edit-type='editable-textarea'></mc-static-edit-table-column></mc-static-edit-table>";
                                var operateHtml = "<div class='layui-col-xs4'><el-button @click='addRow'>增加一行</el-button></div><div class='layui-col-xs4'><el-button @click='deleteRow'>删除一行</el-button></div>";
                                return '<div class="layui-row layui-col-space12" id="mcTestTableApp"><div class="layui-col-xs4">数据</div>'+operateHtml+'<div class="layui-col-xs12">'+elHtml+'</div></div>'
                            },
                            render:function (param) {
                                //从magicalCoder获取当前聚焦节点的属性值 准备传入vue里
                                var config = param.config,focusNode=param.focusNode;
                                var mcDataList = focusNode.attributes['my-test-table'];
                                 var rowSize = 0;
                                if(mcDataList){
                                    mcDataList = JSON.parse(mcDataList.replace(/&quot;/g,"\""));
                                    rowSize = mcDataList.length;
                                }else{
                                    mcDataList = null;
                                }

                                new Vue({
                                    el: '#mcTestTableApp',
                                    data: {
                                        rowSize:rowSize,
                                        mcDataList:mcDataList,
                                    },
                                    methods:{
                                        changeValue:function (newValue) {
                                            MAGICAL_CODER_API.changeAttr({node:focusNode,name:'my-test-table',value:JSON.stringify(newValue),triggerChange:false});
                                        },
                                        addRow:function(){
                                            this.rowSize++;
                                        },
                                        deleteRow:function(){
                                            this.rowSize--;
                                        }
                                    }
                                })
                            }
                        }
                    },
                    /*组件是否可见*/
                    {type:"select",clearAttr:false,oneLine:true ,change:"attr",attrName:'my-test-attr3'   ,title:'',tooltip:"",options:[{"1":"1"},{"2":"2"}],category:"统一" ,hide:true},
                ],
                [
                    /*如何多个属性一行配置 这个主要是通过lineStyle样式来处理*/
                    {type:"select",clearAttr:false,oneLine:false ,change:"attr",attrName:'my-test-attr1'   ,title:'堆在一行',tooltip:"",options:[{"1":"1"},{"2":"2"}],lineStyle:"width:40%;display:inline-block;",category:"统一"  },
                    {type:"select",clearAttr:false,oneLine:true ,change:"attr",attrName:'my-test-attr2'   ,title:'',tooltip:"",options:[{"1":"1"},{"2":"2"}],lineStyle:"width:56%;display:inline-block;margin-left:4%",category:"统一" },
                ]
            ]
        }
    ]
});
