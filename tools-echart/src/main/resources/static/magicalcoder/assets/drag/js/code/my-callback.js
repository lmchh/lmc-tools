/*回调重写*/
MagicalCallback.prototype.after_start = function (param) {
    var api = param.api;
    MAGICAL_JS_CODE.setCodeApi(api);
    MAGICAL_JS_CODE.afterStart(api);
    MAGICAL_JS_CODE.setVariable()
}
/*暂存按钮*/
MagicalCallback.prototype.save_html = function (param) {
    MAGICAL_JS_CODE.save();
    layer.msg("暂存成功")
}
MagicalCallback.prototype.reset_after = function (param) {
    MAGICAL_JS_CODE.resetFunction();
}
MagicalCallback.prototype.after_workspace_change=function(){
	new CodeSyntaxCheck().check();
    MAGICAL_JS_CODE.bindDomChangeValueEvent();
}
MagicalCallback.prototype.after_click_dom=function(param){
	var api = MAGICAL_CODER_API;
	var focusNode = api.getFocusNode();
	if(focusNode){
		var tagName = focusNode.magicalCoder.tagName;
		if(tagName=='input'||tagName=='textarea'){
			api.disableKeyboardEvent();
		}
	}
}
