/*每一种Ui的个性处理 比如各种组件初始化 重绘 注意剩余接口在iframe-ui-end.js中*/
function IframeUi() {
    this.ieVersion = ieVersion();
    this.vueData = {};
    this.vueMethod = this.api().getResetVueMethod();
    this.vueMounted = 'function(){}';
    this.defaultCss = "";//默认样式
    this.css = this.defaultCss;
    this.javascript="";
    //是否开启javascript调试
    this.debug = false;
    this.vueDataReg = new RegExp("var vueData = \\{([\\s\\S]*?)\\};\n*var vueMethod","g");
    this.vueMethodReg = new RegExp("var vueMethod = \\{([\\s\\S]*?)\\};\n*var vueMounted","g");
    this.vueMountedReg = new RegExp("var vueMounted = ([\\s\\S]*?)\n*\\/\\*注意以上代码由系统维护,非专业人士请勿修改\\*\\/","g");
    this.splitStr = ",/*别删我*/\n";
    //此正在匹配函数
    this.functionReg = new RegExp("\"?(\\w+)\"?\\s*:\\s*([\\s\\S]*)","g");
    /*脚本编辑器的数据functionName:html 方法名:脚本编辑器的可恢复数据*/
    this.magicalJsCodeData = {}
    /*工作区画布基础样式设置 key:样式名 value:样式值*/
    this.canvasStyle = {}
    this.construct();
}
IframeUi.prototype.inject = function(SINGLETON){
    this.preInject(SINGLETON);
}
//提供一系列动态操作脚本的接口 您可以调用API自由控制脚本内容
IframeUi.prototype.api = function(){
    return this.vueApi();//具体有哪些可参考ifram-ui-end.js的IframeUi.prototype.vueApi
}
/*此方法不要改名 初始化组件的方法 每次代码重绘 会调用此方法 来重新初始化组件*/
IframeUi.prototype.render=function (param) {
//    console.log(param.trigger.triggerChange)
    var c = param.trigger.triggerChange;
    var focusNode = this.magicalApi.getFocusNode();

    if(param.globalConstant.settings.other.speed.fastRender &&
        ( c =='changeAttr' || c == 'centerDrop' || c == 'leftDrop'|| c== 'changeText' || c == 'appendText'
        ||c == 'delete' || c == 'changeStyles' /*|| c == 'duplicate'*/)
    ){
        this.fastRender(param);
    }else{
        if(this.useMagicalCoderRender(param)){
            this.magicalCoderComponentsRender(param);
        }else{
            this.slowRender(param);
        }
    }
    //别管了 调用即可
    param.jsonFactory.resetTrigger();
}
/*把所有html拿出来重绘一遍，简单粗暴，但是当界面结构里有很多ajax请求，就可能会很慢 但是很保险 页面出错率0*/
IframeUi.prototype.slowRender = function(param){
//    console.log("全量刷新")
   var html=param.html,jsonFactory=param.jsonFactory,globalConstant=param.globalConstant;
    if(html==null){
        return;
    }
    var magicalCoderCss = this.autoCreateMagicalCss();
    magicalCoderCss.innerHTML = this.getCss();//设置样式内容

    var magicalDragScene = this.autoCreateRootDom();
    //由于vue框架不支持body做根 并且<template></template>标签在html里会自动变成虚无 所以我们正则替换一下 自动加上让vue渲染
    var html = this.realHtml(html);
    magicalDragScene.innerHTML = html;

    var javascript = this.getJavascript();
    try {
        if(this.debug){
            javascript = "debugger\n" + javascript;
        }
        //使用eval才行 使用全局eval 是全局作用域，更有利于window对象可用
        eval(javascript);
    }catch (e) {
        var msgHtml = '<div class="layui-row"><div class="layui-col-xs12" style="font-size: 17px; font-weight: bolder;">'+e.message+'</div><div class="layui-col-xs12" style="color: rgb(221, 32, 32);">'+e.stack+'</div></div>';
        parent.window.layui.layer.open({
            type:1,
            title:"您编写的脚本编译错误-非布局器报错",
            area: ['800px', '400px'],
            shadeClose:true,
            content:msgHtml,
        })
        console.log(e);
        eval("var Ctor = Vue.extend({});new Ctor().$mount('#magicalDragScene');new IframeComponents().execute();");//兼容一下js错误 给出渲染界面
    }
    //做一些优化 比如删除一些不需要的结构
    this.fixDynamicDomAfterRender({container:$("#magicalDragScene")});
}
/*
    快速render极致性能：局部更新的方式 建个临时结构来接管 渲染完替换原始位置
    虽然很快，但是针对于界面上有
    @ echarts组件 可以给固定宽度 那就不会空白了
 */
IframeUi.prototype.fastRender=function (param) {
//    console.log("局部刷新")
    var focusNode = this.magicalApi.getFocusNode();
    var html = focusNode!=null ? this.magicalApi.nodeToHtml({node:focusNode,pure:true}):"";

    var c = param.trigger.triggerChange,triggerOperateNodes=param.trigger.triggerOperateNodes;
    //原始结构
    var originElem = null;
    if(c == 'leftDrop'){
        originElem = $("#magicalDragScene").find(".magicalcoder-page-drag-item-label");
    }else if(c == 'delete'){
        if(triggerOperateNodes){
            for(var i=0;i<triggerOperateNodes.length;i++){
                var elem = this.magicalApi.getElemByMagicalCoderId({id:triggerOperateNodes[i].magicalCoder.id});
                if(elem){
                    elem.remove();
                }
            }
        }
        return;
    }else{
        originElem = this.magicalApi.getElemByMagicalCoderId({id:focusNode.magicalCoder.id});
    }


    //body里增加一个临时结构
    $("body").append("<div id='magicalDragSceneGhost'></div>");

    var magicalDragScene = $("#magicalDragSceneGhost");
    //由于vue框架不支持body做根 并且<template></template>标签在html里会自动变成虚无 所以我们正则替换一下 自动加上让vue渲染
    var html = "<template>"+html+"</template>";
    magicalDragScene[0].innerHTML=html;

    var javascript = this.getJavascript();

    try {
        //使用eval才行
        var newJs = javascript.replace("new Ctor().$mount('#magicalDragScene')","new Ctor().$mount('#magicalDragSceneGhost')");
        eval(newJs);
    }catch (e) {
        var msgHtml = '<div class="layui-row"><div class="layui-col-xs12" style="font-size: 17px; font-weight: bolder;">'+e.message+'</div><div class="layui-col-xs12" style="color: rgb(221, 32, 32);">'+e.stack+'</div></div>';
        parent.window.layui.layer.open({
            type:1,
            title:"您编写的脚本编译错误-非布局器报错",
            area: ['800px', '400px'],
            shadeClose:true,
            content:msgHtml,
        })
        console.log(e);
        eval("var Ctor = Vue.extend({});new Ctor().$mount('#magicalDragSceneGhost');");//兼容一下js错误 给出渲染界面
    }
    //做一些优化 比如删除一些不需要的结构
    this.fixDynamicDomAfterRender({container:$("#magicalDragSceneGhost")});
    var children =  $("#magicalDragSceneGhost").children();
    originElem.replaceWith(children);
    //删除临时结构即可
    $("#magicalDragSceneGhost").remove();
}
//修复一下动态创建的结构跑偏问题 有些ui设计的比较差 动态没用的结构很多，我们设置简介标签上的属性样式无法继承 所以就需要我们修复一下
IframeUi.prototype.fixDynamicDomAfterRender = function(param){
    var container = param.container;
     //写好标记死结构 就是为了打开弹窗的
    var dragMcPane = this.jsonFactory.api().pubGetDragMcPaneName();
    var magicalCoderIdAttrName = this.jsonFactory.api().getMagicalCoderIdAttrName();
    var _t = this;
    //弹窗
    container.find(".v-transfer-dom").removeClass(dragMcPane);
    container.find(".ivu-modal-wrap").each(function(idx,item){
        var body = $(this).find(".ivu-modal-body");
        if(body.length>0){// 你为啥开始不创建这个结构烦死
            var elDialogWrapper = $(this).parent();
            var mcId = elDialogWrapper.attr(magicalCoderIdAttrName);
            elDialogWrapper.removeAttr(magicalCoderIdAttrName)
            body.attr(magicalCoderIdAttrName,mcId);
            body.addClass(dragMcPane);
            //刷新拖拽事件
            _t.center.refreshDragEvent();
        }
    })
}

IframeUi.prototype.getJavascript = function(){
    return this.getVueScript();
}
IframeUi.prototype.setJavascript = function(javascript){
    this.setVueScript(javascript);
}
/*扩展可选图标icon*/
IframeUi.prototype.iconList = function(){
    if(window.location.href.indexOf("from=icon_list")!=-1){
        $("body").css("overflow-y","scroll")
        var util = this.util();
        var iconArr = ["ios-ionic","ios-arrow-forward","md-qr-scanner","md-copy","md-checkmark","md-code-working","md-code","ios-arrow-down","ios-arrow-up","ios-add","md-add","ios-add-circle","ios-add-circle-outline","md-add-circle","ios-alarm","ios-alarm-outline","md-alarm","ios-albums","ios-albums-outline","md-albums","ios-alert","ios-alert-outline","md-alert","ios-american-football","ios-american-football-outline","md-american-football","ios-analytics","ios-analytics-outline","md-analytics","logo-android","logo-angular","ios-aperture","ios-aperture-outline","md-aperture","logo-apple","ios-apps","ios-apps-outline","md-apps","ios-appstore","ios-appstore-outline","md-appstore","ios-archive","ios-archive-outline","md-archive","ios-arrow-back","md-arrow-back","md-arrow-down","ios-arrow-dropdown","md-arrow-dropdown","ios-arrow-dropdown-circle","md-arrow-dropdown-circle","ios-arrow-dropleft","md-arrow-dropleft","ios-arrow-dropleft-circle","md-arrow-dropleft-circle","ios-arrow-dropright","md-arrow-dropright","ios-arrow-dropright-circle","md-arrow-dropright-circle","ios-arrow-dropup","md-arrow-dropup","ios-arrow-dropup-circle","md-arrow-dropup-circle","md-arrow-forward","ios-arrow-round-back","md-arrow-round-back","ios-arrow-round-down","md-arrow-round-down","ios-arrow-round-forward","md-arrow-round-forward","ios-arrow-round-up","md-arrow-round-up","md-arrow-up","ios-at","ios-at-outline","md-at","ios-attach","md-attach","ios-backspace","ios-backspace-outline","md-backspace","ios-barcode","ios-barcode-outline","md-barcode","ios-baseball","ios-baseball-outline","md-baseball","ios-basket","ios-basket-outline","md-basket","ios-basketball","ios-basketball-outline","md-basketball","ios-battery-charging","md-battery-charging","ios-battery-dead","md-battery-dead","ios-battery-full","md-battery-full","ios-beaker","ios-beaker-outline","md-beaker","ios-beer","ios-beer-outline","md-beer","ios-bicycle","md-bicycle","logo-bitcoin","ios-bluetooth","md-bluetooth","ios-boat","ios-boat-outline","md-boat","ios-body","ios-body-outline","md-body","ios-bonfire","ios-bonfire-outline","md-bonfire","ios-book","ios-book-outline","md-book","ios-bookmark","ios-bookmark-outline","md-bookmark","ios-bookmarks","ios-bookmarks-outline","md-bookmarks","ios-bowtie","ios-bowtie-outline","md-bowtie","ios-briefcase","ios-briefcase-outline","md-briefcase","ios-browsers","ios-browsers-outline","md-browsers","ios-brush","ios-brush-outline","md-brush","logo-buffer","ios-bug","ios-bug-outline","md-bug","ios-build","ios-build-outline","md-build","ios-bulb","ios-bulb-outline","md-bulb","ios-bus","ios-bus-outline","md-bus","ios-cafe","ios-cafe-outline","md-cafe","ios-calculator","ios-calculator-outline","md-calculator","ios-calendar","ios-calendar-outline","md-calendar","ios-call","ios-call-outline","md-call","ios-camera","ios-camera-outline","md-camera","ios-car","ios-car-outline","md-car","ios-card","ios-card-outline","md-card","ios-cart","ios-cart-outline","md-cart","ios-cash","ios-cash-outline","md-cash","ios-chatboxes","ios-chatboxes-outline","md-chatboxes","ios-chatbubbles","ios-chatbubbles-outline","md-chatbubbles","ios-checkbox","ios-checkbox-outline","md-checkbox","md-checkbox-outline","ios-checkmark","ios-checkmark-circle","ios-checkmark-circle-outline","md-checkmark-circle","md-checkmark-circle-outline","logo-chrome","ios-clipboard","ios-clipboard-outline","md-clipboard","ios-clock","ios-clock-outline","md-clock","ios-close","md-close","ios-close-circle","ios-close-circle-outline","md-close-circle","ios-closed-captioning","ios-closed-captioning-outline","md-closed-captioning","ios-cloud","ios-cloud-outline","md-cloud","ios-cloud-circle","ios-cloud-circle-outline","md-cloud-circle","ios-cloud-done","ios-cloud-done-outline","md-cloud-done","ios-cloud-download","ios-cloud-download-outline","md-cloud-download","md-cloud-outline","ios-cloud-upload","ios-cloud-upload-outline","md-cloud-upload","ios-cloudy","ios-cloudy-outline","md-cloudy","ios-cloudy-night","ios-cloudy-night-outline","md-cloudy-night","ios-code","ios-code-download","md-code-download","ios-code-working","logo-codepen","ios-cog","ios-cog-outline","md-cog","ios-color-fill","ios-color-fill-outline","md-color-fill","ios-color-filter","ios-color-filter-outline","md-color-filter","ios-color-palette","ios-color-palette-outline","md-color-palette","ios-color-wand","ios-color-wand-outline","md-color-wand","ios-compass","ios-compass-outline","md-compass","ios-construct","ios-construct-outline","md-construct","ios-contact","ios-contact-outline","md-contact","ios-contacts","ios-contacts-outline","md-contacts","ios-contract","md-contract","ios-contrast","md-contrast","ios-copy","ios-copy-outline","ios-create","ios-create-outline","md-create","ios-crop","ios-crop-outline","md-crop","logo-css3","ios-cube","ios-cube-outline","md-cube","ios-cut","ios-cut-outline","md-cut","logo-designernews","ios-desktop","ios-desktop-outline","md-desktop","ios-disc","ios-disc-outline","md-disc","ios-document","ios-document-outline","md-document","ios-done-all","md-done-all","ios-download","ios-download-outline","md-download","logo-dribbble","logo-dropbox","ios-easel","ios-easel-outline","md-easel","ios-egg","ios-egg-outline","md-egg","logo-euro","ios-exit","ios-exit-outline","md-exit","ios-expand","md-expand","ios-eye","ios-eye-outline","md-eye","ios-eye-off","ios-eye-off-outline","md-eye-off","logo-facebook","ios-fastforward","ios-fastforward-outline","md-fastforward","ios-female","md-female","ios-filing","ios-filing-outline","md-filing","ios-film","ios-film-outline","md-film","ios-finger-print","md-finger-print","ios-flag","ios-flag-outline","md-flag","ios-flame","ios-flame-outline","md-flame","ios-flash","ios-flash-outline","md-flash","ios-flask","ios-flask-outline","md-flask","ios-flower","ios-flower-outline","md-flower","ios-folder","ios-folder-outline","md-folder","ios-folder-open","ios-folder-open-outline","md-folder-open","ios-football","ios-football-outline","md-football","logo-foursquare","logo-freebsd-devil","ios-funnel","ios-funnel-outline","md-funnel","ios-game-controller-a","ios-game-controller-a-outline","md-game-controller-a","ios-game-controller-b","ios-game-controller-b-outline","md-game-controller-b","ios-git-branch","md-git-branch","ios-git-commit","md-git-commit","ios-git-compare","md-git-compare","ios-git-merge","md-git-merge","ios-git-network","md-git-network","ios-git-pull-request","md-git-pull-request","logo-github","ios-glasses","ios-glasses-outline","md-glasses","ios-globe","ios-globe-outline","md-globe","logo-google","logo-googleplus","ios-grid","ios-grid-outline","md-grid","logo-hackernews","ios-hammer","ios-hammer-outline","md-hammer","ios-hand","ios-hand-outline","md-hand","ios-happy","ios-happy-outline","md-happy","ios-headset","ios-headset-outline","md-headset","ios-heart","ios-heart-outline","md-heart","md-heart-outline","ios-help","md-help","ios-help-buoy","ios-help-buoy-outline","md-help-buoy","ios-help-circle","ios-help-circle-outline","md-help-circle","ios-home","ios-home-outline","md-home","logo-html5","ios-ice-cream","ios-ice-cream-outline","md-ice-cream","ios-image","ios-image-outline","md-image","ios-images","ios-images-outline","md-images","ios-infinite","ios-infinite-outline","md-infinite","ios-information","md-information","ios-information-circle","ios-information-circle-outline","md-information-circle","logo-instagram","ios-ionic-outline","md-ionic","ios-ionitron","ios-ionitron-outline","md-ionitron","logo-javascript","ios-jet","ios-jet-outline","md-jet","ios-key","ios-key-outline","md-key","ios-keypad","ios-keypad-outline","md-keypad","ios-laptop","md-laptop","ios-leaf","ios-leaf-outline","md-leaf","ios-link","ios-link-outline","md-link","logo-linkedin","ios-list","md-list","ios-list-box","ios-list-box-outline","md-list-box","ios-locate","ios-locate-outline","md-locate","ios-lock","ios-lock-outline","md-lock","ios-log-in","md-log-in","ios-log-out","md-log-out","ios-magnet","ios-magnet-outline","md-magnet","ios-mail","ios-mail-outline","md-mail","ios-mail-open","ios-mail-open-outline","md-mail-open","ios-male","md-male","ios-man","ios-man-outline","md-man","ios-map","ios-map-outline","md-map","logo-markdown","ios-medal","ios-medal-outline","md-medal","ios-medical","ios-medical-outline","md-medical","ios-medkit","ios-medkit-outline","md-medkit","ios-megaphone","ios-megaphone-outline","md-megaphone","ios-menu","ios-menu-outline","md-menu","ios-mic","ios-mic-outline","md-mic","ios-mic-off","ios-mic-off-outline","md-mic-off","ios-microphone","ios-microphone-outline","md-microphone","ios-moon","ios-moon-outline","md-moon","ios-more","ios-more-outline","md-more","ios-move","md-move","ios-musical-note","ios-musical-note-outline","md-musical-note","ios-musical-notes","ios-musical-notes-outline","md-musical-notes","ios-navigate","ios-navigate-outline","md-navigate","ios-no-smoking","ios-no-smoking-outline","md-no-smoking","logo-nodejs","ios-notifications","ios-notifications-outline","md-notifications","ios-notifications-off","ios-notifications-off-outline","md-notifications-off","md-notifications-outline","ios-nuclear","ios-nuclear-outline","md-nuclear","ios-nutrition","ios-nutrition-outline","md-nutrition","logo-octocat","ios-open","ios-open-outline","md-open","ios-options","ios-options-outline","md-options","ios-outlet","ios-outlet-outline","md-outlet","ios-paper","ios-paper-outline","md-paper","ios-paper-plane","ios-paper-plane-outline","md-paper-plane","ios-partly-sunny","ios-partly-sunny-outline","md-partly-sunny","ios-pause","ios-pause-outline","md-pause","ios-paw","ios-paw-outline","md-paw","ios-people","ios-people-outline","md-people","ios-person","ios-person-outline","md-person","ios-person-add","ios-person-add-outline","md-person-add","ios-phone-landscape","md-phone-landscape","ios-phone-portrait","md-phone-portrait","ios-photos","ios-photos-outline","md-photos","ios-pie","ios-pie-outline","md-pie","ios-pin","ios-pin-outline","md-pin","ios-pint","ios-pint-outline","md-pint","logo-pinterest","ios-pizza","ios-pizza-outline","md-pizza","ios-plane","ios-plane-outline","md-plane","ios-planet","ios-planet-outline","md-planet","ios-play","ios-play-outline","md-play","logo-playstation","ios-podium","ios-podium-outline","md-podium","ios-power","ios-power-outline","md-power","ios-pricetag","ios-pricetag-outline","md-pricetag","ios-pricetags","ios-pricetags-outline","md-pricetags","ios-print","ios-print-outline","md-print","ios-pulse","ios-pulse-outline","md-pulse","logo-python","ios-qr-scanner","ios-quote","ios-quote-outline","md-quote","ios-radio","ios-radio-outline","md-radio","ios-radio-button-off","md-radio-button-off","ios-radio-button-on","md-radio-button-on","ios-rainy","ios-rainy-outline","md-rainy","ios-recording","ios-recording-outline","md-recording","logo-reddit","ios-redo","ios-redo-outline","md-redo","ios-refresh","md-refresh","ios-refresh-circle","ios-refresh-circle-outline","md-refresh-circle","ios-remove","md-remove","ios-remove-circle","ios-remove-circle-outline","md-remove-circle","ios-reorder","md-reorder","ios-repeat","md-repeat","ios-resize","md-resize","ios-restaurant","ios-restaurant-outline","md-restaurant","ios-return-left","md-return-left","ios-return-right","md-return-right","ios-reverse-camera","ios-reverse-camera-outline","md-reverse-camera","ios-rewind","ios-rewind-outline","md-rewind","ios-ribbon","ios-ribbon-outline","md-ribbon","ios-rose","ios-rose-outline","md-rose","logo-rss","ios-sad","ios-sad-outline","md-sad","logo-sass","ios-school","ios-school-outline","md-school","ios-search","ios-search-outline","md-search","ios-send","ios-send-outline","md-send","ios-settings","ios-settings-outline","md-settings","ios-share","ios-share-outline","md-share","ios-share-alt","ios-share-alt-outline","md-share-alt","ios-shirt","ios-shirt-outline","md-shirt","ios-shuffle","md-shuffle","ios-skip-backward","ios-skip-backward-outline","md-skip-backward","ios-skip-forward","ios-skip-forward-outline","md-skip-forward","logo-skype","logo-snapchat","ios-snow","ios-snow-outline","md-snow","ios-speedometer","ios-speedometer-outline","md-speedometer","ios-square","ios-square-outline","md-square","md-square-outline","ios-star","ios-star-outline","md-star","ios-star-half","md-star-half","md-star-outline","ios-stats","ios-stats-outline","md-stats","logo-steam","ios-stopwatch","ios-stopwatch-outline","md-stopwatch","ios-subway","ios-subway-outline","md-subway","ios-sunny","ios-sunny-outline","md-sunny","ios-swap","md-swap","ios-switch","ios-switch-outline","md-switch","ios-sync","md-sync","ios-tablet-landscape","md-tablet-landscape","ios-tablet-portrait","md-tablet-portrait","ios-tennisball","ios-tennisball-outline","md-tennisball","ios-text","ios-text-outline","md-text","ios-thermometer","ios-thermometer-outline","md-thermometer","ios-thumbs-down","ios-thumbs-down-outline","md-thumbs-down","ios-thumbs-up","ios-thumbs-up-outline","md-thumbs-up","ios-thunderstorm","ios-thunderstorm-outline","md-thunderstorm","ios-time","ios-time-outline","md-time","ios-timer","ios-timer-outline","md-timer","ios-train","ios-train-outline","md-train","ios-transgender","md-transgender","ios-trash","ios-trash-outline","md-trash","ios-trending-down","md-trending-down","ios-trending-up","md-trending-up","ios-trophy","ios-trophy-outline","md-trophy","logo-tumblr","logo-tux","logo-twitch","logo-twitter","ios-umbrella","ios-umbrella-outline","md-umbrella","ios-undo","ios-undo-outline","md-undo","ios-unlock","ios-unlock-outline","md-unlock","logo-usd","ios-videocam","ios-videocam-outline","md-videocam","logo-vimeo","ios-volume-down","md-volume-down","ios-volume-mute","md-volume-mute","ios-volume-off","md-volume-off","ios-volume-up","md-volume-up","ios-walk","md-walk","ios-warning","ios-warning-outline","md-warning","ios-watch","md-watch","ios-water","ios-water-outline","md-water","logo-whatsapp","ios-wifi","ios-wifi-outline","md-wifi","logo-windows","ios-wine","ios-wine-outline","md-wine","ios-woman","ios-woman-outline","md-woman","logo-wordpress","logo-xbox","logo-yahoo","logo-yen","logo-youtube","ios-loading"]
        var html = [];
        html.push('<ul id="magicalDragScene" class="magicalcoder-extend-icons magicalcoder-antd-extend-icons">')
        for(var i=0;i<iconArr.length;i++){
            html.push("<li><icon type='"+iconArr[i]+"'></icon></li>")
        }
        html.push('</ul>')
        var magicalDragScene = this.autoCreateRootDom();
        magicalDragScene.innerHTML = html.join('');
        var Ctor = Vue.extend({});
        new Ctor().$mount('#magicalDragScene');

        var lis = document.getElementsByTagName("li");
        for(var i=0;i<lis.length;i++){
            lis[i].addEventListener('click',function () {
                var icon = this.childNodes[0]
                var active = true;
                if(icon.className.indexOf("active")==-1){
                    active = false;
                }
                var actives = document.getElementsByClassName("active");
                for(var j=0;j<actives.length;j++){
                    util.removeClass(actives[j],"active");
                }
                if(!active){
                    icon.className = icon.className +" active";
                }

            })
        }
        return true;
    }
    return false;
}
//下载按钮下载的内容
IframeUi.prototype.download = function(html){
    var source = [
        '<meta charset="UTF-8">','<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">',
        '<title>element-由www.magicalcoder.com可视化布局器生成</title>',
        '<script type="text/javascript" src="assets/drag/js/user/iframe/iview/4.4.0/components/import.js"></script>'
    ]
    //设置样式
    var style = [];
    for(var key in this.canvasStyle){
        style.push(key+":"+this.canvasStyle[key]);
    }
    var css = '\n<style>\n'+this.getCss()+'\n</style>\n';
    var head = '<head>'+source.join('\n')+css+'\n</head>\n';
    var body = '<body style="'+style.join(";").replace(/\"/g,"'")+'">\n'+this.realHtml(html)+'\n<script>\n'+this.getJavascript()+'\n</script>\n</body>\n';
    return {
        htmlBefore:"<!DOCTYPE html><html>\n<!--代码由www.magicalcoder.com可视化布局器拖拽生成 资源请自行下载到本地-->\n",
        head:head,
        body:body,
        htmlEnd:"\n</html>",
    }
}
