/*test组件:MagicalCoder参考lib\mc\magicalcoder.js*/
MagicalCoder.install({
    /*左侧可拖拽的源*/
    dragItems:[
        {
            name:"线性图表",
            icon:"ri-line-chart-line",
            search:true,
            children:[
                {
                    name:"线性",
                    children:[
                        {
                            name:"线性图",
                            icon:"ri-line-chart-line",
                            html:"<div class='mc-echarts-line' style='height:400px;'></div>"
                        }
                    ]
                }

            ]
        }
   ],
    /*自定义组件和属性*/
    components:[
        {
            identifier:"mc-echarts-line",
            properties:{
                name:"线性图",
                dragInto:false,
                duplicate:true,
                assistDuplicate:true,
                copy:true,
                paste:false,
                canDelete:true,
                assistDelete:true,
                afterDragDrop:{refresh:true},
                afterResize:{refresh:true}
            },
            attributes:[
                {
                    title:"属性",
                    active:true,
                    width:"100%",
                    content:[
                        {
                            type:"html",
                            category:"数据",
                            callback:{
                                htmlCallback:function(param){
                                    return ''+
                                        '<div class="layui-row layui-col-space3" id="mcEchartsDataManagerApp">'+
                                            '<template>'+
                                            '<div class="layui-col-xs12">'+
                                                '<el-row>'+
                                                    '<el-col :span="24">'+
                                                        '<el-radio-group v-model="echarts.dataManager.dataType"  size="mini" style="margin-bottom:10px">'+
                                                          '<el-radio-button :label="1">已知数据源</el-radio-button>'+
                                                          '<el-radio-button :label="2">未知数据源</el-radio-button>'+
                                                          '<el-radio-button :label="3">自定义数据</el-radio-button>'+
                                                        '</el-radio-group>'+
                                                         '<el-form v-if="echarts.dataManager.dataType==1" label-position="left" size="mini" label-width="60px">'+
                                                              '<el-form-item label="数据源">'+
                                                                '<el-select v-model="echarts.dataManager.choose1.sourceApi" @change="changeSourceApi">'+
                                                                    '<el-option v-for="(item,idx) in sourceApiList" :key="item.title" :label="item.title" :value="item.url"></el-option>'+
                                                                '</el-select>'+
                                                            '</el-form-item>'+
                                                            '<el-form-item label="X轴">'+
                                                                '<el-select v-model="echarts.dataManager.choose1.x">'+
                                                                      '<el-option v-for="(item,idx) in xList" :key="item.name" :label="item.title" :value="item.name"></el-option>'+
                                                                '</el-select>'+
                                                            '</el-form-item>'+
                                                            '<el-form-item label="Y轴">'+
                                                                '<el-select v-model="echarts.dataManager.choose1.y">'+
                                                                    '<el-option v-for="(item,idx) in yList" :key="item.name" :label="item.title" :value="item.name"></el-option>'+
                                                                '</el-select>'+
                                                            '</el-form-item>'+
                                                        '</el-form>'+
                                                        '<el-form v-if="echarts.dataManager.dataType==2" label-position="left" size="mini" label-width="60px">'+
                                                            '<el-form-item label="数据源">'+
                                                                '<el-input v-model="echarts.dataManager.choose2.sourceApi"></el-input>'+
                                                            '</el-form-item>'+
                                                            '<el-form-item label="X轴">'+
                                                                '<el-input v-model="echarts.dataManager.choose2.x"></el-input>'+
                                                            '</el-form-item>'+
                                                            '<el-form-item label="Y轴">'+
                                                                '<el-input v-model="echarts.dataManager.choose2.y"></el-input>'+
                                                            '</el-form-item>'+
                                                        '</el-form>'+
                                                        '<el-form v-if="echarts.dataManager.dataType==3" label-position="left" size="mini" label-width="0px">'+
                                                            '<el-form-item>'+
                                                                '<mc-static-edit-table :fit="true" :show-header="true" border :row-size="rowSize" :data="choose3Data">'+
                                                                     '<mc-static-edit-table-column prop="x" label="X轴" readonly="false" edit-type="editable-textarea"></mc-static-edit-table-column>'+
                                                                     '<mc-static-edit-table-column prop="y" label="Y轴" readonly="false" edit-type="editable-textarea" format-type="number"></mc-static-edit-table-column>'+
                                                                     '<el-table-column label="操作">'+
                                                                            '<template slot-scope="scope">'+
                                                                                '<el-button mc-type="column-el-button" size="mini" type="danger" @click="deleteRow(scope.$index, scope.row)">删除</el-button>'+
                                                                            '</template>'+
                                                                     '</el-table-column>'+
                                                                '</mc-static-edit-table>'+
                                                            '</el-form-item>'+
                                                             '<el-form-item>'+
                                                                '<el-button-group>'+
                                                                    '<el-button @click="addRow" size="mini">增加一行</el-button>'+
                                                                    '<el-button @click="saveData" size="mini">保存</el-button>'+
                                                                '</el-button-group>'+
                                                            '</el-form-item>'+
                                                        '</el-form>'+
                                                    '</el-col>'+
                                                '</el-row>'+
                                            '</div>'+
                                            '</template>'+
                                        '</div>'+
'';
                                },
                                render:function (param) {
                                    setTimeout(function(){
                                        //从magicalCoder获取当前聚焦节点的属性值 准备传入vue里
                                        var config = param.config,focusNode=param.focusNode;
                                        var mcEchartsAttr = focusNode.attributes['mc-echarts-data-manager'];
                                        var rowSize = 0,choose3Data=[];
                                        if(!mcEchartsAttr){
                                            mcEchartsAttr = {
                                                 dataManager:{
                                                    dataType:1,
                                                    choose1:{x:"",y:"",sourceApi:""},
                                                    choose2:{x:"",y:"",sourceApi:""},
                                                    choose3:{data:[]}
                                                 }
                                            }
                                        }else{
                                            mcEchartsAttr = {dataManager:JSON.parse(mcEchartsAttr.replace(/&quot;/g,"\"").replace(/&lt;/g,"<").replace(/&gt;/g,">"))};
                                            rowSize = mcEchartsAttr.dataManager.choose3.data.length;
                                            choose3Data = JSON.parse(JSON.stringify(mcEchartsAttr.dataManager.choose3.data));
                                        }
                                        new Vue({
                                                el: '#mcEchartsDataManagerApp',
                                                data: {
                                                    echarts: mcEchartsAttr,
                                                    sourceApiList:[],
                                                    sourceApiMap :{},
                                                    xList:[],
                                                    yList:[],
                                                    rowSize:rowSize,
                                                    choose3Data:choose3Data
                                                },
                                                watch:{
                                                    echarts:{
                                                        handler: function(newVal, oldVal) {
                                                            //监听到属性发生变化 那么修改属性 同时触发渲染
                                                            var triggerChange = false;
                                                            if(this.echarts.dataManager.dataType==1){
                                                                triggerChange = this.echarts.dataManager.choose1.sourceApi && this.echarts.dataManager.choose1.x && this.echarts.dataManager.choose1.y;
                                                            }else if(this.echarts.dataManager.dataType==2){
                                                                triggerChange = this.echarts.dataManager.choose2.sourceApi && this.echarts.dataManager.choose2.x && this.echarts.dataManager.choose2.y;
                                                            }else if(this.echarts.dataManager.dataType==3){
                                                                triggerChange = true;
                                                            }
                                                            MAGICAL_CODER_API.changeAttr({node:focusNode,name:"mc-echarts-data-manager",value:JSON.stringify(newVal.dataManager),triggerChange:triggerChange})
                                                        },
                                                        deep: true
                                                    }
                                                },
                                                methods:{
                                                    changeSourceApi:function(value){
                                                        this.echarts.dataManager.choose1.x="";
                                                        this.echarts.dataManager.choose1.y="";
                                                        this.xList = this.sourceApiMap[value].x;
                                                        this.yList = this.sourceApiMap[value].y;
                                                    },
                                                    addRow:function(){
                                                        this.rowSize++;
                                                    },
                                                    deleteRow:function(idx,rowData){
                                                        this.choose3Data.splice(idx,1)
                                                        this.rowSize--;
                                                    },
                                                    saveData:function(){
                                                        //同步一下
                                                        this.echarts.dataManager.choose3.data = JSON.parse(JSON.stringify(this.choose3Data));
                                                    }
                                                },
                                                mounted:function(){
                                                    var _t = this;
                                                    MagicalCoder.ajax(true,{},"assets/drag/js/data/echarts/xy/api.json",function(originParam,data){
                                                        if(data.code==0){
                                                            var list = data.data;
                                                            for(var i=0;i<list.length;i++){
                                                                var item = list[i];
                                                                _t.sourceApiList.push(item);
                                                                _t.sourceApiMap[item.url] = item.fields;
                                                            }
                                                            var sourceApi = _t.echarts.dataManager.choose1.sourceApi;
                                                            if(sourceApi){
                                                                _t.xList = _t.sourceApiMap[sourceApi].x;
                                                                _t.yList = _t.sourceApiMap[sourceApi].y;
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                    },5)
                                }
                            }
                        },
                        {
                                type:"html",
                                category:"标题",
                                callback:{
                                    htmlCallback:function(param){
                                        return ''+
                                            '<div class="layui-row layui-col-space3" id="mcEchartsTitleApp">'+
                                                '<template>'+
                                                '<div class="layui-col-xs12">'+
                                                    '<el-form label-position="left" size="mini" label-width="100px">'+
                                                    	'<el-collapse accordion v-model="titleActive">'+
                                                            '<el-collapse-item name="1" title="主标题">'+
                                                                '<el-form-item label="文本"><mc-change-input :mc-value.sync="echarts.title.text" placeholder="请输入内容"></mc-change-input></el-form-item>'+
                                                                '<el-form-item label="文本超链接"><mc-change-input :mc-value.sync="echarts.title.link" placeholder="请输入内容"></el-input></el-form-item>'+
                                                                '<el-form-item label="链接打开方式"><mc-change-input :mc-value.sync="echarts.title.target" placeholder="请输入内容"></el-input></el-form-item>'+
                                                                '<el-form-item label="颜色"><el-color-picker v-model="echarts.title.textStyle.color"></el-color-picker></el-form-item>'+
                                                                '<el-form-item label="字体风格"><el-select v-model="echarts.title.textStyle.fontStyle"><el-option label="正常" value="normal"></el-option><el-option label="斜体1" value="italic"></el-option><el-option label="斜体2" value="oblique"></el-option></el-select></el-form-item>'+
                                                                '<el-form-item label="字体粗细"><el-select v-model="echarts.title.textStyle.fontWeight"><el-option label="正常" value="normal"></el-option><el-option label="加粗1" value="bold"></el-option><el-option label="加粗2" value="bolder"></el-option><el-option label="变细" value="lighter"></el-option></el-select></el-form-item>'+
                                                                '<el-form-item label="字体"><el-select v-model="echarts.title.textStyle.fontFamily"><el-option label="自己取名" value="sans-serif"></el-option><el-option label="自己取名" value="serif"></el-option><el-option label="自己取名" value="monospace"></el-option><el-option label="自己取名" value="Arial"></el-option><el-option label="自己取名" value="Courier New"></el-option></el-select></el-form-item>'+
                                                                '<el-form-item label="字体大小"><el-input-number v-model="echarts.title.textStyle.fontSize" step-strictly :step="1" :min="1" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="行高"><el-input-number v-model="echarts.title.textStyle.lineHeight" step-strictly :step="1" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="宽度"><el-input-number v-model="echarts.title.textStyle.width" step-strictly :step="1" :min="1" :max="500" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="高度"><el-input-number v-model="echarts.title.textStyle.height" step-strictly :step="1" :min="1" :max="500" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="描边颜色"><el-color-picker v-model="echarts.title.textStyle.textBorderColor"></el-color-picker></el-form-item>'+
                                                                '<el-form-item label="描边宽度"><el-input-number v-model="echarts.title.textStyle.textBorderWidth" step-strictly :step="0.5" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="阴影颜色"><el-color-picker v-model="echarts.title.textStyle.textShadowColor"></el-color-picker></el-form-item>'+
                                                                '<el-form-item label="阴影长度"><el-input-number v-model="echarts.title.textStyle.textShadowBlur" step-strictly :step="0.5" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="阴影X偏移"><el-input-number v-model="echarts.title.textStyle.textShadowOffsetX" step-strictly :step="0.5" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="阴影Y偏移"><el-input-number v-model="echarts.title.textStyle.textShadowOffsetY" step-strictly :step="0.5" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="超出截断"><el-select v-model="echarts.title.textStyle.overflow"><el-option label="截断" value="truncate"></el-option><el-option label="换行1" value="break"></el-option><el-option label="换行2" value="breakAll"></el-option></el-select></el-form-item>'+
                                                                '<el-form-item label="末尾文本"><mc-change-input :mc-value.sync="echarts.title.textStyle.ellipsis" placeholder="请输入内容"></el-input></el-form-item>'+
                                                                '<el-form-item label="高度截断"><mc-change-input :mc-value.sync="echarts.title.textStyle.lineOverflow" placeholder="请输入内容"></el-input></el-form-item>'+
                                                            '</el-collapse-item>'+
                                                            '<el-collapse-item name="2" title="子标题">'+
                                                                '<el-form-item label="文本"><mc-change-input :mc-value.sync="echarts.title.subtext" placeholder="请输入内容"></el-input></el-form-item>'+
                                                                '<el-form-item label="文本超链接"><mc-change-input :mc-value.sync="echarts.title.sublink" placeholder="请输入内容"></el-input></el-form-item>'+
                                                                '<el-form-item label="链接打开方式"><mc-change-input :mc-value.sync="echarts.title.subtarget" placeholder="请输入内容"></el-input></el-form-item>'+
                                                                '<el-form-item label="颜色"><el-color-picker v-model="echarts.title.subtextStyle.color"></el-color-picker></el-form-item>'+
                                                                '<el-form-item label="字体风格"><el-select v-model="echarts.title.subtextStyle.fontStyle"><el-option label="正常" value="normal"></el-option><el-option label="斜体1" value="italic"></el-option><el-option label="斜体2" value="oblique"></el-option></el-select></el-form-item>'+
                                                                '<el-form-item label="字体粗细"><el-select v-model="echarts.title.subtextStyle.fontWeight"><el-option label="正常" value="normal"></el-option><el-option label="加粗1" value="bold"></el-option><el-option label="加粗2" value="bolder"></el-option><el-option label="变细" value="lighter"></el-option></el-select></el-form-item>'+
                                                                '<el-form-item label="字体"><el-select v-model="echarts.title.subtextStyle.fontFamily"><el-option label="自己取名" value="sans-serif"></el-option><el-option label="自己取名" value="serif"></el-option><el-option label="自己取名" value="monospace"></el-option><el-option label="自己取名" value="Arial"></el-option><el-option label="自己取名" value="Courier New"></el-option></el-select></el-form-item>'+
                                                                '<el-form-item label="字体大小"><el-input-number v-model="echarts.title.subtextStyle.fontSize" step-strictly :step="1" :min="1" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="水平对齐"><el-select v-model="echarts.title.subtextStyle.align"><el-option label="靠左" value="left"></el-option><el-option label="居中" value="center"></el-option><el-option label="靠右" value="right"></el-option></el-select></el-form-item>'+
                                                                '<el-form-item label="垂直对齐"><el-select v-model="echarts.title.subtextStyle.verticalAlign"><el-option label="靠上" value="top"></el-option><el-option label="居中" value="middle"></el-option><el-option label="靠下" value="bottom"></el-option></el-select></el-form-item>'+
                                                                '<el-form-item label="行高"><el-input-number v-model="echarts.title.subtextStyle.lineHeight" step-strictly :step="1" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="宽度"><el-input-number v-model="echarts.title.subtextStyle.width" step-strictly :step="1" :min="1" :max="500" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="高度"><el-input-number v-model="echarts.title.subtextStyle.height" step-strictly :step="1" :min="1" :max="500" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="描边颜色"><el-color-picker v-model="echarts.title.subtextStyle.textBorderColor"></el-color-picker></el-form-item>'+
                                                                '<el-form-item label="描边宽度"><el-input-number v-model="echarts.title.subtextStyle.textBorderWidth" step-strictly :step="0.5" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="阴影颜色"><el-color-picker v-model="echarts.title.subtextStyle.textShadowColor"></el-color-picker></el-form-item>'+
                                                                '<el-form-item label="阴影长度"><el-input-number v-model="echarts.title.subtextStyle.textShadowBlur" step-strictly :step="0.5" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="阴影X偏移"><el-input-number v-model="echarts.title.subtextStyle.textShadowOffsetX" step-strictly :step="0.5" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="阴影Y偏移"><el-input-number v-model="echarts.title.subtextStyle.textShadowOffsetY" step-strictly :step="0.5" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="超出截断"><el-select v-model="echarts.title.subtextStyle.overflow"><el-option label="截断" value="truncate"></el-option><el-option label="换行1" value="break"></el-option><el-option label="换行2" value="breakAll"></el-option></el-select></el-form-item>'+
                                                                '<el-form-item label="末尾文本"><mc-change-input :mc-value.sync="echarts.title.subtextStyle.ellipsis" placeholder="请输入内容"></el-input></el-form-item>'+
                                                                '<el-form-item label="高度截断"><mc-change-input :mc-value.sync="echarts.title.subtextStyle.lineOverflow" placeholder="请输入内容"></el-input></el-form-item>'+
                                                            '</el-collapse-item>'+
                                                            '<el-collapse-item name="3" title="其他">'+
                                                                '<el-form-item label="是否显示"><el-switch v-model="echarts.title.show" ></el-switch></el-form-item>'+
                                                                '<el-form-item label="水平对齐"><el-select v-model="echarts.title.textAlign"><el-option label="自适应" value="auto"></el-option><el-option label="靠左" value="left"></el-option><el-option label="居中" value="center"></el-option><el-option label="靠右" value="right"></el-option></el-select></el-form-item>'+
                                                                '<el-form-item label="垂直对齐"><el-select v-model="echarts.title.textVerticalAlign"><el-option label="自适应" value="auto"></el-option><el-option label="靠上" value="top"></el-option><el-option label="居中" value="middle"></el-option><el-option label="靠下" value="bottom"></el-option></el-select></el-form-item>'+
                                                                '<el-form-item label="主副间距"><el-input-number v-model="echarts.title.itemGap" step-strictly :step="1" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="左边距"><mc-unit v-model="echarts.title.left"></mc-unit></el-form-item>'+
                                                                '<el-form-item label="上边距"><mc-unit v-model="echarts.title.top"></mc-unit></el-form-item>'+
                                                                '<el-form-item label="右边距"><mc-unit v-model="echarts.title.right"></mc-unit></el-form-item>'+
                                                                '<el-form-item label="下边距"><mc-unit v-model="echarts.title.bottom"></mc-unit></el-form-item>'+
                                                                '<el-form-item label="背景色"><el-color-picker v-model="echarts.title.backgroundColor"></el-color-picker></el-form-item>'+
                                                                '<el-form-item label="边框色"><el-color-picker v-model="echarts.title.borderColor"></el-color-picker></el-form-item>'+
                                                                '<el-form-item label="边框线宽"><el-input-number v-model="echarts.title.borderWidth" step-strictly :step="0.5" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="阴影模糊大小"><el-input-number v-model="echarts.title.shadowBlur" step-strictly :step="0.5" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="阴影颜色"><el-color-picker v-model="echarts.title.shadowColor"></el-color-picker></el-form-item>'+
                                                                '<el-form-item label="阴影水平偏移"><el-input-number v-model="echarts.title.shadowOffsetX" step-strictly :step="0.5" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                                '<el-form-item label="阴影垂直偏移"><el-input-number v-model="echarts.title.shadowOffsetY" step-strictly :step="0.5" :min="0" :max="1000" ></el-input-number></el-form-item>'+
                                                            '</el-collapse-item>'+
                                                    	'</el-collapse>'+
                                                    '</el-form>'+
                                                '</div>'+
                                                '</template>'+
                                            '</div>'+
'';
                                    },
                                    render:function (param) {
                                        setTimeout(function(){
                                            //从magicalCoder获取当前聚焦节点的属性值 准备传入vue里
                                            var config = param.config,focusNode=param.focusNode;
                                            var mcEchartsAttr = focusNode.attributes['mc-echarts-title'];
                                            var isFirstCome = true;
                                            if(!mcEchartsAttr){
                                                mcEchartsAttr = {
                                                     title:{
                                                          "show": true,
                                                          "textStyle": {},
                                                          "subtextStyle": {},
                                                        }
                                                     }
                                            }else{
                                                mcEchartsAttr = {title:JSON.parse(mcEchartsAttr.replace(/&quot;/g,"\"").replace(/&lt;/g,"<").replace(/&gt;/g,">"))};
                                            }
                                            new Vue({
                                                    el: '#mcEchartsTitleApp',
                                                    data: {
                                                        echarts: mcEchartsAttr,
                                                        titleActive:"1"
                                                    },
                                                    watch:{
                                                        echarts:{
                                                            handler: function(newVal, oldVal) {
                                                                if(isFirstCome){
                                                                    isFirstCome = false;
                                                                    return;
                                                                }
                                                                //监听到属性发生变化 那么修改属性 同时触发渲染
                                                                this.changeAttr();
                                                            },
                                                            deep: true
                                                        }
                                                    },
                                                    methods:{
                                                        changeAttr:function(){
                                                             console.log("change")
                                                             MAGICAL_CODER_API.changeAttr({node:focusNode,name:"mc-echarts-title",value:JSON.stringify(this.echarts.title),triggerChange:true})
                                                        }
                                                    }
                                                })
                                        },5)
                                    }
                                }
                            },
                    ]
                }
            ]
        }
    ]
});
