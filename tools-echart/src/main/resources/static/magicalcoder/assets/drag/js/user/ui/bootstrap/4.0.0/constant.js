/*常量池*/
function Constant(){
    var env = APPLICATION_ENV.getEnv();
    var envConstantSettings = env.constant.settings;
    this.type = env.constant.type;
    this.change = env.constant.change;
    /*全局设置*/
    this.settings = {
        navigateTree:envConstantSettings.navigateTree,
        styleTool:envConstantSettings.styleTool,
        javascript:envConstantSettings.javascript,
        html:envConstantSettings.html,
        css:envConstantSettings.css,
        debug:envConstantSettings.debug,
        cache:envConstantSettings.cache,
        workspace:envConstantSettings.workspace,
        file:envConstantSettings.file,
        other:envConstantSettings.other,
        //布局器右上角有个预览按钮 在弹窗打开 预览一下实际情况 因不同ui依赖资源不同 所以采用配置方式来加载资源
        view:{
            //预览地址
            url:envConstantSettings.view.url,
            //layui弹窗配置 area[0]宽如果不写则自动根据当前选中的设备模式宽 area[1]高
            layerExtra:envConstantSettings.view.layerExtra,
            //预览页<head></head>内的样式地址
            link:[],
            //预览页<head></head>内的脚本地址
            headJs:[],
            //预览页<body></body>内的脚本地址
            bodyJs:[
                "assets/drag/js/user/iframe/bootstrap/4.0.0/components/import.js"
            ]
        },
        //搜索节点的规则 每个节点在布局器都有一个唯一的magicalcoderid,但是有些情况部分ui会生成各种新的结构 导致我们想要操作的结构不对
        searchMagicalCoderIdRule:{
            //true:则从里往外搜索 false则从外往里搜索
            searchParent:function (elem){//一般您要配合iframe-ui.js的fixDynamicDomAfterRender方法来同时工作
                return false;
            }
        }
    }

    this.UI_TYPE = 9;
    this.UI_NAME = "bootstrap4";
    this.UI_FRAME = "jquery";
    /*响应式布局*/
    this.responsive = {
        SM:"sm",
        MD:"md",
        LG:"lg",
        XL:"xl",
    }
    this.responsiveList = [
        {id:this.responsive.SM,name:"手机",width:"320px",height:"100%",icon:"assets/drag/img/header/phone1.png"},
        {id:this.responsive.MD,name:"平板",width:"973px",height:"100%",icon:"assets/drag/img/header/paid1.png"},
        {id:this.responsive.LG,name:"笔记本",width:"1140px",height:"100%",icon:"assets/drag/img/header/notebook2.png"},
        {id:this.responsive.XL,name:"电脑",width:"100%",height:"100%",icon:"assets/drag/img/header/pc1.png",checked:true},
    ]
    /*左侧可拖拽的组件*/
    this.components = [];

    /*组件命名空间*/
    this.tagClassMapping = {}
    /*右侧面板属性-未来版本中将废弃，逐步往rightAttribute迁移*/
    this.rightPanel = {}
    /*新版右侧属性面板 可以控制tab标签个数*/
    this.rightAttribute = {
    }
    //默认脚本
    this.defaultJavascript ='//以下脚本为标签属性转换成layui组件的还原过程\n' +
        '//调试:打开浏览器控制台(F12),在代码中某行增加 debugger 即可调试\n' +
        '//执行自定义的组件渲染逻辑\n'+
        'new IframeComponents().execute();\n';
    this.refactor();
}
/*有些配置都是通用的 一个个写实在是麻烦，写个算法自动填充*/
Constant.prototype.refactor = function(){
    /*真正的安装组件*/
    MagicalCoder.installConstantComponents({tagClassMapping:this.tagClassMapping,dragItems:this.components,rightAttribute:this.rightAttribute});
    {//兼容下ie ie系列无法拖拽普通的input之类的控件，咱们在此加上tmpShade
        if(APPLICATION_ENV['addTmpShadeWhileIe']){
            APPLICATION_ENV.addTmpShadeWhileIe(this.tagClassMapping,['type=text'])
        }
    }
    {/*col追加到命名空间*/
        var tagCol = this.rowColBuild().tagColBuild();
        for(var key in tagCol){
            this.tagClassMapping[key] = tagCol[key];
        }
    }
    /*左侧可自定义拖拽组件区域的渲染逻辑*/
    {
        var mcTools = new McTools();
        var _t = this;
        this.componentMap={};
        this.leftComponentsRender = {
            /*包含html的容器 jquery对象 */
            container:$(".magicalcoder-left-config"),
            /*自己构造左侧的html 如果不想这里构造 可以采用下面的render来做
            * @param.components 上面自定义的组件
            * @param.container 容器
            * */
            html:function (param) {
                return null;
            },
            /*
            * html构造完后的事件渲染 您也可以自己构造渲染
            * @param.components 组件
            * @param.container 容器
            * */
            render:function (param) {
                _t.componentMap = {};//清空
                var titles = [];
                var contents = [];
                var idx = 0;
                var components = param.components;
                for(var i=0;i<components.length;i++){
                    var component = components[i];
                    var children = component.children;
                    if(children && children.length>0){//注意此处child已经通过_autoSetComponentId给了id
                        var htmlArr = []

                        var titleHtml = component.icon?('<i class="iconfont '+component.icon+'"></i><div>'+component.name+'</div>'):component.name;
                        titles.push({html:titleHtml});

                        var isLeaf = false;
                        for(var j=0;j<children.length;j++){
                            var child = children[j];
                            var icon = child.icon;
                            var innerChildren = child.children;
                            if(innerChildren){//示例里就做了3层 自己可以自行升级更多层 其他
                                isLeaf = false;
                                htmlArr.push('<div class="layui-colla-item">')
                                    htmlArr.push('<h6 class="layui-colla-title">'+child.name+'</h6>')
                                    htmlArr.push('<div class="layui-colla-content layui-show">')
                                        htmlArr.push('<ul class="magicalcoder-left-drag-item">');
                                        for(var k=0;k<innerChildren.length;k++){//
                                            var inner = innerChildren[k];
                                            var id = idx++;
                                            _t.componentMap[id]=inner;//放入组件map缓存 必填否则拖拽后不生效
                                            htmlArr.push(laytpl('<li component-id="{{d.id}}" class="magicalcoder-page-drag-item-label {{=d.child.clazz}}"><a>{{d.iconHtml}}<span>{{d.child.name}}</span></a></li>').render({id:id,child:inner,iconHtml:inner.icon.indexOf(".")!=-1?'<img class="iconfont" src="'+inner.icon+'">':'<i class="iconfont '+inner.icon+'"></i>'}))
                                        }
                                        htmlArr.push("</ul>")
                                    htmlArr.push("</div>")
                                htmlArr.push("</div>")
                            }else {
                                isLeaf = true;
                                var id = idx++;
                                _t.componentMap[id]=child;//放入组件map缓存  必填否则拖拽后不生效
                                //component-id 每个可拖拽结构必须有一个id 并且必须存在 componentMap的key=id value={} 当拖拽到中间时布局器需要根据component-id从componentMap取出value并且拿到html进行渲染
                                htmlArr.push(laytpl('<li component-id="{{d.id}}" class="magicalcoder-page-drag-item-label {{=d.child.clazz}}"><a>{{d.iconHtml}}<span>{{d.child.name}}</span></a></li>').render({id:id,child:child,iconHtml:icon.indexOf(".")!=-1?'<img class="iconfont" src="'+child.icon+'">':'<i class="iconfont '+icon+'"></i>'}))
                            }
                        }
                        if(isLeaf){
                            htmlArr.unshift('<ul class="magicalcoder-left-drag-item">');
                            htmlArr.push('</ul>');
                        }else {
                            //折叠
                            htmlArr.unshift('<div class="layui-collapse">')
                            htmlArr.push('</div>')
                        }
                        if(component.search){//添加了搜索功能
                            htmlArr.unshift('<div class="mc-left-search-components"><input class="layui-input" type="text" autocomplete="off" placeholder="搜索"/></div>')
                        }
                        contents.push({html:htmlArr.join("")})
                    }
                }

                 mcTools.leftRightTabsHtml({
                    elem:param.container,
                    titles:titles,
                    contents:contents
                });
                //初始化一下折叠功能 layui
                element.init();
                $(".mc-left-search-components>input").on('input',function () {
                    var name = $(this).val();
                    //遍历当前结构下 有没有匹配名称的
                    $(this).parent().parent().find(".magicalcoder-page-drag-item-label").each(function (idx, item) {
                        var componentId = $(this).attr("component-id");
                        var component = _t.componentMap[componentId];
                        if(component){
                            if(!name){
                                $(this).show();
                            }else {
                                if(component.name.indexOf(name)!=-1){//找到
                                    $(this).show();
                                }else {
                                    $(this).hide();
                                }
                            }

                        }
                    })
                })

                //下面是点击左侧结构 自动加入工作区的写法 如果您不需要 也可以忽略
                var $li = param.container.find(".magicalcoder-page-drag-item-label")
                $li.unbind('click').bind('click',function () {
                    MAGICAL_CODER_API.clickLeftDomAddToWorkspace({elem:$(this)});
                })
            },
            /*
            * 被拖拽元素的父亲jquery对象 布局器会执行each方法
            * 构造完左侧结构后，我们需要绑定拖拽事件中，这里就是ul，那么它下面的li将自动被绑定拖拽事件
            * */
            dragItemContainer:function (){ return $(".magicalcoder-left-config .magicalcoder-left-drag-item")}
        }
    }
    /*追加行列配置*/
    {
        var colConfig = this.rowColBuild().rightColBuild();
        for(var key in colConfig){
            this.rightAttribute[key] = [
                {   title:"属性",width:"100%",active:true,content:colConfig[key]}
            ]
        }
    }
    /*通用属性*/
//    this.commonAttrConfig();
}
/*如果您改动了上述配置 记得调用此方法 刷新一下*/
Constant.prototype.refresh = function(){}
/*通用配置*/
Constant.prototype.commonAttrConfig = function(){
    for(var key in this.tagClassMapping){
        var value = [];
        value.push({type:this.type.TEXT,clearAttr:true,oneLine:true,change:this.change.ATTR,attrName:'id',title:'id'});
        value.push({type:this.type.TEXT,clearAttr:true,oneLine:true,change:this.change.ATTR,attrName:'name',title:'name'});
        value.push({type:this.type.TEXTAREA,clearAttr:true,oneLine:true,change:this.change.ATTR,attrName:'style',title:'style'});
        value.push({type:this.type.TEXTAREA,clearAttr:true,oneLine:true,change:this.change.ATTR,attrName:'class',title:'class'});
        value.push({type:this.type.TEXT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'z-index',title:'层级',tooltip:'当重叠时可以用数值决定哪个控件置于上层',validate:{"^[0-9]*$":"请使用数字"}});
        value.push({type:this.type.FILEUPLOAD,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-image',title:'背景图',tooltip:'背景图片',callback:{coverValueToAttr:function(value,focusNode){return "url(\""+value+"\")"}}});
        value.push({type:this.type.TEXT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-size',title:'背景图尺寸',tooltip:"背景图尺寸:配置规则 宽 高<br>100px 200px<br>20% 30%",placeholder:"例如:10px 10px"});
        value.push({type:this.type.SELECT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-repeat',title:'背景图重复',tooltip:'背景图重复',options:[{"repeat":"默认"},{"repeat-x":"水平铺满"},{"repeat-y":"垂直铺满"},{"no-repeat":"仅显示一次"},{"inherit":"继承外层"}]});
        value.push({type:this.type.SELECT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-position',title:'背景图重复',tooltip:'背景图定位',options:[{"center center":"居中"},{"left top":"靠上靠左"},{"center top":"靠上居中"},{"right top":"靠上靠右"},{"left center":"靠左居中"},{"right center":"靠右居中"},{"left bottom":"靠下靠左"},{"center bottom":"靠下居中"},{"right bottom":"靠下靠右"}]});
        value.push({type:this.type.SELECT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-attachment',title:'滚动关联',tooltip:'滚动关联',options:[{"scroll":"跟随滚动"},{"fixed":"固定"}]});

        value.push( {type:this.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:this.change.CLASS    ,title:'文本颜色',options:[{'text-primary':'主要-蓝'},{'text-secondary':'次要-灰'},{'text-success':'成功-绿'},{'text-danger':'危险-红'},{'text-warning':'警告-橘黄'},{'text-info':'信息-淡绿'},{'text-light':'高亮-淡白'},{'text-dark':'暗黑'},{'text-white':'白色'}]});
        value.push( {type:this.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:this.change.CLASS    ,title:'背景色',options:[{'bg-primary':'主要-蓝'},{'bg-secondary':'次要-灰'},{'bg-success':'成功-绿'},{'bg-danger':'危险-红'},{'bg-warning':'警告-橘黄'},{'bg-info':'信息-淡绿'},{'bg-light':'高亮-淡白'},{'bg-dark':'暗黑'},{'bg-white':'白色'}]});
        value.push( {type:this.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:this.change.CLASS    ,title:'边框颜色',options:[{'border-primary':'主要-蓝'},{'border-secondary':'次要-灰'},{'border-success':'成功-绿'},{'border-danger':'危险-红'},{'border-warning':'警告-橘黄'},{'border-info':'信息-淡绿'},{'border-light':'高亮-淡白'},{'border-dark':'暗黑'},{'border-white':'白色'}]});
        value.push( {type:this.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:this.change.CLASS    ,title:'加边框',options:[{'border':'四边框'},{'border-top':'仅上边框'},{'border-bottom':'仅下边框'},{'border-left':'仅左边框'},{'border-right':'仅右边框'}]});
        value.push( {type:this.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:this.change.CLASS    ,title:'减边框',options:[{'border-0':'无边框'},{'border-top-0':'无上边框'},{'border-bottom-0':'无下边框'},{'border-left-0':'无左边框'},{'border-right-0':'无右边框'}]});
        value.push( {type:this.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:this.change.CLASS    ,title:'边框弧度',options:[{'rounded':'四角'},{'rounded-top':'上'},{'rounded-bottom':'下'},{'rounded-left':'左'},{'rounded-right':'右'},{'rounded-circle':'圆形'}]});
        value.push( {type:this.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:this.change.CLASS    ,title:'宽',options:[{'w-25':'25%'},{'w-50':'50%'},{'w-75':'75%'},{'w-100':'100%'}]});
        value.push( {type:this.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:this.change.CLASS    ,title:'高',options:[{'h-25':'25%'},{'h-50':'50%'},{'h-75':'75%'},{'h-100':'100%'}]});
        value.push( {type:this.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:this.change.CLASS    ,title:'与父元素文字垂直对齐方式',options:[{'align-baseline':'基准线'},{'align-top':'顶部'},{'align-middle':'居中'},{'align-bottom':'底部'},{'align-text-top':'字体顶部对齐'},{'align-bottom':'字体底部对齐'}]});
        this.rightPanel[1].content[key] = value;
    }
}

/*列是变动的 所以简单起见 全部遍历所有可能*/
Constant.prototype.rowColBuild = function(){
    var _t = this;
    return {
        rightCommonConfig:function(){
            var configArr = [
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'sm对齐方式',options:[{"d-sm-flex":"块方式"},{"d-sm-inline-flex":"内联方式"}] ,responsive:_t.responsive.SM},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'md对齐方式',options:[{"d-md-flex":"块方式"},{"d-md-inline-flex":"内联方式"}] ,responsive:_t.responsive.MD},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'lg对齐方式',options:[{"d-lg-flex":"块方式"},{"d-lg-inline-flex":"内联方式"}] ,responsive:_t.responsive.LG},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'xl对齐方式',options:[{"d-xl-flex":"块方式"},{"d-xl-inline-flex":"内联方式"}] ,responsive:_t.responsive.XL},

                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'sm内部结构垂直对齐',options:[{"align-items-sm-start":"起始"},{"align-items-sm-center":"居中"},{"align-items-sm-end":"结束"},{"align-items-sm-baseline":"基准线"},{"align-items-sm-stretch":"stretch"}] ,responsive:_t.responsive.SM},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'md内部结构垂直对齐',options:[{"align-items-md-start":"起始"},{"align-items-md-center":"居中"},{"align-items-md-end":"结束"},{"align-items-md-baseline":"基准线"},{"align-items-md-stretch":"stretch"}] ,responsive:_t.responsive.MD},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'lg内部结构垂直对齐',options:[{"align-items-lg-start":"起始"},{"align-items-lg-center":"居中"},{"align-items-lg-end":"结束"},{"align-items-lg-baseline":"基准线"},{"align-items-lg-stretch":"stretch"}] ,responsive:_t.responsive.LG},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'xl内部结构垂直对齐',options:[{"align-items-xl-start":"起始"},{"align-items-xl-center":"居中"},{"align-items-xl-end":"结束"},{"align-items-xl-baseline":"基准线"},{"align-items-xl-stretch":"stretch"}] ,responsive:_t.responsive.XL},

                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'sm内部结构水平对齐',options:[{"justify-content-sm-start":"起始"},{"justify-content-sm-center":"居中"},{"justify-content-sm-end":"结束"},{"justify-content-sm-around":"环绕"},{"justify-content-sm-between":"间隔"}] ,responsive:_t.responsive.SM},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'md内部结构水平对齐',options:[{"justify-content-md-start":"起始"},{"justify-content-md-center":"居中"},{"justify-content-md-end":"结束"},{"justify-content-md-around":"环绕"},{"justify-content-md-between":"间隔"}] ,responsive:_t.responsive.MD},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'lg内部结构水平对齐',options:[{"justify-content-lg-start":"起始"},{"justify-content-lg-center":"居中"},{"justify-content-lg-end":"结束"},{"justify-content-lg-around":"环绕"},{"justify-content-lg-between":"间隔"}] ,responsive:_t.responsive.LG},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'xl内部结构水平对齐',options:[{"justify-content-xl-start":"起始"},{"justify-content-xl-center":"居中"},{"justify-content-xl-end":"结束"},{"justify-content-xl-around":"环绕"},{"justify-content-xl-between":"间隔"}] ,responsive:_t.responsive.XL},

                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'sm自身对齐',options:[{"align-self-sm-auto":"自动"},{"align-self-sm-start":"起始"},{"align-self-sm-center":"居中"},{"align-self-sm-end":"结束"},{"align-self-sm-baseline":"基准线"},{"align-self-sm-stretch":"stretch"}] ,responsive:_t.responsive.SM},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'md自身对齐',options:[{"align-self-md-auto":"自动"},{"align-self-md-start":"起始"},{"align-self-md-center":"居中"},{"align-self-md-end":"结束"},{"align-self-md-baseline":"基准线"},{"align-self-md-stretch":"stretch"}] ,responsive:_t.responsive.MD},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'lg自身对齐',options:[{"align-self-lg-auto":"自动"},{"align-self-lg-start":"起始"},{"align-self-lg-center":"居中"},{"align-self-lg-end":"结束"},{"align-self-lg-baseline":"基准线"},{"align-self-lg-stretch":"stretch"}] ,responsive:_t.responsive.LG},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'xl自身对齐',options:[{"align-self-xl-auto":"自动"},{"align-self-xl-start":"起始"},{"align-self-xl-center":"居中"},{"align-self-xl-end":"结束"},{"align-self-xl-baseline":"基准线"},{"align-self-xl-stretch":"stretch"}] ,responsive:_t.responsive.XL},

                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'sm内部文字对齐方式',options:[{"text-sm-left":"左"},{"text-sm-center":"居中"},{"text-sm-right":"右"}] ,responsive:_t.responsive.SM},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'md内部文字对齐方式',options:[{"text-md-left":"左"},{"text-md-center":"居中"},{"text-md-right":"右"}] ,responsive:_t.responsive.MD},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'lg内部文字对齐方式',options:[{"text-lg-left":"左"},{"text-lg-center":"居中"},{"text-lg-right":"右"}] ,responsive:_t.responsive.LG},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'xl内部文字对齐方式',options:[{"text-xl-left":"左"},{"text-xl-center":"居中"},{"text-xl-right":"右"}] ,responsive:_t.responsive.XL},

                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'sm浮动',options:[{"float-sm-left":"左"},{"float-sm-right":"右"}] ,responsive:_t.responsive.SM},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'md浮动',options:[{"float-md-left":"左"},{"float-md-right":"右"}] ,responsive:_t.responsive.MD},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'lg浮动',options:[{"float-lg-left":"左"},{"float-lg-right":"右"}] ,responsive:_t.responsive.LG},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'xl浮动',options:[{"float-xl-left":"左"},{"float-xl-right":"右"}] ,responsive:_t.responsive.XL},

                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'sm内部结构垂直对齐',options:[{"-sm-":""},] ,responsive:_t.responsive.SM},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'md内部结构垂直对齐',options:[{"-md-":""},] ,responsive:_t.responsive.MD},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'lg内部结构垂直对齐',options:[{"-lg-":""},] ,responsive:_t.responsive.LG},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'xl内部结构垂直对齐',options:[{"-xl-":""},] ,responsive:_t.responsive.XL},
            ]
            return configArr;
        },
        rightColBuild:function(){
            var configArr = [
                {type:_t.type.TEXT      ,clearAttr:true       ,oneLine:true       ,change:_t.change.TEXT	,title:'文本'                },
                {type:_t.type.CHECKBOX  ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'自由定位',options:[{"c":"mc-ui-absolute-pane","n":"","t":"是否开启","u":""}]},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true       ,change:_t.change.CLASS   ,title:'背景色'   ,options:[{'bg-white':'白色'},{'bg-red':'赤色'},{'bg-orange':'橙色'},{'bg-green':'墨绿'},{'bg-cyan':'藏青'},{'bg-blue':'蓝色'},{'bg-black':'雅黑'},{'bg-gray':'银灰'}]},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'sm栅格数',options:[{"col-sm-auto":"自适应"},{"col-sm-1":"1/12"},{"col-sm-2":"2/12"},{"col-sm-3":"3/12"},{"col-sm-4":"4/12"},{"col-sm-5":"5/12"},{"col-sm-6":"6/12"},{"col-sm-7":"7/12"},{"col-sm-8":"8/12"},{"col-sm-9":"9/12"},{"col-sm-10":"10/12"},{"col-sm-11":"11/12"},{"col-sm-12":"12/12"}] ,responsive:_t.responsive.SM},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'md栅格数',options:[{"col-md-auto":"自适应"},{"col-md-1":"1/12"},{"col-md-2":"2/12"},{"col-md-3":"3/12"},{"col-md-4":"4/12"},{"col-md-5":"5/12"},{"col-md-6":"6/12"},{"col-md-7":"7/12"},{"col-md-8":"8/12"},{"col-md-9":"9/12"},{"col-md-10":"10/12"},{"col-md-11":"11/12"},{"col-md-12":"12/12"}] ,responsive:_t.responsive.MD},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'lg栅格数',options:[{"col-lg-auto":"自适应"},{"col-lg-1":"1/12"},{"col-lg-2":"2/12"},{"col-lg-3":"3/12"},{"col-lg-4":"4/12"},{"col-lg-5":"5/12"},{"col-lg-6":"6/12"},{"col-lg-7":"7/12"},{"col-lg-8":"8/12"},{"col-lg-9":"9/12"},{"col-lg-10":"10/12"},{"col-lg-11":"11/12"},{"col-lg-12":"12/12"}] ,responsive:_t.responsive.LG},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'xl栅格数',options:[{"col-xl-auto":"自适应"},{"col-xl-1":"1/12"},{"col-xl-2":"2/12"},{"col-xl-3":"3/12"},{"col-xl-4":"4/12"},{"col-xl-5":"5/12"},{"col-xl-6":"6/12"},{"col-xl-7":"7/12"},{"col-xl-8":"8/12"},{"col-xl-9":"9/12"},{"col-xl-10":"10/12"},{"col-xl-11":"11/12"},{"col-xl-12":"12/12"}] ,responsive:_t.responsive.XL},
            //偏移
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'sm偏移',options:[{"offset-sm-0":"无"},{"offset-sm-1":"8%"},{"offset-sm-2":"16%"},{"offset-sm-3":"1/4"},{"offset-sm-4":"1/3"},{"offset-sm-5":"41%"},{"offset-sm-6":"1/2"},{"offset-sm-7":"58%"},{"offset-sm-8":"66%"},{"offset-sm-9":"75%"},{"offset-sm-10":"83%"},{"offset-sm-11":"91%"}] ,responsive:_t.responsive.SM},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'md偏移',options:[{"offset-md-0":"无"},{"offset-md-1":"8%"},{"offset-md-2":"16%"},{"offset-md-3":"1/4"},{"offset-md-4":"1/3"},{"offset-md-5":"41%"},{"offset-md-6":"1/2"},{"offset-md-7":"58%"},{"offset-md-8":"66%"},{"offset-md-9":"75%"},{"offset-md-10":"83%"},{"offset-md-11":"91%"}] ,responsive:_t.responsive.MD},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'lg偏移',options:[{"offset-lg-0":"无"},{"offset-lg-1":"8%"},{"offset-lg-2":"16%"},{"offset-lg-3":"1/4"},{"offset-lg-4":"1/3"},{"offset-lg-5":"41%"},{"offset-lg-6":"1/2"},{"offset-lg-7":"58%"},{"offset-lg-8":"66%"},{"offset-lg-9":"75%"},{"offset-lg-10":"83%"},{"offset-lg-11":"91%"}] ,responsive:_t.responsive.LG},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'xl偏移',options:[{"offset-xl-0":"无"},{"offset-xl-1":"8%"},{"offset-xl-2":"16%"},{"offset-xl-3":"1/4"},{"offset-xl-4":"1/3"},{"offset-xl-5":"41%"},{"offset-xl-6":"1/2"},{"offset-xl-7":"58%"},{"offset-xl-8":"66%"},{"offset-xl-9":"75%"},{"offset-xl-10":"83%"},{"offset-xl-11":"91%"}] ,responsive:_t.responsive.XL},
            ]
            var commonConfigArr = this.rightCommonConfig();
            for(var i=0;i<commonConfigArr.length;i++){
                configArr.push(commonConfigArr[i]);
            }
            var config = [];
            for(var key in _t.responsive){
                var value = _t.responsive[key];
                for(var i=1;i<=12;i++){
                    config['col-'+value+'-'+i] = JSON.parse(JSON.stringify(configArr));
                }
            }
            return config;
        },
        tagColBuild:function(){
            var col = {name:"列",canZoom:true,assistZoom:true,dragInto:true,assistDuplicate:true,duplicate:true, copy:true,      paste:true, assistDelete:true,   canDelete:true,onlyParents:["row"],dblClickChange:{type:'text'}};
            var map = {};
            for(var key in _t.responsive){
                var value = _t.responsive[key];
                for(var i=1;i<=12;i++){
                    map['col-'+value+'-'+i] = JSON.parse(JSON.stringify(col));
                }
            }
            return map;
        },
        cols:function () {
            var arr = ['col'];
            for(var key in _t.responsive){
                var value = _t.responsive[key];
                for(var i=1;i<=12;i++){
                    arr.push('col-'+value+'-'+i);
                }
            }
            return arr;
        }
    }
}

Constant.prototype.getComponents = function(){
    return this.components;
}

Constant.prototype.getSettings = function(){
    return this.settings;
}

Constant.prototype.getComponentMap = function(){
    return this.componentMap;
}

Constant.prototype.getRightPanel = function(){
    return this.rightPanel;
}

Constant.prototype.getTagClassMapping = function(){
    return this.tagClassMapping;
}
Constant.prototype.getChange = function () {
    return this.change;
}
Constant.prototype.getType = function () {
    return this.type;
}
Constant.prototype.getResponsiveList = function () {
    return this.responsiveList;
}
Constant.prototype.getResponsive = function () {
    return this.responsive;
}
Constant.prototype.getUiType = function () {
    return this.UI_TYPE;
}
