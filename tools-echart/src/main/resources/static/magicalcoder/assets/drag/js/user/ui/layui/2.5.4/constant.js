/*常量池*/
function Constant(){
    var env = APPLICATION_ENV.getEnv();
    var envConstantSettings = env.constant.settings;
    this.type = env.constant.type;
    this.change = env.constant.change;
    var formItemOnlyParents = this.rowColBuild().cols();
    formItemOnlyParents.push('form')
    /*全局设置*/
    this.settings = {
        navigateTree:envConstantSettings.navigateTree,
        styleTool:envConstantSettings.styleTool,
        javascript:envConstantSettings.javascript,
        html:envConstantSettings.html,
        css:envConstantSettings.css,
        debug:envConstantSettings.debug,
        cache:envConstantSettings.cache,
        workspace:envConstantSettings.workspace,
        file:envConstantSettings.file,
        other:envConstantSettings.other,
        //布局器右上角有个预览按钮 在弹窗打开 预览一下实际情况 因不同ui依赖资源不同 所以采用配置方式来加载资源
        view:{
            //预览地址
            url:envConstantSettings.view.url,
            //layui弹窗配置 area[0]宽如果不写则自动根据当前选中的设备模式宽 area[1]高
            layerExtra:envConstantSettings.view.layerExtra,
            //预览页<head></head>内的样式地址
            link:[],
            //预览页<head></head>内的脚本地址
            headJs:[],
            //预览页<body></body>内的脚本地址
            bodyJs:[
                "assets/drag/js/user/iframe/layui/2.5.4/components/import.js"
            ]
        },
        //搜索节点的规则 每个节点在布局器都有一个唯一的magicalcoderid,但是有些情况部分ui会生成各种新的结构 导致我们想要操作的结构不对
        searchMagicalCoderIdRule:{
            //true:则从里往外搜索 false则从外往里搜索
            searchParent:function (elem){//一般您要配合iframe-ui.js的fixDynamicDomAfterRender方法来同时工作
                return false;
            }
        }
    }

    this.UI_TYPE = 0;
    this.UI_NAME = "layui";
    this.UI_FRAME = "jquery";
    /*响应式布局*/
    this.responsive = {
        XS:"xs",
        SM:"sm",
        MD:"md",
        LG:"lg",
    }
    this.responsiveList = [
        {id:this.responsive.XS,name:"手机",width:"320px",height:"100%",icon:"assets/drag/img/header/phone1.png"},
        {id:this.responsive.SM,name:"平板",width:"768px",height:"100%",icon:"assets/drag/img/header/paid1.png"},
        {id:this.responsive.MD,name:"笔记本",width:"992px",height:"100%",icon:"assets/drag/img/header/notebook2.png"},
        {id:this.responsive.LG,name:"电脑",width:"100%",height:"100%",icon:"assets/drag/img/header/pc1.png",checked:true},
    ]
    /*左侧可拖拽的组件*/
    this.components = [];
    /*布局器内组件命名空间*/
    this.tagClassMapping = {};
    /*右侧面板属性-未来版本中将废弃，逐步往rightAttribute迁移*/
    this.rightPanel = [];
    /*新版右侧属性面板 可以控制tab标签个数*/
    this.rightAttribute = {};
    //布局器初始化后加载的默认脚本
    this.defaultJavascript = '//以下脚本为标签属性转换成layui组件的还原过程\n' +
        '//调试:打开浏览器控制台(F12),在代码中某行增加 debugger 即可调试\n' +
        'var $ = layui.jquery, laytpl = layui.laytpl, laydate = layui.laydate, form = layui.form, layedit = layui.layedit, slider = layui.slider, element = layui.element, colorpicker = layui.colorpicker, upload = layui.upload, rate = layui.rate, carousel = layui.carousel, table = layui.table, flow = layui.flow;\n' +
        'var magicalDragLayuiUtil = {\n' +
        '    rebuildLayUiControls: function () {\n' +
        '        var _t = this;\n' +
        '        //日期\n' +
        '        $(".magicalcoder-laydate").each(function (idx, item) {\n' +
        '            laydate.render(_t.iteratorAttr({\n' +
        '                elem: item\n' +
        '            }, item));\n' +
        '        })\n' +
        '        //富文本\n' +
        '        $(".magicalcoder-layedit").each(function (idx, item) {\n' +
        '            var mcDataId = $(item).attr("id");\n' +
        '            if (mcDataId) {\n' +
        '                layedit.build(mcDataId, {\n' +
        '                    height: 300\n' +
        '                });\n' +
        '            }\n' +
        '        })\n' +
        '        //外键\n' +
        '        $(".magicalcoder-foreign-select2").each(function (idx, item) {\n' +
        '            var mcDataId = $(item).attr("id");\n' +
        '            if (mcDataId) {\n' +
        '                $("#" + mcDataId).select2({\n' +
        '                    allowClear: true,\n' +
        '                    width: "150px",\n' +
        '                    delay: 500,\n' +
        '                });\n' +
        '            }\n' +
        '        })\n' +
        '        //颜色选择器\n' +
        '        $(".magicalcoder-color-picker").each(function (idx, item) {\n' +
        '            colorpicker.render(_t.iteratorAttr({\n' +
        '                elem: $(item)}, item));\n' +
        '        })\n' +
        '        //上传组件\n' +
        '        $(".magicalcoder-layupload").each(function (idx, item) {\n' +
        '            upload.render(_t.iteratorAttr({\n' +
        '                elem: $(item)}, item));\n' +
        '        })\n' +
        '        //滑块\n' +
        '        $(".magicalcoder-slider").each(function (idx, item) {\n' +
        '            slider.render(_t.iteratorAttr({\n' +
        '                elem: $(item)}, item));\n' +
        '        })\n' +
        '        //评分\n' +
        '        $(".magicalcoder-rate").each(function (idx, item) {\n' +
        '            rate.render(_t.iteratorAttr({\n' +
        '                elem: $(item)}, item));\n' +
        '        })\n' +
        '        //轮播\n' +
        '        $(".layui-carousel").each(function (idx, item) {\n' +
        '            carousel.render(_t.iteratorAttr({\n' +
        '                elem: $(item)}, item));\n' +
        '        })\n' +
        '        //流加载\n' +
        '        $(".magicalcoder-flow").each(function (idx, item) {\n' +
        '            flow.load(_t.iteratorAttr({\n' +
        '                elem: $(item)}, item));\n' +
        '        })\n' +
        '        //代码\n' +
        '        $(".magicalcoder-code").each(function (idx, item) {\n' +
        '            layui.code(_t.iteratorAttr({\n' +
        '                elem: $(item)}, item));\n' +
        '        })\n' +
        '        //弹窗\n' +
        '        $(".magicalcoder-layer").each(function (idx, item) {\n' +
        '            //先隐藏起来\n' +
        '            $(this).next().hide();\n' +
        '            $(this).click(function () {\n' +
        '                var config = _t.iteratorAttr({\n' +
        '                    elem: $(item)}, item);\n' +
        '                var type = config.type;\n' +
        '                if (type + \'\' == 1) {\n' +
        '                    config.content = $(this).next();\n' +
        '                   if (!config.content.hasClass("magicalcoder-layer-content")) {\n' +
        '                        config.content = config.content.find(".magicalcoder-layer-content")\n' +
        '                    }' +
        '                }\n' +
        '                if (config.btn) {\n' +
        '                    config.btn = config.btn.split(",")\n' +
        '                }\n' +
        '                if (config.area) {\n' +
        '                    config.area = config.area.split(",")\n' +
        '                }\n' +
        '                layer.open(config)\n' +
        '            })\n' +
        '        })\n' +
        '        //动态表格 我们单独封装了layui-table的初始化方式 至于数据排序 返回格式 等操作请根据你的具体环境自行封装\n' +
        '        $(".magicalcoder-table").each(function (idx,\n' +
        '            item) {\n' +
        '            var cols = [];\n' +
        '            //读取列配置\n' +
        '            $(this).find(".magicalcoder-table-th").each(function (i, th) {\n' +
        '                cols.push(_t.iteratorAttr({\n' +
        '                    title: $(this).text()}, th));\n' +
        '            })\n' +
        '            var tableConfig = _t.iteratorAttr({\n' +
        '                elem: $(item),\n' +
        '                cols: [cols]},\n' +
        '                item);\n' +
        '            //初始化表格 至于返回的数据格式 您可以根据自己的系统自行改造 这里仅做一个示例 参考js\\\\data\\\\list.json\n' +
        '            table.render(tableConfig);\n' +
        '        })\n' +
        '        //部分组件初始化\n' +
        '        element.init();\n' +
        '        //表单初始化\n' +
        '        form.render();\n' +
        '    },\n' +
        '    //将标签上的属性 转换成layui函数初始化时的参数名:参数值\n' +
        '    iteratorAttr: function (renderConfig, dom) {\n' +
        '        var attrs = dom.attributes;\n' +
        '        for (var i = 0; i < attrs.length; i++) {\n' +
        '            var attr = attrs[i];\n' +
        '            var name = attr.name;\n' +
        '            var value = attr.value;\n' +
        '            if (name.indexOf("mc-") === 0) {\n' +
        '                name = name.replace("mc-attr-", \'\');\n' +
        '                name = name.replace("mc-event-", \'\');\n' +
        '                if (name.indexOf(\'str-\') === 0) {\n' +
        '                    name = name.replace(\'str-\', \'\');\n' +
        '                } else if (name.indexOf(\'bool-\') === 0) {\n' +
        '                    name = name.replace(\'bool-\', \'\');\n' +
        '                    value == \'true\' || value === \'\' ? value = true: value = value;\n' +
        '                    value == \'false\' ? value = false: value = value;\n' +
        '                } else if (name.indexOf(\'num-\') === 0) {\n' +
        '                    name = name.replace(\'num-\', \'\');\n' +
        '                    if (value !== \'\' && !isNaN(value)) {\n' +
        '                        value = parseFloat(value);\n' +
        '                    }\n' +
        '                } else if (name.indexOf(\'json-\') === 0) {\n' +
        '                    name = name.replace(\'json-\', \'\');\n' +
        '                    if (value !== \'\') {\n' +
        '                        value = JSON.parse(value);\n' +
        '                    }\n' +
        '                }\n' +
        '                renderConfig[this.htmlAttrNameToTuoFeng(name)] = value;\n' +
        '            }\n' +
        '        }\n' +
        '        return renderConfig;\n' +
        '    },\n' +
        '    //user-name -> userName html上的标签名转换成驼峰名称\n' +
        '    htmlAttrNameToTuoFeng: function (name) {\n' +
        '        var arr = name.split("-");\n' +
        '        var newArr = []\n' +
        '        for (var i = 0; i < arr.length; i++) {\n' +
        '            if (i != 0) {\n' +
        '                if (arr[i] != \'\') {\n' +
        '                    newArr.push(this.firstCharToUpLower(arr[i]));\n' +
        '                }\n' +
        '            } else {\n' +
        '                newArr.push(arr[i]);\n' +
        '            }\n' +
        '        }\n' +
        '        return newArr.join(\'\');\n' +
        '    },\n' +
        '    //首字母大写\n' +
        '    firstCharToUpLower: function (name) {\n' +
        '        var arr = name.split("");\n' +
        '        arr[0] = arr[0].toUpperCase();\n' +
        '        return arr.join(\'\');\n' +
        '    },\n' +
        '}\n' +
        'magicalDragLayuiUtil.rebuildLayUiControls();\n'+
        '//执行自定义的组件渲染逻辑\n'+
        'new IframeComponents().execute();\n'+
        '//functions-begin\n' +
        '//functions-end';
    this.refactor();
}
/*有些配置都是通用的 一个个写实在是麻烦，写个算法自动填充*/
Constant.prototype.refactor = function(){
    /*真正的安装组件*/
    MagicalCoder.installConstantComponents({tagClassMapping:this.tagClassMapping,dragItems:this.components,rightAttribute:this.rightAttribute});
    //把左侧图标追加下
    {
        var iconArr = ["layui-icon-rate-half","layui-icon-rate","layui-icon-rate-solid","layui-icon-cellphone","layui-icon-vercode","layui-icon-login-wechat","layui-icon-login-qq","layui-icon-login-weibo","layui-icon-password","layui-icon-username","layui-icon-refresh-3","layui-icon-auz","layui-icon-spread-left","layui-icon-shrink-right","layui-icon-snowflake","layui-icon-tips","layui-icon-note","layui-icon-home","layui-icon-senior","layui-icon-refresh","layui-icon-refresh-1","layui-icon-flag","layui-icon-theme","layui-icon-notice","layui-icon-website","layui-icon-console","layui-icon-face-surprised","layui-icon-set","layui-icon-template-1","layui-icon-app","layui-icon-template","layui-icon-praise","layui-icon-tread","layui-icon-male","layui-icon-female","layui-icon-camera","layui-icon-camera-fill","layui-icon-more","layui-icon-more-vertical","layui-icon-rmb","layui-icon-dollar","layui-icon-diamond","layui-icon-fire","layui-icon-return","layui-icon-location","layui-icon-read","layui-icon-survey","layui-icon-face-smile","layui-icon-face-cry","layui-icon-cart-simple","layui-icon-cart","layui-icon-next","layui-icon-prev","layui-icon-upload-drag","layui-icon-upload","layui-icon-download-circle","layui-icon-component","layui-icon-file-b","layui-icon-user","layui-icon-find-fill","layui-icon-loading layui-anim layui-anim-rotate layui-anim-loop","layui-icon-loading-1 layui-anim layui-anim-rotate layui-anim-loop","layui-icon-add-1","layui-icon-play","layui-icon-pause","layui-icon-headset","layui-icon-video","layui-icon-voice","layui-icon-speaker","layui-icon-fonts-del","layui-icon-fonts-code","layui-icon-fonts-html","layui-icon-fonts-strong","layui-icon-unlink","layui-icon-picture","layui-icon-link","layui-icon-face-smile-b","layui-icon-align-left","layui-icon-align-right","layui-icon-align-center","layui-icon-fonts-u","layui-icon-fonts-i","layui-icon-tabs","layui-icon-radio","layui-icon-circle","layui-icon-edit","layui-icon-share","layui-icon-delete","layui-icon-form","layui-icon-cellphone-fine","layui-icon-dialogue","layui-icon-fonts-clear","layui-icon-layer","layui-icon-date","layui-icon-water","layui-icon-code-circle","layui-icon-carousel","layui-icon-prev-circle","layui-icon-layouts","layui-icon-util","layui-icon-templeate-1","layui-icon-upload-circle","layui-icon-tree","layui-icon-table","layui-icon-chart","layui-icon-chart-screen","layui-icon-engine","layui-icon-triangle-d","layui-icon-triangle-r","layui-icon-file","layui-icon-set-sm","layui-icon-add-circle","layui-icon-404","layui-icon-about","layui-icon-up","layui-icon-down","layui-icon-left","layui-icon-right","layui-icon-circle-dot","layui-icon-search","layui-icon-set-fill","layui-icon-group","layui-icon-friends","layui-icon-reply-fill","layui-icon-menu-fill","layui-icon-log","layui-icon-picture-fine","layui-icon-face-smile-fine","layui-icon-list","layui-icon-release","layui-icon-ok","layui-icon-help","layui-icon-chat","layui-icon-top","layui-icon-star","layui-icon-star-fill","layui-icon-close-fill","layui-icon-close","layui-icon-ok-circle","layui-icon-add-circle-fine","layui-icon-table","layui-icon-upload","layui-icon-slider"]
        for(var i=0;i<this.components.length;i++){
            var config = this.components[i];
            if(config.key=='icon'){
                for(var n=0;n<iconArr.length;n++){
                    var icon = iconArr[n];
                    config.children.push({"clazz":"only-icon",name:"",icon:"layui-icon "+icon,html:"<i class='layui-icon "+icon+"'></i>"});
                }
            }
        }
    }
    {//兼容下ie ie系列无法拖拽普通的input之类的控件，咱们在此加上tmpShade
        if(APPLICATION_ENV['addTmpShadeWhileIe']){
            APPLICATION_ENV.addTmpShadeWhileIe(this.tagClassMapping,['layui-btn','type=text','textarea'])
        }
    }
    {/*layui-col追加到命名空间*/
        var tagCol = this.rowColBuild().tagColBuild();
        for(var key in tagCol){
            this.tagClassMapping[key] = tagCol[key];
        }
    }
    /*左侧可自定义拖拽组件区域的渲染逻辑*/
    {
        var mcTools = new McTools();
        var _t = this;
        this.componentMap={};
        this.leftComponentsRender = {
            /*包含html的容器 jquery对象 */
            container:$(".magicalcoder-left-config"),
            /*自己构造左侧的html 如果不想这里构造 可以采用下面的render来做
            * @param.components 上面自定义的组件
            * @param.container 容器
            * */
            html:function (param) {
                return null;
            },
            /*
            * html构造完后的事件渲染 您也可以自己构造渲染
            * @param.components 组件
            * @param.container 容器
            * */
            render:function (param) {
                _t.componentMap = {};//清空
                var titles = [];
                var contents = [];
                var idx = 0;
                var components = param.components;
                for(var i=0;i<components.length;i++){
                    var component = components[i];
                    var children = component.children;
                    if(children && children.length>0){//注意此处child已经通过_autoSetComponentId给了id
                        var htmlArr = []

                        var titleHtml = component.icon?('<i class="iconfont '+component.icon+'"></i><div>'+component.name+'</div>'):component.name;
                        titles.push({html:titleHtml});

                        var isLeaf = false;
                        for(var j=0;j<children.length;j++){
                            var child = children[j];
                            var icon = child.icon;
                            var innerChildren = child.children;
                            if(innerChildren){//示例里就做了3层 自己可以自行升级更多层 其他
                                isLeaf = false;
                                htmlArr.push('<div class="layui-colla-item">')
                                    htmlArr.push('<h6 class="layui-colla-title">'+child.name+'</h6>')
                                    htmlArr.push('<div class="layui-colla-content layui-show">')
                                        htmlArr.push('<ul class="magicalcoder-left-drag-item">');
                                        for(var k=0;k<innerChildren.length;k++){//
                                            var inner = innerChildren[k];
                                            var id = idx++;
                                            _t.componentMap[id]=inner;//放入组件map缓存 必填否则拖拽后不生效
                                            htmlArr.push(laytpl('<li component-id="{{d.id}}" class="magicalcoder-page-drag-item-label {{=d.child.clazz}}"><a>{{d.iconHtml}}<span>{{d.child.name}}</span></a></li>').render({id:id,child:inner,iconHtml:inner.icon.indexOf(".")!=-1?'<img class="iconfont" src="'+inner.icon+'">':'<i class="iconfont '+inner.icon+'"></i>'}))
                                        }
                                        htmlArr.push("</ul>")
                                    htmlArr.push("</div>")
                                htmlArr.push("</div>")
                            }else {
                                isLeaf = true;
                                var id = idx++;
                                _t.componentMap[id]=child;//放入组件map缓存  必填否则拖拽后不生效
                                //component-id 每个可拖拽结构必须有一个id 并且必须存在 componentMap的key=id value={} 当拖拽到中间时布局器需要根据component-id从componentMap取出value并且拿到html进行渲染
                                htmlArr.push(laytpl('<li component-id="{{d.id}}" class="magicalcoder-page-drag-item-label {{=d.child.clazz}}"><a>{{d.iconHtml}}<span>{{d.child.name}}</span></a></li>').render({id:id,child:child,iconHtml:icon.indexOf(".")!=-1?'<img class="iconfont" src="'+child.icon+'">':'<i class="iconfont '+icon+'"></i>'}))
                            }
                        }
                        if(isLeaf){
                            htmlArr.unshift('<ul class="magicalcoder-left-drag-item">');
                            htmlArr.push('</ul>');
                        }else {
                            //折叠
                            htmlArr.unshift('<div class="layui-collapse">')
                            htmlArr.push('</div>')
                        }
                        if(component.search){//添加了搜索功能
                            htmlArr.unshift('<div class="mc-left-search-components"><input class="layui-input" type="text" autocomplete="off" placeholder="搜索"/></div>')
                        }
                        contents.push({html:htmlArr.join("")})
                    }
                }

                 mcTools.leftRightTabsHtml({
                    elem:param.container,
                    titles:titles,
                    contents:contents
                });
                //初始化一下折叠功能 layui
                element.init();
                $(".mc-left-search-components>input").on('input',function () {
                    var name = $(this).val();
                    //遍历当前结构下 有没有匹配名称的
                    $(this).parent().parent().find(".magicalcoder-page-drag-item-label").each(function (idx, item) {
                        var componentId = $(this).attr("component-id");
                        var component = _t.componentMap[componentId];
                        if(component){
                            if(!name){
                                $(this).show();
                            }else {
                                if(component.name.indexOf(name)!=-1){//找到
                                    $(this).show();
                                }else {
                                    $(this).hide();
                                }
                            }

                        }
                    })
                })

                //下面是点击左侧结构 自动加入工作区的写法 如果您不需要 也可以忽略
                var $li = param.container.find(".magicalcoder-page-drag-item-label")
                $li.unbind('click').bind('click',function () {
                    MAGICAL_CODER_API.clickLeftDomAddToWorkspace({elem:$(this)});
                })
            },
            /*
            * 被拖拽元素的父亲jquery对象 布局器会执行each方法
            * 构造完左侧结构后，我们需要绑定拖拽事件中，这里就是ul，那么它下面的li将自动被绑定拖拽事件
            * */
            dragItemContainer:function (){ return $(".magicalcoder-left-config .magicalcoder-left-drag-item")}
        }
    }
    /*追加行列配置*/
    {
        var colConfig = this.rowColBuild().rightColBuild();
        for(var key in colConfig){
            this.rightAttribute[key] = [
                {   title:"属性",width:"100%",active:true,content:colConfig[key]}
            ]
        }
    }
    /*通用属性*/
//    this.commonAttrConfig();
}
/*如果您改动了上述配置 记得调用此方法 刷新一下*/
Constant.prototype.refresh = function(){}
/*可以通用的配置*/
Constant.prototype.commonAttrConfig = function(){
    for(var key in this.tagClassMapping){
        var value = [];
        value.push({type:this.type.TEXT,clearAttr:true,oneLine:true,change:this.change.ATTR,attrName:'id',title:'id'});
        value.push({type:this.type.TEXT,clearAttr:true,oneLine:true,change:this.change.ATTR,attrName:'name',title:'name'});
        value.push({type:this.type.TEXTAREA,clearAttr:true,oneLine:true,change:this.change.ATTR,attrName:'style',title:'style'});
        value.push({type:this.type.TEXTAREA,clearAttr:true,oneLine:true,change:this.change.ATTR,attrName:'class',title:'class'});
        value.push({type:this.type.TEXT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'z-index',title:'层级',tooltip:'当重叠时可以用数值决定哪个控件置于上层',validate:{"^[0-9]*$":"请使用数字"}});
        value.push({type:this.type.FILEUPLOAD,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-image',title:'背景图',tooltip:'背景图片',callback:{coverValueToAttr:function(value,focusNode){return "url(\""+value+"\")"}}});
        value.push({type:this.type.TEXT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-size',title:'背景图尺寸',tooltip:"背景图尺寸:配置规则 宽 高<br>100px 200px<br>20% 30%",placeholder:"例如:10px 10px"});
        value.push({type:this.type.SELECT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-repeat',title:'背景图重复',tooltip:'背景图重复',options:[{"repeat":"默认"},{"repeat-x":"水平铺满"},{"repeat-y":"垂直铺满"},{"no-repeat":"仅显示一次"},{"inherit":"继承外层"}]});
        value.push({type:this.type.SELECT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-position',title:'背景图重复',tooltip:'背景图定位',options:[{"center center":"居中"},{"left top":"靠上靠左"},{"center top":"靠上居中"},{"right top":"靠上靠右"},{"left center":"靠左居中"},{"right center":"靠右居中"},{"left bottom":"靠下靠左"},{"center bottom":"靠下居中"},{"right bottom":"靠下靠右"}]});
        value.push({type:this.type.SELECT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-attachment',title:'滚动关联',tooltip:'滚动关联',options:[{"scroll":"跟随滚动"},{"fixed":"固定"}]});
        value.push({type:this.type.CHECKBOX,clearAttr:true,oneLine:true,change:this.change.CLASS,title:'开启动画',options:[{"c":"layui-anim","n":"","t":"是否开启","u":""}]});
        value.push({type:this.type.CHECKBOX,clearAttr:true,oneLine:true,change:this.change.CLASS,title:'循环播放',options:[{"c":"layui-anim-loop","n":"","t":"是否开启","u":""}]});
        value.push({type:this.type.SELECT,clearAttr:true,oneLine:true,change:this.change.CLASS,title:'动画效果',options:[{'layui-anim-up':'底部向上'},{'layui-anim-upbit':'微微向上滑入'},{'layui-anim-scale':'平滑放大'},{'layui-anim-scaleSpring':'弹簧式放大'},{'layui-anim-fadein':'渐显'},{'layui-anim-fadeout':'渐隐'},{'layui-anim-rotate':'360度旋转'}]});
        this.rightPanel[1].content[key] = value;
    }
}
/*列是变动的 所以简单起见 全部遍历所有可能*/
Constant.prototype.rowColBuild = function(){
    var _t = this;
    return {
        rightColBuild:function(){
            var configArr = [
                {type:_t.type.TEXT      ,clearAttr:true       ,oneLine:true      ,change:_t.change.TEXT	    ,title:'文本'                },
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'背景色'  ,options:[{'layui-bg-white':'白色'},{'layui-bg-red':'赤色'},{'layui-bg-orange':'橙色'},{'layui-bg-green':'墨绿'},{'layui-bg-cyan':'藏青'},{'layui-bg-blue':'蓝色'},{'layui-bg-black':'雅黑'},{'layui-bg-gray':'银灰'}]},
                {type:_t.type.CHECKBOX  ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'自由定位',options:[{"c":"mc-ui-absolute-pane","n":"","t":"是否开启","u":""}]},
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'xs栅格数',options:[{"layui-col-xs1":"1/12"},{"layui-col-xs2":"2/12"},{"layui-col-xs3":"3/12"},{"layui-col-xs4":"4/12"},{"layui-col-xs5":"5/12"},{"layui-col-xs6":"6/12"},{"layui-col-xs7":"7/12"},{"layui-col-xs8":"8/12"},{"layui-col-xs9":"9/12"},{"layui-col-xs10":"10/12"},{"layui-col-xs11":"11/12"},{"layui-col-xs12":"12/12"}]                       ,responsive:_t.responsive.XS                    },
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'sm栅格数',options:[{"layui-col-sm1":"1/12"},{"layui-col-sm2":"2/12"},{"layui-col-sm3":"3/12"},{"layui-col-sm4":"4/12"},{"layui-col-sm5":"5/12"},{"layui-col-sm6":"6/12"},{"layui-col-sm7":"7/12"},{"layui-col-sm8":"8/12"},{"layui-col-sm9":"9/12"},{"layui-col-sm10":"10/12"},{"layui-col-sm11":"11/12"},{"layui-col-sm12":"12/12"}]                       ,responsive:_t.responsive.SM                    },
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'md栅格数',options:[{"layui-col-md1":"1/12"},{"layui-col-md2":"2/12"},{"layui-col-md3":"3/12"},{"layui-col-md4":"4/12"},{"layui-col-md5":"5/12"},{"layui-col-md6":"6/12"},{"layui-col-md7":"7/12"},{"layui-col-md8":"8/12"},{"layui-col-md9":"9/12"},{"layui-col-md10":"10/12"},{"layui-col-md11":"11/12"},{"layui-col-md12":"12/12"}]                       ,responsive:_t.responsive.MD                    },
                {type:_t.type.SELECT    ,clearAttr:true       ,oneLine:true      ,change:_t.change.CLASS    ,title:'lg栅格数',options:[{"layui-col-lg1":"1/12"},{"layui-col-lg2":"2/12"},{"layui-col-lg3":"3/12"},{"layui-col-lg4":"4/12"},{"layui-col-lg5":"5/12"},{"layui-col-lg6":"6/12"},{"layui-col-lg7":"7/12"},{"layui-col-lg8":"8/12"},{"layui-col-lg9":"9/12"},{"layui-col-lg10":"10/12"},{"layui-col-lg11":"11/12"},{"layui-col-lg12":"12/12"}] ,responsive:_t.responsive.LG                    },
            ]
            var config = [];
            for(var key in _t.responsive){
                var value = _t.responsive[key];
                for(var i=1;i<=12;i++){
                    config['layui-col-'+value+i] = JSON.parse(JSON.stringify(configArr));
                }
            }
            return config;
        },
        tagColBuild:function(){
            var col = {name:"列",canZoom:true,assistZoom:true,dragInto:true,assistDuplicate:true,duplicate:true, copy:true,      paste:true, assistDelete:true,   canDelete:true,onlyParents:["layui-row"],dblClickChange:{type:"text"}};
            var map = {};
            for(var key in _t.responsive){
                var value = _t.responsive[key];
                for(var i=1;i<=12;i++){
                    map['layui-col-'+value+i] = JSON.parse(JSON.stringify(col));
                }
            }
            return map;
        },
        cols:function () {
            var arr = [];
            for(var key in _t.responsive){
                var value = _t.responsive[key];
                for(var i=1;i<=12;i++){
                    arr.push('layui-col-'+value+i);
                }
            }
            return arr;
        }
    }
}

Constant.prototype.getComponents = function(){
    return this.components;
}

Constant.prototype.getSettings = function(){
    return this.settings;
}

Constant.prototype.getComponentMap = function(){
    return this.componentMap;
}

Constant.prototype.getRightPanel = function(){
    return this.rightPanel;
}

Constant.prototype.getTagClassMapping = function(){
    return this.tagClassMapping;
}
Constant.prototype.getChange = function () {
    return this.change;
}
Constant.prototype.getType = function () {
    return this.type;
}
Constant.prototype.getResponsiveList = function () {
    return this.responsiveList;
}
Constant.prototype.getResponsive = function () {
    return this.responsive;
}
Constant.prototype.getUiType = function () {
    return this.UI_TYPE;
}
