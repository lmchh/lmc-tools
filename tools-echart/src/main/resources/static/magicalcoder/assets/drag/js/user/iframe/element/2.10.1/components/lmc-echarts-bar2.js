function McEChartsBar(){
    //___mcUtils参考mc-utils.js
    this.mcUtils = ___mcUtils;
    this.param = null;
    this.domClass = "lmc-echarts-bar2";
}
/*务必带此render方法：否则组件不会被追踪渲染*/
McEChartsBar.prototype.render = function(param){
    if(!this.mcUtils.ifEchartsContinueRenderInMagicalCoder(param,this.domClass)){
        return;
    }
    this.param = param;
    this.bar();
}
McEChartsBar.prototype.bar = function () {
    var _t = this;
    var charsDoms = this.mcUtils.findNeedDealerElements(this.param,this.domClass);
    for(var i=0;i<charsDoms.length;i++){
        var echartsDom = charsDoms[i];
        var myEcharts = echarts.init(echartsDom);
        var _dataManager = this.mcUtils.attr(echartsDom,"mc-echarts-data-manager");
        var option = {
            tooltip: {
                trigger: 'axis',
                "axisPointer": {
                    "type": "shadow"
                }
            },
            legend: {
                data: []
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
             xAxis: {
                 type: 'category',
                 data: [],
                 "axisLabel": {
                     "show":true,
                     "rotate": 45,
                     "interval":0
                 }
             },
             yAxis: {
                 type: 'value'
             },
             series: [{
                 name: '',
                 data: [],
                 type: 'bar',
                 stack: "stack",

             }]
        };
        var dataManager = null;
        if(_dataManager){
            dataManager = JSON.parse(_dataManager);
        }
        var render = false;
        if(dataManager){
            var dataType = dataManager.dataType;
            if(dataType==1){
                render = dataManager.choose1.sourceApi && dataManager.choose1.x && dataManager.choose1.y;
                xFieldName=dataManager.choose1.x;
                yFieldName=dataManager.choose1.y;
            }else if(dataType==2){
                render = dataManager.choose2.sourceApi && dataManager.choose2.x && dataManager.choose2.y;
                xFieldName=dataManager.choose2.x;
                yFieldName=dataManager.choose2.y;
            }else if(dataType==3){
                render = true;
            }
        }
        if(render){
            var dataType = dataManager.dataType;
            if(dataType==1){
                //这里是循环调用ajax所以栈顶不变，参数务必传ajax首位参数  window.iframeUi可以判断当前是否在布局器环境
                this.mcUtils.ajax(window.iframeUi,{myEcharts:myEcharts,dataManager:dataManager,option:option},dataManager.choose1.sourceApi,function(param,data){
                    _t.setOption(param.option,{myEcharts:param.myEcharts,xFieldName:param.dataManager.choose1.x,yFieldName:param.dataManager.choose1.y},data)
                })
            }else if(dataType==2){
                this.mcUtils.ajax(window.iframeUi,{myEcharts:myEcharts,dataManager:dataManager,option:option},dataManager.choose2.sourceApi,function(param,data){
                    _t.setOption(param.option,{myEcharts:param.myEcharts,xFieldName:param.dataManager.choose2.x,yFieldName:param.dataManager.choose2.y},data)
                })
            }else{
                var data = dataManager.choose3.data;
                _t.setOption(option,{myEcharts:myEcharts,xFieldName:"x",yFieldName:"y"},{code:0,data:data})
            }
        }
    }
}
McEChartsBar.prototype.setOption = function(option,param,data){
    if(data.code!=0){
        return;
    }
    var dataList = data.data;
    //拿到数据 我们转换一下 变成echarts的数据
    var xAxisData = [];//x数据
    if (dataList != null && dataList.length > 0) {
        var legends = new Array();
        for (var key in dataList[0]) {
            legends.push(key)
        }
        var series = new Array();
        for (i = 1; i < legends.length; ++i) {
            if (i > 1) {
                option.series[i-1] = {}
                option.series[i-1].type = 'bar'
                option.series[i-1].data = []
                option.series[i-1].stack = 'stack'
            }else {
                series[0] = option.series[0];
                option.series = series;
            }
            option.series[i-1].name = legends[i]
        }
        for(var j=0;j<dataList.length;j++){
            var item = dataList[j];
            xAxisData.push(item[legends[0]]);
            for(l = 1; l < legends.length; ++l) {
                option.series[l-1].data.push(item[legends[l]])

            }
        }
        option.legend.data = legends;
        option.xAxis.data = xAxisData;
    }
    param.myEcharts.setOption(option);
    param.myEcharts.resize();
}
/*把组件安装一下*/
___mcUtils.install({component:new McEChartsBar()})
