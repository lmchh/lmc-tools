/*各种png组成的形状组件放置在此处
* */
var ___geometryAttr = function(){
    var data =
    [
        {
            title:"属性",
            active:true,
            width:"100%",
            content:[
                { type: "colorpicker", clearAttr: true, oneLine: true, change: "attr", attrName: 'fill', title: '填充颜色' },
                { type: "slider", clearAttr: true, oneLine: false, change: "attr", attrName: 'stroke-width', title: '边框粗细', extra: { min: 0, max: 20 } },
                { type: "colorpicker", clearAttr: true, oneLine: true, change: "attr", attrName: 'stroke', title: '边框颜色' },
            ]
        }
    ];
    return data;
}
MagicalCoder.install({
    /*左侧可拖拽的源*/
    dragItems:[
        {
            name:"形状",
            icon:"ri-stop-line",
            children:[
                {
                    name:"矩形",
                    icon:"ri-stop-line",
                    html:"<div class='mc-ui-geometry-rectangle mc-ui-flex-center' style='width:100px;height:100px;'></div>"
                },{
                    name:"横线",
                    icon:"ri-subtract-line",
                    html:"<div class='mc-ui-geometry-x-line' style='width:100px;'></div>"
                },{
                    name:"竖线",
                    icon:"ri-subtract-line mc-transform-90",
                    html:"<div class='mc-ui-geometry-y-line' style='height:100px;'></div>"
                },{
                    name:"圆形",
                    icon:"ri-checkbox-blank-circle-line",
                    html:"<div class='mc-ui-geometry-round mc-ui-flex-center' style='height:100px;width:100px;'></div>"
                },{
                    name:"文字",
                    icon:"ri-character-recognition-line",
                    html:"<span class='mc-ui-geometry-char' style='height:20px;width:100px;'>文字</span>"
                },
                {
                    name:"圆形",
                    icon:"",
                    html:'<div class="mc-ui-geometry-circular"></div>'
                },
                {
                    name:"三角形",
                    icon:"",
                    html:'<div class="mc-ui-geometry-triangle"></div>'
                },
                {
                    name:"六边形",
                    icon:"",
                    html:'<div class="mc-ui-geometry-hexagon"></div>'
                },
                {
                    name:"圆角矩形",
                    icon:"",
                    html:'<div class="mc-ui-geometry-rectwyl"></div>'
                },

                {
                    name:"标签",
                    icon:"",
                    html:'<div class="mc-ui-geometry-pathwyl"></div>'
                },
                {
                    name:"坐标",
                    icon:"",
                    html:'<div class="mc-ui-geometry-path2wyl"></div>'
                },
                {
                    name:"正方形",
                    icon:"",
                    html:'<div class="mc-ui-geometry-square"></div>'
                },
                {
                    name:"梯形",
                    icon:"",
                    html:'<div class="mc-ui-geometry-trapezoid"></div>'
                },
                {
                    name:"平行四边形",
                    icon:"",
                    html:'<div class="mc-ui-geometry-parallelogram"></div>'
                },
                {
                    name: "X形",
                    icon: "",
                    html: '<div class="mc-ui-geometry-xshape"></div>'
                }, {
                    name: "对话框",
                    icon: "",
                    html: '<div class="mc-ui-geometry-dialog"></div>'
                }, {
                    name: "心形",
                    icon: "",
                    html: '<div class="mc-ui-geometry-heart"></div>'
                },{
                     name:"八边形",
                     icon:"",
                     html:'<div class="mc-ui-geometry-octagon"></div>'
                 },{
                     name:"十二边形",
                     icon:"",
                     html:'<div class="mc-ui-geometry-dodecagonal"></div>'
                 },{
                     name:"左箭头",
                     icon:"",
                     html:'<div class="mc-ui-geometry-leftarrow"></div>'
                 },
                  {
                      name:"五角星形",
                      icon:"",
                      html:'<div class="mc-ui-geometry-pentacle"></div>'
                  },
                  {
                      name:"十字架形",
                      icon:"",
                      html:'<div class="mc-ui-geometry-cross"></div>'
                  }
            ]
        }
   ],
   /*自定义组件和属性*/
    components:[
        {
            identifier:"mc-ui-geometry-rectangle",
            properties:{name:"矩形",dragInto:false,assistDelete:true,assistAdd:true,assistDuplicate:true,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true,dblClickChange:{type:"text"}},
            attributes: [],

        },
        {
            identifier:"mc-ui-geometry-round",
            properties:{name:"圆形",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true,dblClickChange:{type:"text"}},
            attributes:[],
        },
        {
            identifier:"mc-ui-geometry-x-line",
            properties:{name:"横线",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true,dblClickChange:{type:"text"}},
            attributes:[],
        },
        {
            identifier:"mc-ui-geometry-y-line",
            properties:{name:"竖线",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true,dblClickChange:{type:"text"}},
            attributes:[],
        },
        {
            identifier:"mc-ui-geometry-char",
            properties:{name:"文字",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true,dblClickChange:{type:"text"}},
            attributes:[],
        },
        {
            identifier:"mc-ui-geometry-circular",
            properties: {name:"圆形",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-triangle",
            properties: {name:"三角形",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-hexagon",
            properties: {name:"六边形",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-rectwyl",
            properties: {name:"圆角矩形",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-pathwyl",
            properties: {name:"标签",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-path2wyl",
            properties: {name:"坐标",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-square",
            properties: {name:"正方形",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-trapezoid",
            properties: {name:"梯形",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-parallelogram",
            properties: {name:"平行四边形",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-xshape",
            properties: { name: "X形", dragInto: false, assistDelete: false, assistAdd: false, assistDuplicate: false, duplicate: true, duplicateAttr: [], copy: true, paste: false, canDelete: true },
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-dialog",
            properties: { name: "对话框", dragInto: false, assistDelete: false, assistAdd: false, assistDuplicate: false, duplicate: true, duplicateAttr: [], copy: true, paste: false, canDelete: true },
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-heart",
            properties: { name: "心形", dragInto: false, assistDelete: false, assistAdd: false, assistDuplicate: false, duplicate: true, duplicateAttr: [], copy: true, paste: false, canDelete: true },
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-octagon",
            properties: {name:"八边形",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-dodecagonal",
            properties: {name:"十二边形",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-leftarrow",
            properties: {name:"左箭头",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-pentacle",
            properties : {name:"五角星形",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        },
        {
            identifier:"mc-ui-geometry-cross",
            properties: {name:"十字架形",dragInto:false,assistDelete:false,assistAdd:false,assistDuplicate:false,  duplicate:true,duplicateAttr:[],   copy:true,      paste:false,  canDelete:true},
            attributes:___geometryAttr(),
        }
    ]
});
