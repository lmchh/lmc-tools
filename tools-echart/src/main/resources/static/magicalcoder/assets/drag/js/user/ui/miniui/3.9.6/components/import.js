/*
    引入被安装的组件:为了把constant.js涉及的组件解耦，特拆出components目录
    如果您有新增组件 可以引入到此处
    如果您追求性能，也可以把components的文件合并拷贝替换此文件 这样就只加载一个组件了
*/
MagicalCoder.import([
    "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-line.js",
    "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-bar.js",
    "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-pie.js",
    "assets/drag/js/user/ui/magicalcoder/components/mc-echarts-scatter.js",
        "assets/drag/js/user/ui/miniui/3.9.6/components/mc-ui-components.js",
        "assets/drag/js/user/ui/miniui/3.9.6/components/mc-test-tag.js",
    /*由于各个ui都可以复用下面这几个通用的配置，所以我们提取出来放入magicalcoder目录*/
//    "assets/drag/js/user/ui/magicalcoder/components/mc-echarts.js",
    "assets/drag/js/user/ui/magicalcoder/components/mc-geometry.js",
    "assets/drag/js/user/ui/magicalcoder/components/mc-other.js",

])
