/*常量池*/
function Constant(){
    var env = APPLICATION_ENV.getEnv();
    var envConstantSettings = env.constant.settings;
    this.type = env.constant.type;
    this.change = env.constant.change;
    /*全局设置*/
    this.settings = {
        navigateTree:envConstantSettings.navigateTree,
        styleTool:envConstantSettings.styleTool,
        javascript:envConstantSettings.javascript,
        html:envConstantSettings.html,
        css:envConstantSettings.css,
        debug:envConstantSettings.debug,
        cache:envConstantSettings.cache,
        workspace:envConstantSettings.workspace,
        file:envConstantSettings.file,
        other:envConstantSettings.other,
        //布局器右上角有个预览按钮 在弹窗打开 预览一下实际情况 因不同ui依赖资源不同 所以采用配置方式来加载资源
        view:{
            //预览地址
            url:envConstantSettings.view.url,
            //layui弹窗配置 area[0]宽如果不写则自动根据当前选中的设备模式宽 area[1]高
            layerExtra:envConstantSettings.view.layerExtra,
            //预览页<head></head>内的样式地址
            link:[],
            //预览页<head></head>内的脚本地址
            headJs:[],
            //预览页<body></body>内的脚本地址
            bodyJs:[
                "assets/drag/js/user/iframe/iview/4.4.0/components/import.js"
            ]
        },
        //搜索节点的规则 每个节点在布局器都有一个唯一的magicalcoderid,但是有些情况部分ui会生成各种新的结构 导致我们想要操作的结构不对
        searchMagicalCoderIdRule:{
            //优先使用的搜索方式 返回包含magical-coder-id的结构 这种方式精准但是要费很多时间写每一个组件的查询方式，所以当默认搜索逻辑不行时 可以用这个来优先搜索
            firstSearch:function(elem){
                if(elem.is(".el-card__header") || elem.is(".el-card__body")){
                    return elem.parent();
                }
                return null;
            },
            //系统默认的搜索方式：true:则从里往外搜索 false则从外往里搜索
            searchParent:function (elem){//一般您要配合iframe-ui.js的fixDynamicDomAfterRender方法来同时工作
                return false;
            }
        }
    }
    this.UI_TYPE = 10;
    this.UI_NAME = "iview";
    this.UI_FRAME = "vue";
    /*响应式布局*/
    this.responsive = {
        XS:"xs",
        SM:"sm",
        MD:"md",
        LG:"lg",
        XL:"xl",
        XXL:"xxl",
    }
    this.responsiveList = [
        {id:this.responsive.XS,name:"手机",width:"320px",height:"100%",icon:"assets/drag/img/header/phone1.png"},
        {id:this.responsive.SM,name:"手机到平板",width:"576px",height:"100%",icon:"assets/drag/img/header/paid1.png"},
        {id:this.responsive.MD,name:"平板",width:"768px",height:"100%",icon:"assets/drag/img/header/notebook2.png"},
        {id:this.responsive.LG,name:"笔记本",width:"992px",height:"100%",icon:"assets/drag/img/header/pc1.png"},
        {id:this.responsive.XL,name:"电脑",width:"100%",height:"100%",icon:"assets/drag/img/header/pc1.png",checked:true},
        {id:this.responsive.XXL,name:"大屏",width:"100%",height:"100%",icon:"assets/drag/img/header/pc1.png"}
    ]
    /*左侧可拖拽的组件*/
    this.components = [];

    /*自定义组件命名空间*/
    this.tagClassMapping = {};
    /*右侧面板属性-未来版本中将废弃，逐步往rightAttribute迁移*/
    this.rightPanel = [];
    /*新版右侧属性面板 可以控制tab标签个数*/
    this.rightAttribute = {}
    //布局器初始化后加载的默认脚本
    this.defaultJavascript = '//ajax库采用axios\n' +
        '//调试:打开浏览器控制台(F12),在代码中某行增加 debugger 即可调试\n' +
        'var vueData = {};\n' +
        'var vueMethod = {};\n' +
        'var vueMounted = function(){}\n' +
        '/*注意以上代码由系统维护,非专业人士请勿修改*/\n' +
        'var myMethod = {\n'+
        '   tabClick:function(tab){//标签页点击记住位置\n' +
        '         if(typeof iframeUi!= "undefined"){//发生在布局器内\n' +
        '             var vnodeData = tab.$parent._self.$vnode.data;\n' +
        '             if(vnodeData["model"]){//v-model藏得太深了\n' +
        '                 iframeUi.vueApi().setVueData(vnodeData.model.expression,"\\""+tab.name+"\\"");\n' +
        '               }\n' +
        '         }\n' +
        '           //解决内部如果放echarts则需要初始化一下\n'+
        '         setTimeout(function(){new IframeComponents().execute({action:"resize"});},200)\n' +
        '     }\n' +
        '}\n' +
        'for (var key in myMethod) {\n' +
        '    vueMethod[key] = myMethod[key];\n' +
        '}\n' +
        'var myMounted = null\n' +
        'if(myMounted!==null){vueMounted=myMounted;}\n' +
        'var myData = {}\n' +
        '/*把您定义的数据覆盖布局器自动识别的变量,考虑到兼容性，请下载查看head中重写的assign方法*/\n' +
        'Object.assign(vueData, myData);\n' +
        'window.vueData = vueData;\n'+
        'var _t = this;\n' +
        'var Ctor = Vue.extend({\n' +
        '    //提前绑定的变量\n' +
        '    data: function() {\n' +
        '        return vueData;\n' +
        '    },\n' +
        '    //页面加载完 会执行方法 可以做一些初始化操作\n' +
        '    mounted: vueMounted,\n' +
        '    /*当前页面绑定的方法集合 与布局器节点一一映射即可 参照element ui文档*/\n' +
        '    methods: vueMethod\n' +
        '});\n' +
        'new Ctor().$mount(\'#magicalDragScene\');\n'+
        '//执行自定义的组件渲染逻辑\n'+
        'new IframeComponents().execute();\n';
        this.refactor();
}
/*有些配置都是通用的 一个个写实在是麻烦，写个算法自动填充*/
Constant.prototype.refactor = function(){
    /*真正的安装组件*/
    MagicalCoder.installConstantComponents({tagClassMapping:this.tagClassMapping,dragItems:this.components,rightAttribute:this.rightAttribute});
    {//兼容下ie ie系列无法拖拽普通的input之类的控件，咱们在此加上tmpShade
        if(APPLICATION_ENV['addTmpShadeWhileIe']){
            APPLICATION_ENV.addTmpShadeWhileIe(this.tagClassMapping,['a-input','a-input-search','a-date-picker','a-range-picker','a-textarea','a-input-number'])
        }
    }
    //绑定通用v-for
    {
        for(var key in this.tagClassMapping){
            var mapping = this.tagClassMapping[key];
            var bind = mapping.bind;
            if(!bind){
                bind = {'v-for':"[]"}
            }else {
                bind['v-for']="[]";
            }
            mapping.bind = bind;
        }
    }
    /*左侧可自定义拖拽组件区域的渲染逻辑*/
    {
        var mcTools = new McTools();
        var _t = this;
        this.componentMap={};
        this.leftComponentsRender = {
                /*包含html的容器 jquery对象 */
                container:$(".magicalcoder-left-config"),
                /*自己构造左侧的html 如果不想这里构造 可以采用下面的render来做
                * @param.components 上面自定义的组件
                * @param.container 容器
                * */
                html:function (param) {
                    return null;
                },
                /*
                * html构造完后的事件渲染 您也可以自己构造渲染
                * @param.components 组件
                * @param.container 容器
                * */
                render:function (param) {
                    _t.componentMap = {};//清空
                    var titles = [];
                    var contents = [];
                    var idx = 0;
                    var components = param.components;
                    for(var i=0;i<components.length;i++){
                        var component = components[i];
                        var children = component.children;
                        if(children && children.length>0){//注意此处child已经通过_autoSetComponentId给了id
                            var htmlArr = []

                            var titleHtml = component.icon?('<i class="iconfont '+component.icon+'"></i><div>'+component.name+'</div>'):component.name;
                            titles.push({html:titleHtml});

                            var isLeaf = false;
                            for(var j=0;j<children.length;j++){
                                var child = children[j];
                                var icon = child.icon;
                                var innerChildren = child.children;
                                if(innerChildren){//示例里就做了3层 自己可以自行升级更多层 其他
                                    isLeaf = false;
                                    htmlArr.push('<div class="layui-colla-item">')
                                        htmlArr.push('<h6 class="layui-colla-title">'+child.name+'</h6>')
                                        htmlArr.push('<div class="layui-colla-content layui-show">')
                                            htmlArr.push('<ul class="magicalcoder-left-drag-item">');
                                            for(var k=0;k<innerChildren.length;k++){//
                                                var inner = innerChildren[k];
                                                var id = idx++;
                                                _t.componentMap[id]=inner;//放入组件map缓存 必填否则拖拽后不生效
                                                htmlArr.push(laytpl('<li component-id="{{d.id}}" class="magicalcoder-page-drag-item-label {{=d.child.clazz}}"><a>{{d.iconHtml}}<span>{{d.child.name}}</span></a></li>').render({id:id,child:inner,iconHtml:inner.icon.indexOf(".")!=-1?'<img class="iconfont" src="'+inner.icon+'">':'<i class="iconfont '+inner.icon+'"></i>'}))
                                            }
                                            htmlArr.push("</ul>")
                                        htmlArr.push("</div>")
                                    htmlArr.push("</div>")
                                }else {
                                    isLeaf = true;
                                    var id = idx++;
                                    _t.componentMap[id]=child;//放入组件map缓存  必填否则拖拽后不生效
                                    //component-id 每个可拖拽结构必须有一个id 并且必须存在 componentMap的key=id value={} 当拖拽到中间时布局器需要根据component-id从componentMap取出value并且拿到html进行渲染
                                    htmlArr.push(laytpl('<li component-id="{{d.id}}" class="magicalcoder-page-drag-item-label {{=d.child.clazz}}"><a>{{d.iconHtml}}<span>{{d.child.name}}</span></a></li>').render({id:id,child:child,iconHtml:icon.indexOf(".")!=-1?'<img class="iconfont" src="'+child.icon+'">':'<i class="iconfont '+icon+'"></i>'}))
                                }
                            }
                            if(isLeaf){
                                htmlArr.unshift('<ul class="magicalcoder-left-drag-item">');
                                htmlArr.push('</ul>');
                            }else {
                                //折叠
                                htmlArr.unshift('<div class="layui-collapse">')
                                htmlArr.push('</div>')
                            }
                            if(component.search){//添加了搜索功能
                                htmlArr.unshift('<div class="mc-left-search-components"><input class="layui-input" type="text" autocomplete="off" placeholder="搜索"/></div>')
                            }
                            contents.push({html:htmlArr.join("")})
                        }
                    }

                     mcTools.leftRightTabsHtml({
                        elem:param.container,
                        titles:titles,
                        contents:contents
                    });
                    //初始化一下折叠功能 layui
                    element.init();
                    $(".mc-left-search-components>input").on('input',function () {
                        var name = $(this).val();
                        //遍历当前结构下 有没有匹配名称的
                        $(this).parent().parent().find(".magicalcoder-page-drag-item-label").each(function (idx, item) {
                            var componentId = $(this).attr("component-id");
                            var component = _t.componentMap[componentId];
                            if(component){
                                if(!name){
                                    $(this).show();
                                }else {
                                    if(component.name.indexOf(name)!=-1){//找到
                                        $(this).show();
                                    }else {
                                        $(this).hide();
                                    }
                                }

                            }
                        })
                    })

                    //下面是点击左侧结构 自动加入工作区的写法 如果您不需要 也可以忽略
                    var $li = param.container.find(".magicalcoder-page-drag-item-label")
                    $li.unbind('click').bind('click',function () {
                        MAGICAL_CODER_API.clickLeftDomAddToWorkspace({elem:$(this)});
                    })
                },
                /*
                * 被拖拽元素的父亲jquery对象 布局器会执行each方法
                * 构造完左侧结构后，我们需要绑定拖拽事件中，这里就是ul，那么它下面的li将自动被绑定拖拽事件
                * */
                dragItemContainer:function (){ return $(".magicalcoder-left-config .magicalcoder-left-drag-item")}
            }
    }
    /*通用属性*/
//    this.commonAttrConfig();
}
/*如果您改动了上述配置 记得调用此方法 刷新一下*/
Constant.prototype.refresh = function(){}
/*可以通用的配置*/
Constant.prototype.commonAttrConfig = function(){
     //一些通用配置
     var firstRightPanel = this.rightPanel[0];
     var content = firstRightPanel.content;
     for(var key in content){
         var config = content[key];
         config.push({type:this.type.SWITCH,clearAttr:true,oneLine:true,change:this.change.CLASS,title:'打印',options:[{"c":"","n":"","t":"","u":"no-print"}]});
         config.push({type:this.type.CHECKBOX        ,clearAttr:true     ,oneLine:true     ,change:this.change.ATTR     ,title:'显隐'    ,options:[{"c":"false","n":"v-if","t":"隐藏","u":"true"}]});
         config.push({type:this.type.TEXT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'z-index',title:'层级',tooltip:'当重叠时可以用数值决定哪个控件置于上层',validate:{"^[0-9]*$":"请使用数字"}})
         config.push({type:this.type.FILEUPLOAD,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-image',title:'背景图',tooltip:'背景图片',callback:{coverValueToAttr:function(value,focusNode){return "url(\""+value+"\")"}}});
         config.push({type:this.type.TEXT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-size',title:'背景图尺寸',tooltip:"背景图尺寸:配置规则 宽 高<br>100px 200px<br>20% 30%",placeholder:"例如:10px 10px"});
         config.push({type:this.type.SELECT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-repeat',title:'背景图重复',tooltip:'背景图重复',options:[{"repeat":"默认"},{"repeat-x":"水平铺满"},{"repeat-y":"垂直铺满"},{"no-repeat":"仅显示一次"},{"inherit":"继承外层"}]});
         config.push({type:this.type.SELECT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-position',title:'背景图重复',tooltip:'背景图定位',options:[{"center center":"居中"},{"left top":"靠上靠左"},{"center top":"靠上居中"},{"right top":"靠上靠右"},{"left center":"靠左居中"},{"right center":"靠右居中"},{"left bottom":"靠下靠左"},{"center bottom":"靠下居中"},{"right bottom":"靠下靠右"}]});
         config.push({type:this.type.SELECT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'background-attachment',title:'滚动关联',tooltip:'滚动关联',options:[{"scroll":"跟随滚动"},{"fixed":"固定"}]});
         config.push({type:this.type.TEXT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'width',title:'宽',tooltip:'宽'});
         config.push({type:this.type.TEXT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'height',title:'高',tooltip:'高'});
         config.push({type:this.type.TEXT,clearAttr:true,oneLine:true,change:this.change.STYLE,attrName:'left',title:'左',tooltip:'左'});
     }
}

Constant.prototype.getComponents = function(){
    return this.components;
}

Constant.prototype.getSettings = function(){
    return this.settings;
}

Constant.prototype.getComponentMap = function(){
    return this.componentMap;
}

Constant.prototype.getRightPanel = function(){
    return this.rightPanel;
}

Constant.prototype.getTagClassMapping = function(){
    return this.tagClassMapping;
}
Constant.prototype.getChange = function () {
    return this.change;
}
Constant.prototype.getType = function () {
    return this.type;
}
Constant.prototype.getResponsiveList = function () {
    return this.responsiveList;
}
Constant.prototype.getResponsive = function () {
    return this.responsive;
}
Constant.prototype.getUiType = function () {
    return this.UI_TYPE;
}
/*为脚本编辑器准备参数*/
Constant.prototype.magicalJsCodeRebuildParams = function () {

}
