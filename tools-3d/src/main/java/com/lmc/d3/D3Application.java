package com.lmc.d3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-11-06 14:38
 * @version: 1.0
 */
@SpringBootApplication
public class D3Application {

    public static void main(String[] args) {
        SpringApplication.run(D3Application.class, args);
    }

}
