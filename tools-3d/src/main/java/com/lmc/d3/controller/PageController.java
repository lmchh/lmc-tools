package com.lmc.d3.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-11-06 14:43
 * @version: 1.0
 */
@Controller
@RequestMapping("page")
public class PageController {

    @RequestMapping("example.html")
    public String example() {
        return "example";
    }

    @RequestMapping("factory.html")
    public String factory() {
        return "factory";
    }

    @RequestMapping("test.html")
    public String teset() {
        return "test";
    }

}
