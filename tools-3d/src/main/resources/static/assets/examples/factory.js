var scene,
    camera,
    renderer,
    controls,
    dragControls,
    light,
    modelBox3, meshBox3, explode,
    phone01, phone02, phone03,
    mouseObject,
    stationMap = [{}, {}, {}],
    spriteMap = [{}, {}, {}],
    station_result_echart, charge_echart, humity_echart,
    line_station01_echart, line_station02_echart, total_panel_echart, total_retest_echart,
    stations = []

// 场景
function initScene() {
    scene = new THREE.Scene()
    // scene.background = new THREE.Color(0xffffff)
}

// 相机
function initCamera() {
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 5, 3000);
    camera.position.set(-96, 1120, 965)
    camera.lookAt(new THREE.Vector3(0, 0, 0))
    // camera.position.set(-550, 160, 200)
    // camera.lookAt(new THREE.Vector3(1200, 100, 200))
}

// 渲染器
function initRenderer() {
    renderer = new THREE.WebGLRenderer({
        antialias: true,
        // logarithmicDepthBuffer: true
    })
    renderer.setSize(window.innerWidth, window.innerHeight)
    document.getElementById("3d-container").appendChild(renderer.domElement)
}

// 窗口变动触发的方法
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
    renderer.setSize(window.innerWidth, window.innerHeight)
}

// 初始化轨迹球控件
function initControls() {
    controls = new THREE.TrackballControls(camera, renderer.domElement)
}

// 初始化灯光
function initLight() {
    light = new THREE.SpotLight(0xffffff, 1.1)
    light.position.set(-400, 1400, 850)
    scene.add(light)
    let light2 = new THREE.SpotLight(0xffffff, 1.1)
    light2.position.set(1500, 1400, -750)
    scene.add(light2)

}

/**
 * 鼠标略过事件
 */
function initDragControls() {
    let objects = []
    // 将工站加入鼠标事件
    for (let i = 0; i < scene.children.length; i++) {
        if (scene.children[i].name.indexOf('-STA') != -1 && scene.children[i].name.indexOf('sprite') == -1) {
            for (let j = 0; j < scene.children[i].children[0].children.length; j++) {
                objects.push(scene.children[i].children[0].children[j])
            }
        }
    }
    // 将爆炸机台加入鼠标事件
    for (let i = 0; i < explode.children.length; i++) {
        objects.push(explode.children[i])
    }
    // 初始化拖拽控件
    dragControls = new THREE.DragControls(objects, camera, renderer.domElement)

    // 鼠标略过
    dragControls.addEventListener('hoveron', function (event) {

        // 不为空，为工站，否则为爆炸机台
        if (event.object.parent != null && event.object.parent.parent.parent != null) {
            let g = scene.getObjectByName(event.object.parent.parent.name)
            for (let i = 0; i < g.children[0].children.length; i++) {
                g.children[0].children[i].material.transparent = true
                g.children[0].children[i].material.opacity = 0.5
            }
            if(scene.getObjectByName(event.object.parent.parent.name + '-sprite') != null) {
                scene.getObjectByName(event.object.parent.parent.name + '-sprite').visible = true
            }
        }else {
            event.object.material.transparent = true
            event.object.material.opacity = 0.5
            event.object.material.emissive.r = 1

            mouseObject = event.object
        }

    })

    // 鼠標離開
    dragControls.addEventListener('hoveroff', function (event) {
        // 不为空，为工站，否则为爆炸机台
        if (event.object.parent != null && event.object.parent.parent.parent != null) {
            let g = scene.getObjectByName(event.object.parent.parent.name)
            for (let i = 0; i < g.children[0].children.length; i++) {
                g.children[0].children[i].material.transparent = true
                g.children[0].children[i].material.opacity = 1
            }
            if(scene.getObjectByName(event.object.parent.parent.name + '-sprite') != null) {
                scene.getObjectByName(event.object.parent.parent.name + '-sprite').visible = false
            }
        }else {
            event.object.material.transparent = true
            event.object.material.opacity = 1
            event.object.material.emissive.r = 0

        }

    })

}

/**
 * 鼠標点击爆炸机台展示信息
 * @param event
 */
function clickPhoneEvent(event) {

    let e = event || window.event
    // console.log("(x, y) => (" + e.screenX + ", " + e.screenY + ")")
    let textBox = document.querySelector('.text')
    textBox.style.left = (e.screenX/2) + 'px'
    textBox.style.top = (e.screenY/2) + 'px'
    textBox.style.display = 'inline-block'
    textBox.innerHTML = '组件【' + mouseObject.name + '】，更换组件请双击'


}

/**
 * 鼠标双击爆炸机台事件(更换组件)
 * @param event
 */
function dbclickPhoneEvent(event) {
    document.querySelector('.text').style.display = 'none'
    vue.openFullScreen()
    let count = 0
    let iCount =setInterval(() => {
        ++ count
        if (count == 1) {
            mouseObject.material.emissive.r = 1
        }else if (count == 2) {
            mouseObject.material.emissive.r = 0
            mouseObject.material.emissive.g = 1
        }else if (count == 3) {
            mouseObject.material.emissive.g = 0
            mouseObject.material.emissive.b = 1
        }else if (count == 4) {
            mouseObject.material.emissive.b = 0
            mouseObject.material.emissive.r = 1
        }else if (count == 5){
            mouseObject.material.emissive.r = 0
            install()   // 装机
        }
    }, 900)
    setTimeout(() => {
        clearInterval(iCount)
        explode.visible = false
        vue.isArriveFail = false
        vue.isAnimate02 = true
    }, 7000)

}

/**
 * 三维坐标转屏幕坐标的方法
 * @param position
 * @returns {{x: number, y: number}}
 */
function transPosition(position) {
    let world_vector = new THREE.Vector3(position.x, position.y, position.z);
    let vector = world_vector.project(camera);
    let halfWidth = window.innerWidth / 2,
        halfHeight = window.innerHeight / 2;
    return {
        x: Math.round(vector.x * halfWidth + halfWidth),
        y: Math.round(-vector.y * halfHeight + halfHeight)
    }
}

/**
 * 初始化
 */
function init() {
    initScene()
    initCamera()
    initRenderer()
    initContent()
    initLight()
    initControls()
    animate()
}


/**
 * 動畫
 */
function animate() {
    requestAnimationFrame(animate)
    renderer.render(scene, camera)

    // 机台运动
    if (vue.isAnimate01) {  // 摆设
        phoneMove(phone01);
        phoneToStation2(phone01, 0);
    }
    if (vue.isAnimate02) {  // 真实
        phoneMove(phone02)
        phoneToStation2(phone02, 1)
    }
    if (vue.isAnimate03) {  // 摆设
        phoneMove(phone03)
        // 机台到达工站
        phoneToStation2(phone03, 2)
    }
    // 当视角为漫游，开启漫游时，未到达fail工站时，未暂停漫游时，相机移动
    if (vue.currentPerspective == vue.perspective[1] && vue.isMoveCamera && !vue.isArriveFail && !vue.pauseMoveCamera) {
        moveCamera()
    }else {
        if (vue.currentPerspective == vue.perspective[0]) {
            controls.update()
            renderer.render( scene, camera )
        }
    }

    if (vue.explode.isPlay) {
        if (vue.explode.current <= vue.explode.max && vue.explode.current >= 0) {
            if (vue.explode.isOut) {
                vue.explode.current += 0.5
            }else {
                vue.explode.current -= 0.5
            }
            explode.traverse(value => {
                if (value.isMesh) {
                    value.position.copy(
                        new THREE.Vector3().copy(value.userData.oldPs)
                            .add(new THREE.Vector3().copy(value.worldDir).multiplyScalar(vue.explode.current))
                    )
                }
            })
        }else {
            vue.explode.isPlay = false
            // explode.visible = false
        }
    }

}

/**
 * 拆机
 */
function disassemble() {
    explode.visible = true
    explode.position.set(-250, -100, -150)
    vue.explode.isPlay = true
    vue.explode.current = 0
    vue.explode.isOut = true

}

/**
 * 装机
 */
function install() {
    explode.visible = true
    explode.position.set(-250, -100, -150)
    vue.explode.isPlay = true
    vue.explode.current = vue.explode.max
    vue.explode.isOut = false
}

/**
 * 线体-工站加载初始化
 * @param s
 * @param g
 */
function loadGltfPrefix(s, g) {
    for (let i = 0; i < s.children.length; i++) {
        s.children[i].scale.x = 500
        s.children[i].scale.y = 500
        s.children[i].scale.z = 500
        g.add(s.children[i])
    }
    g.scale.set(0.15, 0.15, 0.15)
    g.position.set(-800, 15, -450)
    g.rotation.x = -Math.PI/2
    g.rotation.z=Math.PI/2
}

/**
 * 机台在产线移动
 * @param phone
 */
function phoneMove(phone) {
    if(phone == undefined){
        return
    }
    let start = -940
    let end = 980
    if (phone.position.x <= end) {
        phone.position.x += 2
    }else {
        phone.position.x = start
    }
}

/**
 * 机台到某个工站处理
 * @param phone
 */
function phoneToStation(phone, num) {
    let x_position = phone.position.x
    if (num == 1) {
        // 经过工站，测试fail
        if (x_position == -250) {
            vue.isAnimate02 = false
            let station = stationMap[num][String(x_position)]
            for (let i = 0; i < station.children.length; i++) {
                if (station.children[i].isMesh) {
                    station.children[i].material.transparent = true
                    station.children[i].material.opacity = 0.5
                }
            }
            vue.station = station.userData
            vue.isDefault = false
            // 播放影音
            vue.showVideo01 = true
            document.getElementById("video01").muted = true
            document.getElementById("video02").muted = true
            document.getElementById("video01").play()

            // 视频结束时关闭音影
            document.getElementById("video01").addEventListener('ended',function () {
                if (vue.showVideo01) {
                    vue.showVideo01 = false
                    // document.getElementById("video01").pause()
                    vue.openModal(station)
                }
            }, false);


            // 经过工站，测试pass时
        }else if (x_position == -850 || x_position == -690 || x_position == -550 || x_position == -390 || x_position == -88 || x_position == 70 ||
            x_position == 200 || x_position == 360 || x_position == 510 || x_position == 660 || x_position == 820) {
            vue.isAnimate02 = false
            let station = stationMap[num][String(x_position)]
            for (let i = 0; i < station.children.length; i++) {
                if (station.children[i].isMesh) {
                    station.children[i].material.transparent = true
                    station.children[i].material.opacity = 0.5
                }
            }
            vue.station = station.userData
            vue.isDefault = false
            setTimeout(() => {
                vue.isAnimate02 = true
                vue.isDefault = true
                // station.scale.z = 0.15
            }, 2000)

        }

    }
}

/**
 * 机台达到工站处理（摆设，静止3秒）
 * @param phone
 * @param num
 */
function phoneToStation2(phone, num) {
    let x_position = phone.position.x
    if (num == 0) {
        if (x_position == -850 || x_position == -690 || x_position == -550 || x_position == -390 || x_position == -88 || x_position == -250 ||
            x_position == -88 || x_position == 70 || x_position == 200 || x_position == 360 || x_position == 510 || x_position == 660 || x_position == 820) {
            vue.isAnimate01 = false
            spriteMap[num][x_position].visible = true
            setTimeout(() => {
                vue.isAnimate01 = true
                spriteMap[num][x_position].visible = false
            }, 1500)
        }
    }else if (num == 1) {
        if (x_position == -850 || x_position == -690 || x_position == -550 || x_position == -390 || x_position == -88 || x_position == -88 ||
             x_position == 70 || x_position == 200 || x_position == 360 || x_position == 510 || x_position == 660 || x_position == 820) {
            vue.isAnimate02 = false
            setTimeout(() => {
                vue.isAnimate02 = true
            }, 1500)
        }else if (x_position == -250) {
            vue.isAnimate02 = false
        }
    }else if (num == 2) {
        if (x_position == -850 || x_position == -690 || x_position == -550 || x_position == -390 || x_position == -88 || x_position == -250 ||
            x_position == -88 || x_position == 70 || x_position == 200 || x_position == 360 || x_position == 510 || x_position == 660 || x_position == 820) {
            vue.isAnimate03 = false
            setTimeout(() => {
                vue.isAnimate03 = true
            }, 1500)
        }
    }

}

/**
 * 相机漫游
 */
function moveCamera() {
    if (camera.position.x <= 700) {
        camera.position.x = (camera.position.x + 1)
        let x_position = camera.position.x + 300
        let new_position = camera.position.x + 150
        if (x_position == -850 || x_position == -690 || x_position == -550 || x_position == -390 ||
            x_position == -88 || x_position == 70 || x_position == 200 ||
            x_position == 360 || x_position ==  510 || x_position == 660 || x_position == 820) {
            spriteMap[1][x_position + ''].visible = true
            let sta = stationMap[1][x_position + '']
            for (let i = 0; i < sta.children[0].children.length; i++) {
                sta.children[0].children[i].material.transparent = true
                sta.children[0].children[i].material.opacity = 0.5
                sta.children[0].children[i].material.emissive.b = 1
            }
        }else if (x_position == -250) {
            spriteMap[1][x_position + ''].visible = true
            let sta = stationMap[1][x_position + '']
            for (let i = 0; i < sta.children[0].children.length; i++) {
                sta.children[0].children[i].material.transparent = true
                sta.children[0].children[i].material.opacity = 0.5
                sta.children[0].children[i].material.emissive.r = 1
            }
            vue.isArriveFail = true
            spriteMap[1]['-390'].visible = false

            setTimeout(() => {
                spriteMap[1][x_position + ''].visible = false
                scene.getObjectByName('error-sprite').visible = true
                setTimeout(() => {
                    // 显示机台
                    scene.getObjectByName('error-sprite').visible = false
                    setTimeout(() => {
                        disassemble() // 拆机
                    }, 500)
                }, 3000)
            }, 2000)

            // 离开失败工站
            // vue.isArriveFail = false

        }
        if (new_position == -850 || new_position == -690 || new_position == -550 || new_position == -390 ||
            new_position == -88 || new_position == 70 || new_position == 200 ||
            new_position == 360 || new_position ==  510 || new_position == 660 || new_position == 820) {
            spriteMap[1][new_position + ''].visible = false
            let sta = stationMap[1][new_position + '']
            for (let i = 0; i < sta.children[0].children.length; i++) {
                sta.children[0].children[i].material.transparent = true
                sta.children[0].children[i].material.opacity = 1
                sta.children[0].children[i].material.emissive.b = 0
            }
        }else if (new_position == -250) {
            let sta = stationMap[1][new_position + '']
            for (let i = 0; i < sta.children[0].children.length; i++) {
                sta.children[0].children[i].material.transparent = true
                sta.children[0].children[i].material.opacity = 1
                sta.children[0].children[i].material.emissive.r = 0
            }
        }
    }else {
        // vue.isMoveCamera = false
        // camera.lookAt(new THREE.Vector3(0, 0, 0))
        // camera.position.set(-96, 1120, 965)
        // vue.isEndMoveCamera = true

        vue.currentPerspective = vue.perspective[0]
    }
}

/**
 * 初始化精灵
 */
function initSprite() {
    var texture = new THREE.TextureLoader().load("/model/img/station101.png")
    // 创建精灵材质对象SpriteMaterial
    var spriteMaterial = new THREE.SpriteMaterial({
        color:0xffffff,//设置精灵矩形区域颜色
        // rotation:Math.PI/4,//旋转精灵对象45度，弧度值
        map: texture,//设置精灵纹理贴图
        transparent: true,
        opacity: 0.5
    })
    // 创建精灵模型对象，不需要几何体geometry参数
    var sprite = new THREE.Sprite(spriteMaterial)
    sprite.name = 'LINE1-STA01-sprite'
    sprite.visible = false
    scene.add(sprite);
    // 控制精灵大小，比如可视化中精灵大小表征数据大小
    sprite.scale.set(270, 162, 1);  //只需要设置x、y两个分量就可以
    sprite.position.set(-850, 230, -380)
    spriteMap[0]['-850'] = sprite

    let station102 = sprite.clone()
    station102.name = 'LINE1-STA02-sprite'
    station102.position.set(-690, 230, -380)
    scene.add(station102)
    spriteMap[0]['-690'] = station102

    let station103 = sprite.clone()
    station103.name = 'LINE1-STA03-sprite'
    station103.position.set(-550, 230, -500)
    scene.add(station103)
    spriteMap[0]['-550'] = station103

    let station104 = sprite.clone()
    station104.name = 'LINE1-STA04-sprite'
    station104.position.set(-390, 230, -380)
    scene.add(station104)
    spriteMap[0]['-390'] = station104

    let station105 = sprite.clone()
    station105.name = 'LINE1-STA05-sprite'
    station105.position.set(-250, 230, -380)
    scene.add(station105)
    spriteMap[0]['-250'] = station105

    let station106 = sprite.clone()
    station106.name = 'LINE1-STA06-sprite'
    station106.position.set(-88, 230, -380)
    scene.add(station106)
    spriteMap[0]['-88'] = station106

    let station107 = sprite.clone()
    station107.name = 'LINE1-STA07-sprite'
    station107.position.set(70, 230, -380)
    scene.add(station107)
    spriteMap[0]['70'] = station107

    let station108 = sprite.clone()
    station108.name = 'LINE1-STA08-sprite'
    station108.position.set(200, 230, -500)
    scene.add(station108)
    spriteMap[0]['200'] = station108

    let station109 = sprite.clone()
    station109.name = 'LINE1-STA09-sprite'
    station109.position.set(360, 230, -380)
    scene.add(station109)
    spriteMap[0]['360'] = station109

    let station110 = sprite.clone()
    station110.name = 'LINE1-STA10-sprite'
    station110.position.set(510, 230, -380)
    scene.add(station110)
    spriteMap[0]['510'] = station110

    let station111 = sprite.clone()
    station111.name = 'LINE1-STA11-sprite'
    station111.position.set(660, 230, -500)
    scene.add(station111)
    spriteMap[0]['660'] = station111

    let station112 = sprite.clone()
    station112.name = 'LINE1-STA12-sprite'
    station112.position.set(820, 230, -380)
    scene.add(station112)
    spriteMap[0]['820'] = station112

    // ===============================================================================
    // 创建精灵模型对象，不需要几何体geometry参数
    var sprite02 = new THREE.Sprite(spriteMaterial)
    sprite02.name = 'LINE2-STA01-sprite'
    scene.add(sprite02);
    sprite02.visible = false
    // 控制精灵大小，比如可视化中精灵大小表征数据大小
    sprite02.scale.set(200, 120, 1);  //只需要设置x、y两个分量就可以
    sprite02.position.set(-800, 180, 100)
    spriteMap[1]['-850'] = sprite02

    let station202 = sprite02.clone()
    station202.name = 'LINE2-STA02-sprite'
    station202.position.set(-660, 150, 80)
    scene.add(station202)
    spriteMap[1]['-690'] = station202

    let station203 = sprite02.clone()
    station203.name = 'LINE2-STA03-sprite'
    station203.position.set(-500, 180, 100)
    scene.add(station203)
    spriteMap[1]['-550'] = station203

    let station204 = sprite02.clone()
    station204.name = 'LINE2-STA04-sprite'
    station204.position.set(-360, 150, 80)
    scene.add(station204)
    spriteMap[1]['-390'] = station204

    let station205 = sprite02.clone()
    station205.name = 'LINE2-STA05-sprite'
    station205.position.set(-230, 180, 100)
    scene.add(station205)
    spriteMap[1]['-250'] = station205

    let station206 = sprite02.clone()
    station206.name = 'LINE2-STA06-sprite'
    station206.position.set(-58, 150, 80)
    scene.add(station206)
    spriteMap[1]['-88'] = station206

    let station207 = sprite02.clone()
    station207.name = 'LINE2-STA07-sprite'
    station207.position.set(70, 120, 100)
    scene.add(station207)
    spriteMap[1]['70'] = station207

    let station208 = sprite02.clone()
    station208.name = 'LINE2-STA08-sprite'
    station208.position.set(240, 180, 100)
    scene.add(station208)
    spriteMap[1]['200'] = station208

    let station209 = sprite02.clone()
    station209.name = 'LINE2-STA09-sprite'
    station209.position.set(360, 180, -20)
    scene.add(station209)
    spriteMap[1]['360'] = station209

    let station210 = sprite02.clone()
    station210.name = 'LINE2-STA10-sprite'
    station210.position.set(510, 180, 100)
    scene.add(station210)
    spriteMap[1]['510'] = station210

    let station211 = sprite02.clone()
    station211.name = 'LINE2-STA11-sprite'
    station211.position.set(660, 180, -20)
    scene.add(station211)
    spriteMap[1]['660'] = station211

    let station212 = sprite02.clone()
    station212.name = 'LINE2-STA12-sprite'
    station212.position.set(820, 180, 100)
    scene.add(station212)
    spriteMap[1]['820'] = station212

    texture = new THREE.TextureLoader().load("/model/img/error02.png")
    spriteMaterial = new THREE.SpriteMaterial({
        color:0xffffff,//设置精灵矩形区域颜色
        // rotation:Math.PI/4,//旋转精灵对象45度，弧度值
        map: texture,//设置精灵纹理贴图
        transparent: true,
        opacity: 0.6
    })
    let errorSprite = new THREE.Sprite(spriteMaterial)
    errorSprite.name = 'error-sprite'
    scene.add(errorSprite);
    errorSprite.visible = false
    // 控制精灵大小，比如可视化中精灵大小表征数据大小
    errorSprite.scale.set(175, 105, 1);  //只需要设置x、y两个分量就可以
    errorSprite.position.set(-400, 155, 200)


}


stations = [
    {
        name: 'STA01',
        total: 17,
        pass: 16,
        fail: 1,
        rate: 'Pass:' + (Math.ceil(200/220 * 100) / 100) + '%',
        yieldrate: '93%',
        retestcount: 3,
        retestrate: '6.79%',
        topissue: 283,
    },
    {
        name: 'STA02',
        total: 23,
        pass: 20,
        fail: 3,
        rate: 'Pass:' + (Math.ceil(180/200 * 100) / 100) + '%',
        yieldrate: '90%',
        retestcount: 5,
        retestrate: '6.71%',
        topissue: 275,
    },
    {
        name: 'STA03',
        total: 26,
        pass: 20,
        fail: 6,
        rate: 'Pass:' + (Math.ceil(200/220 * 100) / 100) + '%',
        yieldrate: '93%',
        retestcount: 6,
        retestrate: '6.79%',
        topissue: 283,
    },
    {
        name: 'STA04',
        total: 9,
        pass: 7,
        fail: 2,
        rate: 'Pass:' + (Math.ceil(180/200 * 100) / 100) + '%',
        yieldrate: '90%',
        retestcount: 5,
        retestrate: '6.71%',
        topissue: 275,
    },
    {
        name: 'STA05',
        total: 22,
        pass: 21,
        fail: 1,
        rate: 'Pass:' + (Math.ceil(200/220 * 100) / 100) + '%',
        yieldrate: '93%',
        retestcount: 7,
        retestrate: '6.79%',
        topissue: 283,
    },
    {
        name: 'STA06',
        total: 33,
        pass: 26,
        fail: 7,
        rate: 'Pass:' + (Math.ceil(180/200 * 100) / 100) + '%',
        yieldrate: '90%',
        retestcount: 7,
        retestrate: '6.71%',
        topissue: 275,
    },
    {
        name: 'STA07',
        total: 37,
        pass: 35,
        fail: 2,
        rate: 'Pass:' + (Math.ceil(200/220 * 100) / 100) + '%',
        yieldrate: '93%',
        retestcount: 5,
        retestrate: '6.79%',
        topissue: 283,
    },
    {
        name: 'STA08',
        total: 39,
        pass: 32,
        fail: 7,
        rate: 'Pass:' + (Math.ceil(180/200 * 100) / 100) + '%',
        yieldrate: '90%',
        retestcount: 15,
        retestrate: '6.71%',
        topissue: 275,
    },
    {
        name: 'STA09',
        total: 49,
        pass: 45,
        fail: 4,
        rate: 'Pass:' + (Math.ceil(200/220 * 100) / 100) + '%',
        yieldrate: '93%',
        retestcount: 10,
        retestrate: '6.79%',
        topissue: 283,
    },
    {
        name: 'STA10',
        total: 41,
        pass: 38,
        fail: 3,
        rate: 'Pass:' + (Math.ceil(180/200 * 100) / 100) + '%',
        yieldrate: '90%',
        retestcount: 6,
        retestrate: '6.71%',
        topissue: 275,
    },
    {
        name: 'STA11',
        total: 33,
        pass: 26,
        fail: 7,
        rate: 'Pass:' + (Math.ceil(200/220 * 100) / 100) + '%',
        yieldrate: '93%',
        retestcount: 4,
        retestrate: '6.79%',
        topissue: 283,
    },
    {
        name: 'STA12',
        total: 60,
        pass: 54,
        fail: 6,
        rate: 'Pass:' + (Math.ceil(180/200 * 100) / 100) + '%',
        yieldrate: '90%',
        retestcount: 8,
        retestrate: '6.71%',
        topissue: 275,
    },
]