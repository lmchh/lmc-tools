var scene, camera, renderer, controls, light, isAnimate, threeOnEvent, direction = true, clickObjects = [];

// 场景
function initScene() {
    scene = new THREE.Scene();
    // scene.background = new THREE.Color(0xffffff)
}

// 相机
function initCamera() {
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.01, 10000);
    camera.position.set(0, 1500, 1150);
    camera.lookAt(new THREE.Vector3(0, 0, 0));
}

// 渲染器
function initRenderer() {
    renderer = new THREE.WebGLRenderer({
        antialias: true,
        logarithmicDepthBuffer: true
    });
    renderer.setSize(window.innerWidth, window.innerHeight);
    // document.body.appendChild(renderer.domElement);
    document.getElementById("3d-container").appendChild(renderer.domElement)
}

// 窗口变动触发的方法
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

// 初始化轨迹球控件
function initControls() {
    controls = new THREE.TrackballControls(camera, renderer.domElement);
}

// 添加拖拽控件和綁定點擊事件
function initDragControls() {
    // 添加平移控件
    var transformControls = new THREE.TransformControls(camera, renderer.domElement);
    scene.add(transformControls);
    // 添加轨道控制控件，可以使摄像机可以围绕目标旋转
    // var orbitControls = new THREE.OrbitControls(camera, renderer.domElement)
    // 过滤不是 Mesh 的物体,例如辅助网格
    var objects = [];
    for (let i = 0; i < scene.children.length; i++) {

        if (scene.children[i].isMesh) {
            objects.push(scene.children[i]);
        }
        if (scene.children[i].isGroup) {
            for (let j = 0; j < scene.children[i].children.length; j++) {
                if(scene.children[i].children[j].isMesh) {
                    objects.push(scene.children[i].children[j])
                }
            }
        }

    }

}

// 初始化灯光
function initLight() {
    light = new THREE.SpotLight(0xffffff, 1.1);
    light.position.set(-400, 1400, 850);
    scene.add(light);
    let light2 = new THREE.SpotLight(0xffffff, 1.1);
    light2.position.set(1500, 1400, -750)
    scene.add(light2)


    // let light3 = new THREE.SpotLight(0xffffff, 0.6);
    // light3.position.set(1300, 400, 0)
    // scene.add(light3)
}