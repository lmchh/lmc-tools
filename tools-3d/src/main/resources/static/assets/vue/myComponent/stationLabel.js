const stationLabel = {
    template: `
        <div v-show="isdefault == true ? false : true">
           <div class="el-notification left"  style="top: 16px;z-index: 1000;width: 290px;height: 97%;">
                <div class="el-notification-group" style="width: 250px">
                    <h2 style="color: red">{{station.name}}</h2>
                    <div class="el-notification-content">
                       <div style="margin-top: -30px;margin-left: 190px">
                           <el-progress type="circle" :percentage="Math.ceil(station.pass/station.total*100)"  :width="75" :height="75"></el-progress>
                        </div>
                        <div id="station-result" style="width: 210px;height:180px;font-size: 14px"></div>
<!--                        <p style="margin-top: 0px;margin-bottom: 5px"><el-tag>总测试数: {{station.total}}</el-tag></p>-->
<!--                        <p style="margin-top: 0px;margin-bottom: 5px"><el-tag type="danger">FAIL数: {{station.fail}}</el-tag></p></p>-->
<!--                        <p style="margin-top: 0px;margin-bottom: 5px"><el-tag type="success">PASS数: {{station.pass}}</el-tag></p>-->
<!--                        <p style="margin-top: 0px;margin-bottom: 5px"><el-tag type="warning">良率: {{station.yieldrate}}</el-tag></p>-->
<!--                        <p style="margin-top: 0px;margin-bottom: 5px"><el-tag type="danger">重测数: {{station.retestcount}}</el-tag></p>-->
<!--                        <p style="margin-top: 0px;margin-bottom: 5px"><el-tag type="warning">重测率: {{station.retestrate}}</el-tag></p>-->
<!--                        <p style="margin-top: 0px;margin-bottom: 5px"><el-tag type="info">Top Issue: {{station.topissue}}</el-tag></p>-->
                            <span>
                                <el-tag style="font-size: 16px;margin-top:3px;margin-bottom:3px">总测试数: {{station.total}}</el-tag>
                                <el-tag style="font-size: 16px;margin-top:3px;margin-bottom:3px" type="danger">FAIL数: {{station.fail}}</el-tag>
                                <el-tag style="font-size: 16px;margin-top:3px;margin-bottom:3px" type="success">PASS数: {{station.pass}}</el-tag>
                                <el-tag style="font-size: 16px;margin-top:3px;margin-bottom:3px" type="warning">良率: {{station.yieldrate}}</el-tag>
                                <el-tag style="font-size: 16px;margin-top:3px;margin-bottom:3px" type="danger">重测数: {{station.retestcount}}</el-tag>
                                <el-tag style="font-size: 16px;margin-top:3px;margin-bottom:3px" type="warning">重测率: {{station.retestrate}}</el-tag>
                                <el-tag style="font-size: 16px;margin-top:3px;margin-bottom:3px" type="info">Top Issue: {{station.topissue}}</el-tag>
                            </span>
                            <el-row>
                                     <div style="text-align: center;font-size: 18px;font-weight: bolder;margin-bottom: 2px">Yield Rate</div>
                                     <el-row>
                                        <el-col :span="5">
                                            <div class="grid-content bg-purple" style="font-size: 14px;margin-right: 2px;font-family:Lucida Console">STA01</div>
                                        </el-col>
                                        <el-col :span="19">
                                            <div class="grid-content bg-purple-light">
                                                <el-progress :text-inside="true" :stroke-width="16" :width="300" :percentage="Math.ceil((stations[0].total-stations[0].retestcount)/stations[0].total*100)" status="success"></el-progress>
                                            </div>
                                        </el-col>
                                    </el-row>
                                    <div style="margin: 5px"></div>
                                    <el-row>
                                        <el-col :span="5">
                                            <div class="grid-content bg-purple" style="font-size: 14px;margin-right: 2px;font-family:Lucida Console">STA02</div>
                                        </el-col>
                                        <el-col :span="19">
                                            <div class="grid-content bg-purple-light">
                                                <el-progress :text-inside="true" :stroke-width="16" :width="300" :percentage="Math.ceil((stations[1].total-stations[1].retestcount)/stations[1].total*100)" status="success"></el-progress>
                                            </div>
                                        </el-col>
                                    </el-row>
                                    <div style="margin: 3px"></div>
                                    <el-row>
                                        <el-col :span="5">
                                            <div class="grid-content bg-purple" style="font-size: 14px;margin-right: 2px;font-family:Lucida Console">STA03</div>
                                        </el-col> 
                                        <el-col :span="19">
                                            <div class="grid-content bg-purple-light">
                                                <el-progress :text-inside="true" :stroke-width="16" :width="300" :percentage="Math.ceil((stations[2].total-stations[2].retestcount)/stations[2].total*100)" status="success"></el-progress>
                                            </div>
                                        </el-col>
                                    </el-row>        
                                    <div style="margin: 3px"></div>
                                    <el-row>
                                        <el-col :span="5">
                                            <div class="grid-content bg-purple" style="font-size: 14px;margin-right: 2px;font-family:Lucida Console">STA04</div>
                                        </el-col> 
                                        <el-col :span="19">
                                            <div class="grid-content bg-purple-light">
                                                <el-progress :text-inside="true" :stroke-width="16" :width="300" :percentage="Math.ceil((stations[3].total-stations[3].retestcount)/stations[3].total*100)" status="success"></el-progress>
                                            </div>
                                        </el-col>
                                    </el-row>        
                                    <div style="margin: 3px"></div>
                                    <el-row>
                                        <el-col :span="5">
                                            <div class="grid-content bg-purple" style="font-size: 14px;margin-right: 2px;font-family:Lucida Console">STA05</div>
                                        </el-col> 
                                        <el-col :span="19">
                                            <div class="grid-content bg-purple-light">
                                                <el-progress :text-inside="true" :stroke-width="16" :width="300" :percentage="Math.ceil((stations[4].total-stations[4].retestcount)/stations[4].total*100)" status="success"></el-progress>
                                            </div>
                                        </el-col>
                                    </el-row>        
                                    <div style="margin: 3px"></div>
                                    <el-row>
                                        <el-col :span="5">
                                            <div class="grid-content bg-purple" style="font-size: 14px;margin-right: 2px;font-family:Lucida Console">STA06</div>
                                        </el-col> 
                                        <el-col :span="19">
                                            <div class="grid-content bg-purple-light">
                                                <el-progress :text-inside="true" :stroke-width="16" :width="300" :percentage="Math.ceil((stations[5].total-stations[5].retestcount)/stations[5].total*100)" status="success"></el-progress>
                                            </div>
                                        </el-col>
                                    </el-row>               
                                </el-row>
                    </div>
                </div>
            </div>     
        </div>
    `,
    props: {
        station: {},
        isdefault: false,
        stations: []
    },
    data() {
        return {

        }
    },
    watch: {
        // 监听工站值station变化
        station: {
            handler(newVal, oldVal) {
                STATION_RESULT_OPTION.series[0].data[0].value = newVal.pass
                STATION_RESULT_OPTION.series[0].data[1].value = newVal.fail
                station_result_echart.setOption(STATION_RESULT_OPTION)

                this.s = newVal
            },
            deep: true
        },
    },
    methods: {

    }
}
// 工站测试结果统计图表option
var STATION_RESULT_OPTION = {
    title: {
        text: 'Test Result Rate',
        // subtext: 'Fake Data',
        left: 'center'
    },
    legend: {
        // orient: 'vertical',
        left: 'left'
    },
    series: [
        {
            // name: 'TEST RESULT',
            type: 'pie',
            type: 'pie',
            radius: '50%',
            data: [
                { value: 200, name: 'PASS' },
                { value: 17, name: 'FAIL' },
            ],
            emphasis: {
                itemStyle: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
}
