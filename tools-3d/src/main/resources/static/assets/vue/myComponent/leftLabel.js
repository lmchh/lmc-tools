const leftLabel = {
    template: `
        <div v-show="isdefault">
            <el-row>
                 <el-col :span="2">
                      <div class="el-notification left" style="top: 16px;z-index: 1000;padding-right: -5px;width: 290px">
                            <div class="el-notification-group" style="height: 98%;width: 300px">
                                 
                                 <div class="el-notification-content">
                                       <el-row><h2 style="color: red">C41-6FT-DK11</h2></el-row>
                                       <el-row>
                                            <el-col :span="12">
                                                 <div id="total-panel" style="width:95%;height: 120px"></div>                                       
                                            </el-col>
                                            <el-col :span="12">
                                                  <div id="total-retest" style="width:95%;height: 120px"></div>                                      
                                            </el-col>                                   
                                       </el-row>
                                       <el-row style="margin-left: 15px;margin-top: 25px">
                                            <div id="line-station01" style="width: 270px;;height: 240px"></div>
                                       </el-row>  
                                       <el-row style="margin-left: 5px">
                                            <div id="line-station02" style="width: 270px;height: 240px"></div>
                                       </el-row>                          
                                 </div>                       
                            </div>              
                      </div>           
                 </el-col> 
                 <el-col :span="22"></el-col>       
            </el-row>
            <!--  左邊欄信息面板(默認) START  -->
<!--            <div class="el-notification left" style="top: 16px;z-index: 1000;width: 300px;height: 650px">-->
<!--                <div class="el-notification-group" style="width: 200px">-->
<!--                    <h2>生產情況</h2>-->
<!--                    <div class="el-notification-content">-->
<!--                        <p>總產量： 108</p>-->
<!--                        <p>工站測試率</p>-->
<!--                        <el-row>-->
<!--                            <el-col :span="9">-->
<!--                                <div class="grid-content bg-purple" style="font-size: 8px;margin-right: 2px">SW-DOWNLOAD</div>-->
<!--                            </el-col>-->
<!--                            <el-col :span="15">-->
<!--                                <div class="grid-content bg-purple-light">-->
<!--                                    <el-progress :text-inside="true" :stroke-width="16" :width="300" :percentage="87" status="success"></el-progress>-->
<!--                                </div>-->
<!--                            </el-col>-->
<!--                        </el-row>-->
<!--                        <div style="margin: 3px"></div>-->
<!--                        <el-row>-->
<!--                            <el-col :span="9">-->
<!--                                <div class="grid-content bg-purple" style="font-size: 8px;margin-right: 2px">HBT</div>-->
<!--                            </el-col>-->
<!--                            <el-col :span="15">-->
<!--                                <div class="grid-content bg-purple-light">-->
<!--                                    <el-progress :text-inside="true" :stroke-width="16" :width="300" :percentage="95" status="success"></el-progress>-->
<!--                                </div>-->
<!--                            </el-col>-->
<!--                        </el-row>-->
<!--                        <div style="margin: 3px"></div>-->
<!--                        <el-row>-->
<!--                            <el-col :span="9">-->
<!--                                <div class="grid-content bg-purple" style="font-size: 8px;margin-right: 2px">STOM</div>-->
<!--                            </el-col> -->
<!--                            <el-col :span="15">-->
<!--                                <div class="grid-content bg-purple-light">-->
<!--                                    <el-progress :text-inside="true" :stroke-width="16" :width="300" :percentage="90" status="success"></el-progress>-->
<!--                                </div>-->
<!--                            </el-col>-->
<!--                        </el-row>-->
<!--                    </div>-->
<!--                </div>-->
<!--                    <div class="col-lg-12 col-sm-12 col-xs-12" style=" float:none;">-->
<!--                      <div class ="col-xs-2 col-sm-2 col-md-2" style=" float:none;padding-left: 0px;padding-Top:0px">-->
<!--                        -->
<!--                            <div id="panel1" style=" width:300px;height:650px; ;border-radius:5px;margin-left: -30px;padding-left:10px" >-->
<!--                            -->
<!--                                  <span><h2 style="margin-top: 0px;"> 生产情况</h2></span>  -->
<!--                                  -->
<!--                                  <span><h3 style=""> 总产量      108</h3></span>  -->
<!--                                  -->
<!--                                  <span><h4 style=""> 工站测试率</h4></span>  -->
<!--                                  -->
<!--                                  <div class="row" style="height: 30px">-->
<!--                                    <div class ="col-xs-12 col-sm-12 col-md-12"  style="height: 30px">-->
<!--                                        <div class ="col-xs-6 col-sm-6 col-md-6" >-->
<!--                                          <span><h5  >ST01</h5></span>  -->
<!--                                      </div>-->
<!--                                    -->
<!--                                      <div class ="col-xs-6 col-sm-6 col-md-6" >-->
<!--                                            -->
<!--                                    <div class="progress" style="margin-top: 15px;">-->
<!--                                        <div class="progress-bar" role="progressbar" aria-valuenow="60" -->
<!--                                            aria-valuemin="0" aria-valuemax="100" style="width: 40%;">-->
<!--                                            <span class="sr-only">40% 完成</span>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                            -->
<!--                                   </div>-->
<!--                                 </div>-->
<!--                                </div>-->
<!--                                           -->
<!--                                <div class="row"  style="height: 30px">-->
<!--                                    <div class ="col-xs-12 col-sm-12 col-md-12"  style="height: 30px">-->
<!--                                        <div class ="col-xs-6 col-sm-6 col-md-6" >-->
<!--                                          <span><h5  >STA01</h5></span>  -->
<!--                                      </div>-->
<!--                                    -->
<!--                                          <div class ="col-xs-6 col-sm-6 col-md-6" >-->
<!--                                            -->
<!--                                <div class="progress" style="margin-top: 15px;">-->
<!--                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" -->
<!--                                        aria-valuemin="0" aria-valuemax="100" style="width: 20%;">-->
<!--                                        <span class="sr-only">40% 完成</span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                            -->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                           </div>-->
<!--                                           -->
<!--                                            <div class="row" style="height: 30px">-->
<!--                                    <div class ="col-xs-12 col-sm-12 col-md-12"  style="height: 30px">-->
<!--                                        <div class ="col-xs-6 col-sm-6 col-md-6" >-->
<!--                                          <span><h5>STA02</h5></span>  -->
<!--                                      </div>-->
<!--                                    -->
<!--                                          <div class ="col-xs-6 col-sm-6 col-md-6" >-->
<!--                                            -->
<!--                                <div class="progress" style="margin-top: 15px;">-->
<!--                                    <div class="progress-bar" role="progressbar" aria-valuenow="60" -->
<!--                                        aria-valuemin="0" aria-valuemax="100" style="width: 80%;">-->
<!--                                        <span class="sr-only">40% 完成</span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                            -->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                           </div>-->
<!--                                           -->
<!--                                           <div class="row"  style="height: 30px;margin-top: 20px">-->
<!--                                         <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">-->
<!--                              <div class="up_downs">-->
<!--                                <div class="left_up_downs pull-left" style="margin-left: -35px">-->
<!--                                  <p>100%</p>-->
<!--                                  <p>80%</p>-->
<!--                                  <p>60%</p>-->
<!--                                  <p>40%</p>-->
<!--                                  <p>20%</p>-->
<!--                                  <p>0%</p>　-->
<!--                                </div>-->
<!--                                <div class="right_up_downs pull-left">-->
<!--                                  <div class="right_up_downs_top">-->
<!--                                    &lt;!&ndash;背景线&ndash;&gt;-->
<!--                                    <ul class="list-unstyled right_top_ul">-->
<!--                                      <li></li>-->
<!--                                      <li></li>-->
<!--                                      <li></li>-->
<!--                                      <li></li>-->
<!--                                      <li></li>-->
<!--                                    </ul>-->
<!--                                    &lt;!&ndash;柱形&ndash;&gt;-->
<!--                                    <div class="right_pillar">-->
<!--                                      <ul class="list-unstyled right_pillar_ul">-->
<!--                                        <li>-->
<!--                                          <div class="right_pillar_for">-->
<!--                                            <div class="right_pillar_fill right_pillar_color1" id="pillar_content1"-->
<!--                                               data-toggle="tooltip" data-placement="auto"-->
<!--                                               title="<p class='text-left'>SUCCESS：100score</p><p  class='text-left'>52%(52score)</p>">-->
<!--                                              <div class="right_pillar_bg_1" style="height: 50%"></div>-->
<!--                                            </div>-->
<!--                                          </div>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                          <div class="right_pillar_for">-->
<!--                                            <div class="right_pillar_fill right_pillar_color3" id="pillar_content3"-->
<!--                                               data-toggle="tooltip" data-placement="auto"-->
<!--                                               title="<p class='text-left'>SUCCESS：100score</p><p  class='text-left'>52%(52score)</p>">-->
<!--                                              <div class="right_pillar_bg_3" style="height: 50%"></div>-->
<!--                                            </div>-->
<!--                                            <div class="right_pillar_fill right_pillar_color4" id="pillar_content4"-->
<!--                                               data-toggle="tooltip" data-placement="auto"-->
<!--                                               title="<p class='text-left'>SUCCESS：120score</p><p  class='text-left'>46%(84score)</p>">-->
<!--                                              <div class="right_pillar_bg_4" style="height: 45%"></div>-->
<!--                                            </div>-->
<!--                                          </div>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                          <div class="right_pillar_for">-->
<!--                                            <div class="right_pillar_fill  right_pillar_color6" id="pillar_content6"-->
<!--                                               data-toggle="tooltip" data-placement="auto"-->
<!--                                               title="<p class='text-left'>SUCCESS：120score</p><p  class='text-left'>46%(84score)</p>">-->
<!--                                              <div class="right_pillar_bg_6" style="height: 45%"></div>-->
<!--                                            </div>-->
<!--                                          </div>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                          <div class="right_pillar_for">-->
<!--                                            <div class="right_pillar_fill right_pillar_color7" id="pillar_content7"-->
<!--                                               data-toggle="tooltip" data-placement="auto"-->
<!--                                               title="<p class='text-left'>SUCCESS：100score</p><p  class='text-left'>52%(52score)</p>">-->
<!--                                              <div class="right_pillar_bg_7" style="height: 50%"></div>-->
<!--                                            </div>-->
<!--                                            <div class="right_pillar_fill right_pillar_color8" id="pillar_content8"-->
<!--                                               data-toggle="tooltip" data-placement="auto"-->
<!--                                               title="<p class='text-left'>SUCCESS：120score</p><p  class='text-left'>46%(84score)</p>">-->
<!--                                              <div class="right_pillar_bg_8" style="height: 45%"></div>-->
<!--                                            </div>-->
<!--                                          </div>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                          <div class="right_pillar_for">-->
<!--                                            <div class="right_pillar_fill   right_pillar_color9" id="pillar_content9"-->
<!--                                               data-toggle="tooltip" data-placement="auto" title="E(需努力)">-->
<!--                                              <div class="right_pillar_bg_9"-->
<!--                                                 style="height: 50%;background:#F3BC8B !important;"></div>-->
<!--                                            </div>-->
<!--                                          </div>-->
<!--                                        </li>-->
<!--                                      </ul>-->
<!--                                    </div>-->
<!--                                  </div>-->
<!--                                  <div class="right_up_downs_bottom clearfix">-->
<!--                                    <div class="pull-left text-center">STA01</div>-->
<!--                                    <div class="pull-left text-center">STA02</div>-->
<!--                                    <div class="pull-left text-center">STA03</div>-->
<!--                                    <div class="pull-left text-center">STA04</div>-->
<!--                                    <div class="pull-left text-center">STA05</div>-->
<!--                                  </div>-->
<!--                                </div>-->
<!--                              </div>-->
<!--                            </div>-->
<!--                            -->
<!--                                          </div>-->
<!--                            </div>-->
<!--                       </div>-->
<!--                   </div>-->
<!--                </div>-->
            </div>
<!--            &lt;!&ndash;  左邊欄信息面板 END  &ndash;&gt;-->
            
    `,
    props: {
        isdefault: true,
        stations: []
    },
    data() {
        return {

        }
    },
    watch: {
        stations: {
            handler(newVal, oldVal) {
                let temp_total = 0, temp_pass = 0, temp_retest = 0
                let option1 = LINE_STATION01_OPTION
                option1.xAxis[0].data = []
                option1.series[0].data = []
                option1.series[1].data = []
                option1.series[2].data = []
                option1.series[3].data = []
                for (let i = 0; i < newVal.length/2; i++) {
                    option1.xAxis[0].data.push(newVal[i].name)
                    option1.series[0].data.push(newVal[i].total)
                    option1.series[1].data.push(newVal[i].pass)
                    option1.series[2].data.push(newVal[i].fail)
                    option1.series[3].data.push(newVal[i].retestcount)

                    temp_total += newVal[i].total
                    temp_pass += newVal[i].pass
                    temp_retest += newVal[i].retestcount
                }
                line_station01_option.legend = {}
                line_station01_echart.setOption(option1)

                let option2 = LINE_STATION02_OPTION
                option2.xAxis[0].data = []
                option2.series[0].data = []
                option2.series[1].data = []
                option2.series[2].data = []
                option2.series[3].data = []
                for (let i = newVal.length/2; i < newVal.length; i++) {
                    option2.xAxis[0].data.push(newVal[i].name)
                    option2.series[0].data.push(newVal[i].total)
                    option2.series[1].data.push(newVal[i].pass)
                    option2.series[2].data.push(newVal[i].fail)
                    option2.series[3].data.push(newVal[i].retestcount)

                    temp_total += newVal[i].total
                    temp_pass += newVal[i].pass
                    temp_retest += newVal[i].retestcount
                }
                line_station02_echart.setOption(option2)

                TOTAL_PANEL_OPTION.series[0].data[0].value = Math.ceil(temp_pass/temp_total*100)
                total_panel_echart.setOption(TOTAL_PANEL_OPTION)

                TOTAL_RETEST_OPTION.series[0].data[0].value = Math.ceil(temp_retest/temp_total*100)
                total_retest_echart.setOption(TOTAL_RETEST_OPTION)

            },
            deep: true
        }
    }
}

var TOTAL_PANEL_OPTION = {
    title:{
        text: '及格率',
        x: 'center',
        y: 'bottom',
        textStyle: {
            fontSize: 10,
            color: '#7E2220'
        }
    },
    series: [
        {
            type: 'gauge',
            progress: {
                show: true,
                width: 8
            },
            axisLine: {
                lineStyle: {
                    width: 8
                }
            },
            axisTick: {
                show: false
            },
            splitLine: {
                length: 5,
                lineStyle: {
                    width: 2,
                    color: '#999'
                }
            },
            axisLabel: {
                distance: 5,
                color: '#999',
                fontSize: 8
            },
            anchor: {
                show: true,
                showAbove: true,
                size: 15,
                itemStyle: {
                    borderWidth: 10
                }
            },
            title: {
                show: false
            },
            detail: {
                valueAnimation: true,
                fontSize: 20,
                offsetCenter: [0, '70%'],
                formatter: '{value}%'
            },
            data: [
                {
                    value: 60
                }
            ]
        }
    ]
}

var TOTAL_RETEST_OPTION =  {
    title:{
        text: '重测率',
        x: 'center',
        y: 'bottom',
        textStyle: {
            fontSize: 10,
            color: '#7E2220',
            padding: [10, 0, 0, 10]
        }
    },
    series: [
        {
            type: 'gauge',
            startAngle: 180,
            endAngle: 0,
            min: 0,
            max: 100,
            splitNumber: 5,
            itemStyle: {
                color: '#58D9F9',
                shadowColor: 'rgba(0,138,255,0.45)',
                shadowBlur: 4,
                shadowOffsetX: 2,
                shadowOffsetY: 2
            },
            progress: {
                show: true,
                roundCap: true,
                width: 3
            },
            pointer: {
                icon: 'path://M2090.36389,615.30999 L2090.36389,615.30999 C2091.48372,615.30999 2092.40383,616.194028 2092.44859,617.312956 L2096.90698,728.755929 C2097.05155,732.369577 2094.2393,735.416212 2090.62566,735.56078 C2090.53845,735.564269 2090.45117,735.566014 2090.36389,735.566014 L2090.36389,735.566014 C2086.74736,735.566014 2083.81557,732.63423 2083.81557,729.017692 C2083.81557,728.930412 2083.81732,728.84314 2083.82081,728.755929 L2088.2792,617.312956 C2088.32396,616.194028 2089.24407,615.30999 2090.36389,615.30999 Z',
                length: '75%',
                width: 3,
                offsetCenter: [0, '5%']
            },
            axisLine: {
                roundCap: true,
                lineStyle: {
                    width: 3
                }
            },
            axisTick: {
                splitNumber: 2,
                lineStyle: {
                    width: 2,
                    color: '#999'
                }
            },
            splitLine: {
                length: 2,
                lineStyle: {
                    width: 3,
                    color: '#999'
                }
            },
            axisLabel: {
                distance: 8,
                color: '#999',
                fontSize: 8
            },
            title: {
                show: false
            },
            detail: {
                backgroundColor: '#fff',
                borderColor: '#999',
                borderWidth: 0,
                width: '50%',
                lineHeight: 41,
                height: 14,
                borderRadius: 8,
                offsetCenter: [0, '35%'],
                valueAnimation: true,
                formatter: function (value) {
                    return '{value|' + value.toFixed(0) + '}{unit|%}';
                },
                rich: {
                    value: {
                        fontSize: 20,
                        fontWeight: 'bolder',
                        color: '#777'
                    },
                    unit: {
                        fontSize: 14,
                        color: '#999',
                        padding: [0, 0, -2, 1]
                    }
                }
            },
            data: [
                {
                    value: 80
                }
            ]
        }
    ]
}

var app = {}
const posList = [
    'left',
    'right',
    'top',
    'bottom',
    'inside',
    'insideTop',
    'insideLeft',
    'insideRight',
    'insideBottom',
    'insideTopLeft',
    'insideTopRight',
    'insideBottomLeft',
    'insideBottomRight'
]
app.configParameters = {
    rotate: {
        min: -90,
        max: 90
    },
    align: {
        options: {
            left: 'left',
            center: 'center',
            right: 'right'
        }
    },
    verticalAlign: {
        options: {
            top: 'top',
            middle: 'middle',
            bottom: 'bottom'
        }
    },
    position: {
        options: posList.reduce(function (map, pos) {
            map[pos] = pos;
            return map;
        }, {})
    },
    distance: {
        min: 0,
        max: 100
    }
}
app.config = {
    rotate: 90,
    align: 'left',
    verticalAlign: 'middle',
    position: 'insideBottom',
    distance: 15,
    onChange: function () {
        const labelOption = {
            rotate: app.config.rotate,
            align: app.config.align,
            verticalAlign: app.config.verticalAlign,
            position: app.config.position,
            distance: app.config.distance
        };
        myChart.setOption({
            series: [
                {
                    label: labelOption
                },
                {
                    label: labelOption
                },
                {
                    label: labelOption
                },
                {
                    label: labelOption
                }
            ]
        });
    }
}
const labelOption = {
    show: true,
    position: app.config.position,
    distance: app.config.distance,
    align: app.config.align,
    verticalAlign: app.config.verticalAlign,
    rotate: app.config.rotate,
    formatter: '{c}  {name|{a}}',
    fontSize: 16,
    rich: {
        name: {}
    }
}

var LINE_STATION01_OPTION = {
    title: {
        text: 'All Station Data',
        left: 'center'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    xAxis: [
        {
            type: 'category',
            // name: 'Station',
            axisTick: { show: false },
            data: [],
            axisLabel: {
                rotate:45
            }
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Num',
        }
    ],
    series: [
        {
            name: 'Total',
            type: 'bar',
            barGap: 0,
            label: labelOption,
            emphasis: {
                focus: 'series'
            },
            data: []
        },
        {
            name: 'Pass',
            type: 'bar',
            label: labelOption,
            emphasis: {
                focus: 'series'
            },
            data: []
        },
        {
            name: 'Fail',
            type: 'bar',
            label: labelOption,
            emphasis: {
                focus: 'series'
            },
            data: []
        },
        {
            name: 'Retest',
            type: 'bar',
            label: labelOption,
            emphasis: {
                focus: 'series'
            },
            data: []
        }
    ]
}
var LINE_STATION02_OPTION = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    legend: {
        data: ['Total', 'Pass', 'Fail', 'Retest']
    },
    xAxis: [
        {
            type: 'category',
            // name: 'Station',
            axisTick: { show: false },
            data: [],
            axisLabel: {
                rotate:45
            }
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: 'Num',
        }
    ],
    series: [
        {
            name: 'Total',
            type: 'bar',
            barGap: 0,
            label: labelOption,
            emphasis: {
                focus: 'series'
            },
            data: []
        },
        {
            name: 'Pass',
            type: 'bar',
            label: labelOption,
            emphasis: {
                focus: 'series'
            },
            data: []
        },
        {
            name: 'Fail',
            type: 'bar',
            label: labelOption,
            emphasis: {
                focus: 'series'
            },
            data: []
        },
        {
            name: 'Retest',
            type: 'bar',
            label: labelOption,
            emphasis: {
                focus: 'series'
            },
            data: []
        }
    ]
}