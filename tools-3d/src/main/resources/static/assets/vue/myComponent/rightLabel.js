const rightLabel = {
    template: `
        <div>
            <el-row>
                <el-col :span="20"></el-col>
                <el-col :span="4">
                     <div class="el-notification right" style="top: 16px;;z-index: 1000;width: 290px">
                        <div class="el-notification-group" style=";height: 98%;width: 280px!important;">
                            <div class="el-notification-content">
                                <el-row style="margin-top: 10px">
                                      <div id="charge" style="width:95%;height:240px"></div>              
                                </el-row>
                                <el-row style="margin-top: -30px">
                                      <span style="font-weight: bolder">
<!--                                        <el-tag style="font-size: 14px;margin-top:3px;margin-bottom:3px" type="danger">环境湿度: 65%</el-tag>-->
                                        <el-tag style="font-size: 14px;margin-top:3px;margin-bottom:3px" type="success">导电材料: 钛合金</el-tag>
                                        <el-tag style="font-size: 14px;margin-top:3px;margin-bottom:3px">材料长度: 20cm</el-tag>
                                        <el-tag style="font-size: 13px;margin-top:3px;margin-bottom:3px" type="danger">材料宽度: 14cm</el-tag>
                                        <el-tag style="font-size: 13px;margin-top:3px;margin-bottom:3px" type="warning">材料厚度: 0.37cm</el-tag>
                                    </span>                           
                                </el-row>
                                <el-row>
                                      <div id="humidity" style="width: 260px;height: 232px"></div>                          
                                </el-row>
                                <!-- 上面是图标并使用div包裹显示在一行，下面是滚屏内容 -->
                               <h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FAILITEM</h4>
                              <div  id="scroll-message" class="scroll-message-style" style="overflow:hidden;height:120px; width: 40%;padding-right:5%;float: left;font-size: 10px;border: 1px solid grey;border-radius: 5%;margin-right: 10px;margin-left: 20px;background-color: white">
                                  <ul style="list-style:none;margin-left:-25px">
                                    <li v-for="value in sn"><el-tag>{{value}}</el-tag></li>
                                  </ul>
                              </div>
                              <div  id="scroll-message1" class="scroll-message-style" style="overflow:hidden;height:120px;width: 40%;padding-right:5%;font-size: 10px;border: 1px solid grey;border-radius: 5%;margin-left: 10px">
                                  <ul style="list-style:none;margin-left:-25px">
                                    <li v-for="value in failitem"><el-tag type="success">{{value}}</el-tag></li>
                                  </ul>
                              </div>
                            </div>
                        </div>     
                    </div>     
                </el-col>        
            </el-row>
           
        </div>
       
    `,
    props: {
        charge: {},
        sn: [],
        failitem: [],
        stations: []
    },
    data() {
        return {}
    },
    watch: {
        charge: {
            handler(newVal, oldVal) {
                let option = charge_echart.getOption()
                option.xAxis[0].data = newVal.time
                option.yAxis[0].data = newVal.num
                option.yAxis[0].min = newVal.num[0]
                option.series[0].data = newVal.num
                charge_echart.setOption(option)

                let humity_option = humity_echart.getOption()
                humity_option.xAxis[0].data = newVal.time
                humity_option.yAxis[0].data = newVal.humity
                humity_option.series[0].data = newVal.humity
                humity_echart.setOption(humity_option)

            },
            deep: true
        }
    }
}

var CHARGE_OPTION = {
    title: {
        text: 'Charge Change',
        // subtext: 'Fake Data',
        left: 'center',
        // textStyle: {
        //     color: '#ffffff'
        // }
    },
    xAxis: {
        type: 'category',
        // name: '时间',
        data: [],
        // axisLabel:{
        //     show:true,
        //     textStyle:{
        //         color:"#ffffff"
        //     }
        // },
    },
    yAxis: {
        type: 'value',
        name: '电荷量',
        splitNumber: 5,
        nameTextStyle:{
            fontSize: 10
        },
        // axisLabel:{
        //     show:true,
        //     textStyle:{
        //         color:"#ffffff"
        //     }
        // },

    },
    series: [
        {
            data: [],
            type: 'line'
        }
    ]
}

var HUMIDITY_OPTION = {
    color: ['#00868B'],
    title: {
        text: 'Env Humidity',
        left: 'center'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    // legend: {
    //     data: ['湿度'],
    //     left: 'left'
    // },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: [
        {
            type: 'category',
            boundaryGap: false,
            name: '时间',
            data: []
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: '湿度%',
            max: 100,
            min: 0
        }
    ],
    series: [
        {
            name: '湿度',
            type: 'line',
            stack: 'Total',
            smooth: true,
            lineStyle: {
                width: 0
            },
            showSymbol: false,
            areaStyle: {},
            // areaStyle: {
            //     opacity: 0.8,
            //     color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
            //         {
            //             offset: 0,
            //             color: 'rgba(128, 255, 165)'
            //         },
            //         {
            //             offset: 1,
            //             color: 'rgba(1, 191, 236)'
            //         }
            //     ])
            // },
            emphasis: {
                focus: 'series'
            },
            data: []
        },

    ]
}




setTimeout(function(){
    var table = document.getElementById("scroll-message");
    var timer = null;
    table.scrollTop = 0;
    table.innerHTML += table.innerHTML;
    function play() {
        clearInterval(timer);
        timer = setInterval(function() {
            table.scrollTop +=2;
            if (table.scrollTop >= 280) {
                table.scrollTop = 0;
            }
        }, 50);
    }
    setTimeout(play, 500);
    table.onmouseover = function() {
        clearInterval(timer)
    };
    table.onmouseout = play;
},3000)

setTimeout(function(){
    var table = document.getElementById("scroll-message1");
    var timer = null;
    table.scrollTop = 0;
    table.innerHTML += table.innerHTML;
    function play() {
        clearInterval(timer);
        timer = setInterval(function() {
            table.scrollTop +=2;
            if (table.scrollTop >= 280) {
                table.scrollTop = 0;
            }
        }, 50);
    }
    setTimeout(play, 500);
    table.onmouseover = function() {
        clearInterval(timer)
    };
    table.onmouseout = play;
},3000)
