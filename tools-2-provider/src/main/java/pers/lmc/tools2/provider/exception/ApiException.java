package pers.lmc.tools2.provider.exception;

import com.lmc.common.enums.ResultCodeEnum;

/**
 * @author lmc
 * @Description: TODO API异常类
 * @Create 2022-06-26 18:48
 * @version: 1.0
 */
public class ApiException extends RuntimeException{

    private int code;

    private String msg;

    public ApiException(String msg) {
        super(msg);
        this.code = ResultCodeEnum.FAILURE.getCode();
        this.msg = ResultCodeEnum.FAILURE.getMsg();
    }

    public ApiException(ResultCodeEnum enums, String msg) {
        super(msg);
        this.code = enums.getCode();
        this.msg = enums.getMsg();
    }
}
