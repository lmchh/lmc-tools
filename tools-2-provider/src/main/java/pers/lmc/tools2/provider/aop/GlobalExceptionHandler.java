package pers.lmc.tools2.provider.aop;

import com.lmc.common.enums.ResultCodeEnum;
import com.lmc.common.vo.ResultVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pers.lmc.tools2.provider.exception.ApiException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-06-26 18:31
 * @version: 1.0
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 处理所有校验失败的异常（MethodArgumentNotValidException异常）
     * @param e
     * @return
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResultVo handleBindGetException(MethodArgumentNotValidException e) {
        // 获取所有异常参数
        List<String> errors = e.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());
        return ResultVo.result(ResultCodeEnum.VALIDATE_PARAMS_ERROR, null).withDesc("参数校验失败:" + errors);
    }

    /**
     * 处理自定义APIException异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = ApiException.class)
    public ResultVo handleApiException(ApiException e) {
        return ResultVo.fail(null).withDesc(e.getMessage());
    }

    /**
     * 处理其他异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    public ResultVo handleException(Exception e) {
        log.info("执行到统一处理方法...");
        return ResultVo.fail(null).withDesc(e.getMessage());
    }
}
