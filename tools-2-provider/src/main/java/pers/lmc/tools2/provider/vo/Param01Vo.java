package pers.lmc.tools2.provider.vo;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-06-26 13:43
 * @version: 1.0
 */
@Data
public class Param01Vo {

    @NotNull(message = "名称不能为空")
    @Size(min = 1, max = 50, message = "名称name长度必须是1-50个字符")
    private String name;

    @NotNull(message = "年龄age不能为空")
    @Min(value = 10, message = "年龄age不能低于10岁")
    @Max(value = 25, message = "年龄age不能超过25岁")
    private Integer age;

    @Min(value = 0, message = "性别sex只能是0和1,0=女1=男")
    @Max(value = 1, message = "性别sex只能是0和1,0=女1=男")
    private Short sex;

}
