package pers.lmc.tools2.provider.controller;

import com.lmc.common.vo.ResultVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.lmc.tools2.provider.vo.Param01Vo;

import javax.validation.Valid;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-06-26 17:10
 * @version: 1.0
 */
@RestController
@Validated
@RequestMapping("/valicate")
@Slf4j
public class ValicateController {

    @PostMapping("/add")
    public ResultVo addParam01(@Valid @RequestBody Param01Vo param01Vo) {
        log.info("执行add()方法，参数：" + param01Vo.toString());
        int k = 1/0;
        return ResultVo.success(param01Vo);
    }

}
