package pers.lmc.tools2.provider.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-04-11 23:34
 * @version: 1.0
 */
@RestController
//@RefreshScope
@RequestMapping("/test")
public class TestController {

    @Value("${tools2.provider.value01:xxx}")
    private String value01;

    @RequestMapping("/sample01")
    @SentinelResource(value = "tools2-provider/test/sample01", blockHandler = "blockHandler_sample01", fallback = "fallback_sample01")
    public String sample01() {
        int i = 10/0;
        return "sample011";
    }

    @RequestMapping("/value01")
    public String value01() {
        return value01 + "1";
    }

    //限流策略
    public String blockHandler_sample01(BlockException exception) {
        return "接口tools2-provider/test/sample01熔断：" + exception.getMessage();
//        return "接口tools2-provider/test/sample01熔断：";
    }

    //熔断策略
    public String fallback_sample01(Throwable throwable) {
        return "接口tools2-provider/test/sample01熔断：" + throwable.getMessage();
    }
}
