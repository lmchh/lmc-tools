package com.lmc.task.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-10-13 23:18
 * @version: 1.0
 */
@RestController
@RequestMapping("test")
@Slf4j
public class TestController {


    @RequestMapping("api")
    public String api() {
        log.info("调用了TestController的api接口");
        return "api01";
    }

}
