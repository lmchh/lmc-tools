package com.lmc.task.controller;

import com.lmc.task.common.ResultMessage;
import com.lmc.task.dto.CronScheduleDTO;
import com.lmc.task.dto.SimpleScheduleDTO;
import com.lmc.task.entity.Schedule;
import com.lmc.task.service.ScheduleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-21 21:48
 * @version: 1.0
 */
@Api
@RestController
@RequestMapping("api/task/schedule")
public class ScheduleController {

    @Autowired
    ScheduleService scheduleService;

    private static Logger log = LoggerFactory.getLogger(ScheduleController.class);


    @ApiOperation("getAll")
    @GetMapping(value = "/getAll")
    public ResultMessage getAll() {
        List<Schedule> schedules = scheduleService.findAll();
        if (schedules == null || schedules.size() == 0) {
            return new ResultMessage(404, "查询结果为空", null);
        }
        return new ResultMessage(200, "查询任务成功", schedules);
    }

    @PostMapping(value = "/{jobName}/add")
    public ResultMessage schedule(@PathVariable("jobName") String jobName, @RequestBody SimpleScheduleDTO simpleScheduleDTO) {
        log.info(simpleScheduleDTO.toString());
        Schedule schedule = scheduleService.setSchedule(jobName, simpleScheduleDTO);
        if (schedule == null && schedule.getId() != null) {
            return new ResultMessage(500, "创建任务失败", null);
        }
        return new ResultMessage(200, "创建任务成功", schedule);
    }

    @PostMapping(value = "/{jobName}/cronadd")
    public ResultMessage CronSchedule(@PathVariable("jobName") String jobName, @RequestBody CronScheduleDTO cronScheduleDTO) {
        log.info(cronScheduleDTO.toString());
        Schedule schedule = scheduleService.setSchedule(jobName, cronScheduleDTO);
        if (schedule != null && schedule.getId() != null) {
            return new ResultMessage(200, "创建任务成功", schedule);
        }
        return new ResultMessage(200, "创建任务成功", schedule);
    }

    @PostMapping(value = "/{jobName}/update")
    public ResultMessage modifySchedule(@PathVariable("jobName") String jobName, @RequestBody SimpleScheduleDTO simpleScheduleDTO) {
        Schedule schedule = scheduleService.modifySchedule(jobName, simpleScheduleDTO);
        if (schedule == null) {
            return new ResultMessage(500, "没有找到符合的任务", null);
        }
        return new ResultMessage(200, "修改任务成功", schedule);
    }

    @PostMapping(value = "/{jobName}/cronupdate")
    public ResultMessage modifyCronSchedule(@PathVariable("jobName") String jobName, @RequestBody CronScheduleDTO cronScheduleDTO) {
        Schedule schedule = scheduleService.modifySchedule(jobName, cronScheduleDTO);
        if (schedule == null) {
            return new ResultMessage(500, "没有找到符合的任务", null);
        }
        return new ResultMessage(200, "修改任务成功", schedule);
    }

    @PostMapping(value = "/{jobName}/del")
    public ResultMessage removeSchedule(@PathVariable("jobName") String jobName) {
        String result = scheduleService.removerSchedule(jobName);
        return new ResultMessage(200, "删除任务成功", result);
    }

    @PostMapping(value = "/{jobName}/pause")
    public ResultMessage pauseSchedule(@PathVariable("jobName") String jobName) {
        Schedule schedule = scheduleService.pauseSchedule(jobName);
        if (schedule == null) {
            return new ResultMessage(500, "没有找到符合的任务", null);
        }
        return new ResultMessage(200, "暂停任务成功", schedule);
    }

    @PostMapping(value = "/{jobName}/resume")
    public ResultMessage resumeSchedule(@PathVariable("jobName") String jobName) {
        Schedule schedule = scheduleService.resumeSchedule(jobName);
        if (schedule == null) {
            return new ResultMessage(500, "没有找到符合的任务", null);
        }
        return new ResultMessage(200, "恢复任务成功", schedule);
    }

    @GetMapping(value = "/{jobName}")
    public ResultMessage scheduleInfo(@PathVariable("jobName") String jobName) {
        Schedule schedule = scheduleService.getSchedule(jobName);
        if (schedule == null) {
            return new ResultMessage(500, "没有找到符合的任务", null);
        }
        return new ResultMessage(200, "获取任务成功", schedule);
    }

}
