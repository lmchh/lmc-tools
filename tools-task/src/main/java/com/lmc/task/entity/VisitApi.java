package com.lmc.task.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-09-02 22:05
 * @version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VisitApi {

    private String apiName;

    private String apiUrl;

    private String params;

    private String description;

}
