package com.lmc.task.entity;

import com.lmc.task.common.BaseEnum;

/**
 * @author lmc
 * @Description: TODO 定时任务状态枚举类，用来映射定时任务的状态
 * @Create 2021-06-20 19:18
 * @version: 1.0
 */
public enum ScheduleStatusEnum implements BaseEnum<ScheduleStatusEnum, Integer> {

    INACTIVATED(0, "未激活"),
    ACTIVATED(1, "已激活");
    private int state;
    private String stateInfo;

    ScheduleStatusEnum(int state, String stateInfo) {
        this.state = state;
        this.stateInfo = stateInfo;
    }

    public int getState() {
        return state;
    }

    public String getStateInfo() {
        return stateInfo;
    }

    @Override
    public Integer getValue() {
        return getState();
    }
}
