package com.lmc.task.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-20 19:20
 * @version: 1.0
 */
@Data
public class Schedule implements Serializable {

    private String id;
    private String triggerInfo;
    private ScheduleStatusEnum status;
    private String groupName;
    private String jobName;

    private int record;//运行记录
    private Date createdTimestamp;
    private Date updatedTimestamp;

}
