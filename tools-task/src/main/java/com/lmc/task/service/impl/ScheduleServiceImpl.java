package com.lmc.task.service.impl;

import com.lmc.task.dto.CronScheduleDTO;
import com.lmc.task.dto.SimpleScheduleDTO;
import com.lmc.task.entity.Schedule;
import com.lmc.task.job.MyJob;
import com.lmc.task.manager.ScheduleManager;
import com.lmc.task.service.ScheduleService;
import org.quartz.JobDataMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-21 21:19
 * @version: 1.0
 */
@Service("scheduleService")
public class ScheduleServiceImpl implements ScheduleService {

    private final static String GROUP = "TEST_GROUP";
    @Autowired
    ScheduleManager scheduleManager;


    @Override
    public List<Schedule> findAll() {
        return scheduleManager.getAll();
    }

    @Override
    public Schedule setSchedule(String joName, SimpleScheduleDTO ssd) {
        ssd.setJobName(joName);
        ssd.setGroup(GROUP);
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("type", "simple");
        return scheduleManager.createSimpleJob(MyJob.class, ssd, jobDataMap);
    }

    @Override
    public Schedule setSchedule(String jobName, CronScheduleDTO csd) {
        csd.setJobName(jobName);
        csd.setGroup(GROUP);
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("type", "cron");
        return scheduleManager.createCronJob(MyJob.class, csd, jobDataMap);
    }

    @Override
    public Schedule modifySchedule(String jobName, SimpleScheduleDTO ssd) {
        Schedule schedule = scheduleManager.getJobByNameAndGroup(jobName, GROUP);
        ssd.setJobName(jobName);
        ssd.setGroup(GROUP);
        return scheduleManager.updateSimpleJob(schedule, ssd);
    }

    @Override
    public Schedule modifySchedule(String jobbName, CronScheduleDTO csd) {
        Schedule schedule = scheduleManager.getJobByNameAndGroup(jobbName, GROUP);
        csd.setJobName(jobbName);
        csd.setGroup(GROUP);
        return scheduleManager.updateCronJob(MyJob.class, schedule, csd);
    }

    @Override
    public Schedule getSchedule(String jobName) {
        Schedule schedule = scheduleManager.getJobByNameAndGroup(jobName, GROUP);
        return scheduleManager.getSchedule(schedule);
    }

    @Override
    public Schedule pauseSchedule(String jobName) {
        Schedule schedule = scheduleManager.getJobByNameAndGroup(jobName, GROUP);
        return scheduleManager.pauseJob(schedule);
    }

    @Override
    public Schedule resumeSchedule(String jobName) {
        Schedule schedule = scheduleManager.getJobByNameAndGroup(jobName, GROUP);
        return scheduleManager.resumeJob(schedule);
    }

    @Override
    public String removerSchedule(String jobName) {
        Schedule schedule = scheduleManager.getJobByNameAndGroup(jobName, GROUP);
        if (schedule == null) {
            return "Not find schedule";
        }
        scheduleManager.deleteJob(schedule);
        return "Delete schedule job succeed.";
    }
}
