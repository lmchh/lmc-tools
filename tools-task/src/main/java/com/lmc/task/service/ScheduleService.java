package com.lmc.task.service;

import com.lmc.task.dto.CronScheduleDTO;
import com.lmc.task.dto.SimpleScheduleDTO;
import com.lmc.task.entity.Schedule;

import java.util.List;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-21 21:13
 * @version: 1.0
 */
public interface ScheduleService {

    List<Schedule> findAll();

    /**
     * 设置编排定时任务
     * @param ssd 定时参数
     * @return schedule
     */
    Schedule setSchedule(String jobName, SimpleScheduleDTO ssd);

    Schedule setSchedule(String jobName, CronScheduleDTO ssd);

    /**
     * 更新编排定时任务
     * @param ssd 参数
     * @return schedule
     */
    Schedule modifySchedule(String jobbName, SimpleScheduleDTO ssd);

    Schedule modifySchedule(String jobbName, CronScheduleDTO ssd);

    /**
     * 获取编排定时信息
     * @param jobName jobName
     * @return schedule
     */
    Schedule getSchedule(String jobName);
    /**
     * 暂停编排定时任务
     * @param jobName jobName
     * @return schedule
     */
    Schedule pauseSchedule(String jobName);

    /**
     * 恢复编排定时任务
     * @param jobName jobName
     * @return schedule
     */
    Schedule resumeSchedule(String jobName);

    /**
     * 删除编排定时任务
     * @param jobName jobName
     * @return str
     */
    String removerSchedule(String jobName);

}
