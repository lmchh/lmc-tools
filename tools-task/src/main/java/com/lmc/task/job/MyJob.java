package com.lmc.task.job;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-20 18:17
 * @version: 1.0
 */
public class MyJob implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        //获取上下文数据
        JobDataMap dataMap = jobExecutionContext.getMergedJobDataMap();
        if (dataMap.get("data") != null) {
            System.out.println(dataMap.get("data"));
        }
        System.out.println("===============================MyJob 任務正在進行：" + dataMap.get("type") + "==============================");
    }
}