package com.lmc.task.manager;

import com.alibaba.fastjson.JSON;
import com.lmc.task.dto.CronScheduleDTO;
import com.lmc.task.dto.Period;
import com.lmc.task.dto.ScheduleDTO;
import com.lmc.task.dto.SimpleScheduleDTO;
import com.lmc.task.entity.Schedule;
import com.lmc.task.entity.ScheduleStatusEnum;
import com.lmc.task.repository.ScheduleRepository;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author lmc
 * @Description: TODO 定时任务管理器
 * @Create 2021-06-20 21:05
 * @version: 1.0
 */
@Component("scheduleManager")
public class ScheduleManager {
    @Autowired
    Scheduler scheduler;
    @Autowired
    private ScheduleRepository scheduleRepository;

    private static Logger log = LoggerFactory.getLogger(ScheduleManager.class);

    public List<Schedule> getAll() {
        return scheduleRepository.findAll();
    }

    public Schedule getJobByNameAndGroup(String jobName, String groupName) {
        return scheduleRepository.findScheduleByJobNameAndGroupName(jobName, groupName);
    }

    /**
     * 删除 job
     * @param scheduleId id
     */
    public void deleteJob(String scheduleId) {
        Schedule schedule = getSchedule(scheduleId);
        deleteJob(schedule);
    }

    public void deleteJob(Schedule schedule) {
        JobKey jobKey = new JobKey(schedule.getJobName(), schedule.getGroupName());
        try {
            scheduler.deleteJob(jobKey);
            scheduleRepository.delete(schedule);
        } catch (SchedulerException e) {
            log.error("Delete schedule job error:{}", e.getMessage());
        }
    }

    /**
     * 恢复某个job
     * @param scheduleId id
     */
    public Schedule resumeJob(String scheduleId) {
        Schedule schedule = getSchedule(scheduleId);
        return resumeJob(schedule);
    }

    public Schedule resumeJob(Schedule schedule) {
        JobKey jobKey = new JobKey(schedule.getJobName(), schedule.getGroupName());
        try {
            scheduler.resumeJob(jobKey);
            schedule.setStatus(ScheduleStatusEnum.ACTIVATED);
            scheduleRepository.update(schedule);
        } catch (SchedulerException e) {
            log.error("Resume schedule job error:{}", e.getMessage());
        }
        return schedule;
    }

    /**
     * 暂停某个job
     * @param scheduleId id
     */
    public Schedule pauseJob(String scheduleId) {
        Schedule schedule = getSchedule(scheduleId);
        return pauseJob(schedule);
    }
    public Schedule pauseJob(Schedule schedule) {
        JobKey jobKey = new JobKey(schedule.getJobName(), schedule.getGroupName());
        try {
            scheduler.pauseJob(jobKey);
            schedule.setStatus(ScheduleStatusEnum.INACTIVATED);
            scheduleRepository.update(schedule);
        } catch (SchedulerException e) {
            log.error("Pause schedule job error:{}", e.getMessage());
        }
        return schedule;
    }

    /**
     * 更新simple job
     * @param scheduleId scheduleId
     * @param ssd        ssv
     * @return Schedule
     */
    public Schedule updateSimpleJob(String scheduleId, SimpleScheduleDTO ssd) {
        Schedule schedule = getSchedule(scheduleId);
        return updateSimpleJob(schedule, ssd);
    }
    public Schedule updateSimpleJob(Schedule schedule, SimpleScheduleDTO ssd) {
        try {
            String jobName = schedule.getJobName();
            String groupName = schedule.getGroupName();
            JobKey jobKey = new JobKey(jobName, groupName);
            JobDetail jobDetail = scheduler.getJobDetail(jobKey);
            //先删除
            scheduler.deleteJob(jobKey);
            //重新创建
            Trigger trigger = getSimpleTrigger(ssd);
            scheduler.scheduleJob(jobDetail, trigger);
            //更新元数据
            schedule.setRecord(0);
            schedule.setTriggerInfo(JSON.toJSONString(ssd));
            scheduleRepository.update(schedule);
        } catch (SchedulerException e) {
            log.error("Update simple schedule job error:{}", e.getMessage());
        }
        return schedule;
    }

    /**
     * 更新Cron job
     * @param scheduleId
     * @param csd
     * @return
     */
    public Schedule updateCronJob(Class<? extends Job> jobClass, String scheduleId, CronScheduleDTO csd) {
        Schedule schedule = scheduleRepository.findScheduleById(scheduleId);
        return updateCronJob(jobClass, schedule, csd);
    }
    public Schedule updateCronJob(Class<? extends Job> jobClass, Schedule schedule, CronScheduleDTO csd) {
        try {
            JobKey jobKey = new JobKey(schedule.getJobName(), schedule.getGroupName());

            JobDetail jobDetail = scheduler.getJobDetail(jobKey);
            JobDataMap jobDataMap = jobDetail.getJobDataMap();
            if (jobDataMap == null) {
                jobDataMap = new JobDataMap();
            }
            jobDataMap.put("data", JSON.toJSONString(csd.getData()));
            jobDetail = JobBuilder.newJob(jobClass).withIdentity(schedule.getJobName(), schedule.getGroupName()).usingJobData(jobDataMap).build();
            //先删除
            scheduler.deleteJob(jobKey);
            //重新创建
            Trigger trigger = getCronTrigger(csd);
            scheduler.scheduleJob(jobDetail, trigger);
            // 更新元数据
            schedule.setRecord(0);
            schedule.setTriggerInfo(JSON.toJSONString(csd));
            scheduleRepository.update(schedule);
        } catch (SchedulerException e) {
            log.error("Update cron schedule job error:{}", e.getMessage());
        }finally {
            return schedule;
        }
    }

    public Schedule getSchedule(String scheduleId) {
        Schedule schedule = scheduleRepository.findScheduleById(scheduleId);
        if (schedule == null) {
            throw new RuntimeException("Schedule job does not exist");
        }
        return schedule;
    }

    public Schedule getSchedule(Schedule schedule) {
        if (schedule == null) {
            throw new RuntimeException("Schedule job does not exist");
        }
        return schedule;
    }

    /**
     * 创建Job
     * @param jobClass 要调度的类名
     * @param sd 调度参数
     * @param jobDataMap 数据
     * @return Schedule
     */
    public Schedule createJob(Class<? extends Job> jobClass, ScheduleDTO sd, JobDataMap jobDataMap, Trigger trigger){
        //判断记录在数据库是否存在
        Schedule schedule = scheduleRepository.findScheduleByJobNameAndGroupName(sd.getJobName(), sd.getGroup());
        if (schedule == null) {
            schedule = new Schedule();
        } else {
            log.error("Schedule job already exists.");
            throw new RuntimeException("Schedule job already exists.");
        }
        String scheduleId = UUID.randomUUID().toString();
        try {
            if (jobDataMap == null) {
                jobDataMap = new JobDataMap();
            }
            if (sd.getData() != null) {
                jobDataMap.put("data", JSON.toJSONString(sd.getData()));
            }
            jobDataMap.put("id", scheduleId);
            //创建JobDetail
            JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(sd.getJobName(), sd.getGroup()).usingJobData(jobDataMap).build();
            schedule.setId(scheduleId);
            schedule.setStatus(ScheduleStatusEnum.ACTIVATED);
            schedule.setJobName(sd.getJobName());
            schedule.setGroupName(sd.getGroup());
            schedule.setTriggerInfo(JSON.toJSONString(sd));
            schedule.setRecord(0);
            //保存记录信息
            scheduleRepository.add(schedule);
            //调度执行定时任务
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (Exception e) {
            log.error("Create schedule job error:{}", e.getMessage());
            throw new RuntimeException(e);
        }
        return schedule;
    }

    public Schedule createSimpleJob(Class<? extends Job> jobClass, SimpleScheduleDTO sd, JobDataMap jobDataMap) {
        Trigger trigger = getSimpleTrigger(sd);
        return createJob(jobClass, sd, jobDataMap, trigger);
    }

    public Schedule createCronJob(Class<? extends Job> jobClass, CronScheduleDTO csd, JobDataMap jobDataMap) {
        Trigger trigger = getCronTrigger(csd);
        return createJob(jobClass, csd, jobDataMap, trigger);
    }

    /**
     * 构建 CronTrigger
     * @param csd
     * @return
     */
    private Trigger getCronTrigger(CronScheduleDTO csd) {
        String cronExpression = csd.getCronExpression();
        TriggerBuilder triggerBuilder = TriggerBuilder.newTrigger()
                .withIdentity(csd.getJobName(), csd.getGroup())
                .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression));
        if (csd.getStartTime() != 0) {
            //设置起始时间
            triggerBuilder.startAt(new Date(csd.getStartTime()));
        } else {
            triggerBuilder.startNow();
        }
        if (csd.getEndTime() != 0) {
            //设置终止时间
            triggerBuilder.endAt(new Date(csd.getEndTime()));
        }
        return triggerBuilder.build();
    }

    /**
     * 构建 SimpleTrigger
     * @param ssd 参数
     * @return Trigger
     */
    private Trigger getSimpleTrigger(SimpleScheduleDTO ssd) {
        int repeatCount = ssd.getRepeatCount();
        TriggerBuilder triggerBuilder = TriggerBuilder.newTrigger()
                //设置jobName和group
                .withIdentity(ssd.getJobName(), ssd.getGroup())
                //设置Schedule方式
                .withSchedule(getSimpeScheduleBuilder(ssd.getPeriod(), repeatCount));
        if (ssd.getStartTime() != 0) {
            //设置起始时间
            triggerBuilder.startAt(new Date(ssd.getStartTime()));
        } else {
            triggerBuilder.startNow();
        }
        if (ssd.getEndTime() != 0) {
            //设置终止时间
            triggerBuilder.endAt(new Date(ssd.getEndTime()));
        }
        return triggerBuilder.build();
    }

    /**
     * 构建 SimpleScheduleBuilder
     * @param period 周期
     * @param repeatCount 重复次数
     * @return
     */
    private SimpleScheduleBuilder getSimpeScheduleBuilder(Period period, int repeatCount) {
        SimpleScheduleBuilder ssb = SimpleScheduleBuilder.simpleSchedule();
        String unit = period.getUnit();
        long time = period.getTime();
        switch (unit) {
            case "milliseconds":
                ssb.withIntervalInMilliseconds(time);
                break;
            case "seconds":
                ssb.withIntervalInSeconds((int) time);
                break;
            case "minutes":
                ssb.withIntervalInMinutes((int) time);
                break;
            case "hours":
                ssb.withIntervalInHours((int) time);
                break;
            case "days":
                ssb.withIntervalInHours((int) time * 24);
                break;
            default:
                break;
        }
        ssb.withRepeatCount(repeatCount);
        return ssb;
    }




}
