package com.lmc.task.repository;

import com.lmc.task.entity.Schedule;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-20 19:30
 * @version: 1.0
 */
@Mapper
public interface ScheduleRepository {

    List<Schedule> findAll();

    Schedule findScheduleByJobNameAndGroupName(@Param("jobName") String jobName, @Param("groupName") String groupName);

    Schedule findScheduleById(String scheduleId);

    void add(Schedule schedule);

    void update(Schedule schedule);

    void delete(Schedule schedule);

}
