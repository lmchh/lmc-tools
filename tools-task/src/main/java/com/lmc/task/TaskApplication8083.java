package com.lmc.task;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.lmc.task.repository")
public class TaskApplication8083 {

    public static void main(String[] args) {
        SpringApplication.run(TaskApplication8083.class, args);
    }
}
