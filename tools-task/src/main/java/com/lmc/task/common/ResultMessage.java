package com.lmc.task.common;

import java.util.Map;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-21 22:51
 * @version: 1.0
 */
public class ResultMessage {

    private int code;

    private String message;

    private Object data;

    public ResultMessage(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
