package com.lmc.task.common;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-22 21:03
 * @version: 1.0
 */
public interface BaseEnum<E extends Enum<?>, T> {
    /**
     * 获取枚举值
     *
     * @return 枚举值
     */
    T getValue();
}