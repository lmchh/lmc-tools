package com.lmc.task.dto;

import com.lmc.task.entity.VisitApi;
import lombok.Data;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-08-31 22:50
 * @version: 1.0
 */
@Data
public class CronScheduleDTO extends ScheduleDTO {

    private String cronExpression;

}
