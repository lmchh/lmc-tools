package com.lmc.task.dto;

import lombok.Data;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-20 20:56
 * @version: 1.0
 */
@Data
public class Period {
    private long time;
    private String unit;
}
