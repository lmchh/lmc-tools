package com.lmc.task.dto;

import lombok.Data;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-20 20:57
 * @version: 1.0
 */
@Data
public class SimpleScheduleDTO extends ScheduleDTO {
    /**
     * 重复次数
     */
    private int repeatCount;
    /**
     * 周期
     */
    private Period period;
}
