package com.lmc.task.dto;

import lombok.Data;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-06-20 20:55
 * @version: 1.0
 */
@Data
public class ScheduleDTO {
    private String jobName;
    private String group;
    private Object data;
    /**
     * 开始时间
     */
    private long startTime;
    /**
     * 结束时间
     */
    private long endTime;
}
