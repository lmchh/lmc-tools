package com.lmc.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lmc.common.enums.ResultCodeEnum;
import lombok.Data;

import java.util.Date;

/**
 * @author lmc
 * @Description: TODO 接口返回结果类型
 * @Create 2022-06-26 13:13
 * @version: 1.0
 */
@Data
public class ResultVo {

    /**
     * 状态码
     */
    private int code;
    /**
     * 状态码信息
     */
    private String msg;
    /**
     * 返回描述信息（预备为调用失败的情况下提供详细的失败原因）
     */
    private String desc;
    /**
     * 返回数据
     */
    private Object data;
    /**
     * 接口调用结束时间
     */
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date searchTime;

    public ResultVo(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.searchTime = new Date();
    }

    /**
     * 调用成功时返回
     * @param data
     * @return
     */
    public static ResultVo success(Object data) {
        return new ResultVo(ResultCodeEnum.SUCCESS.getCode(), ResultCodeEnum.SUCCESS.getMsg(), data);
    }

    /**
     * 调用失败时返回
     * @param data
     * @return
     */
    public static ResultVo fail(Object data) {
        return new ResultVo(ResultCodeEnum.FAILURE.getCode(), ResultCodeEnum.FAILURE.getMsg(), data);
    }

    /**
     * 调用时指定状态码
     * @param enums
     * @param data
     * @return
     */
    public static ResultVo result(ResultCodeEnum enums, Object data) {
        return new ResultVo(enums.getCode(), enums.getMsg(), data);
    }

    public ResultVo withDesc(String desc) {
        this.desc = desc;
        return this;
    }

}
