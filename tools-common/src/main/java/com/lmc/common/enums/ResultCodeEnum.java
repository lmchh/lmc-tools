package com.lmc.common.enums;

/**
 * @author lmc
 * @Description: TODO 接口API返回状态码枚举
 * @Create 2022-06-26 13:16
 * @version: 1.0
 */
public enum ResultCodeEnum {

    SUCCESS(1000, "请求成功"),

    FAILURE(1001, "请求失败"),

    VALIDATE_PARAMS_ERROR(1002, "参数校验失败");

    private int code;

    private String msg;

    ResultCodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 获取code
     * @return
     */
    public int getCode() {
        return code;
    }

    /**
     * 获取信息
     * @return
     */
    public String getMsg() {
        return msg;
    }
}
