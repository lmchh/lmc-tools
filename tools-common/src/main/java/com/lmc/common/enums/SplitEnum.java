package com.lmc.common.enums;

/**
 * @author lmc
 * @Description: TODO 切割字符枚举类型
 * @Create 2022-05-29 16:48
 * @version: 1.0
 */
public enum SplitEnum {

    /**
     * APIMessage字符串切割符
     */
    API_MESSAGE_SPLIT("##");

    private final String split;

    SplitEnum(String split) {
        this.split = split;
    }

    public String value() {
        return split;
    }

}
