package com.lmc.common.enums;

/**
 * @author lmc
 * @Description: TODO 语言类别
 * @Create 2022-05-29 16:23
 * @version: 1.0
 */
public enum LanguageEnum {
    /**
     * 中文
     */
    Chinese("cn"),
    /**
     * 英文
     */
    English("en");

    private final String type;

    LanguageEnum(String type) {
        this.type = type;
    }

    public String value() {
        return type;
    }

}
