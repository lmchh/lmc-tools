package com.lmc.common.entity;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lmc
 * @Description: TODO Web返回结果
 * @Create 2021-10-10 12:42
 * @version: 1.0
 */
@Data
public class WebResult {

    private boolean result;
    private int code;
    private String msg;
    private Map<String, Object> data;

    private WebResult(boolean result, int code, String msg) {
        this.result = result;
        this.code = code;
        this.msg = msg;
    }

    public static WebResult ok(String msg) {
        return new WebResult(true, 200, msg);
    }

    public static WebResult fail(String msg) {
        return new WebResult(false, 500, msg);
    }

    public static WebResult result(boolean result, int code, String msg) {
        return new WebResult(result, code, msg);
    }

    public WebResult withData(Map<String, Object> data) {
        this.data = data;
        return this;
    }

    public WebResult withKeyValue(String key, Object value) {
        this.data = new HashMap<String, Object>();
        data.put(key, value);
        return this;
    }

}
