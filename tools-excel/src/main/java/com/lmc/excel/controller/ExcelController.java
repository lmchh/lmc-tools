package com.lmc.excel.controller;

import com.lmc.excel.util.ExcelUtils;
import com.lmc.excel.util.PublicUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.*;

@Controller
@RequestMapping("excel")
public class ExcelController {

    @RequestMapping("/page")
    public String page() {
        return "page";
    }

    @RequestMapping("index")
    public String index() {
        return "index";
    }

    /**
     * 將在線Excel導出下載
     * @param exceldatas excel数据
     * @param fileName 文件名称
     * @return
     */
    @PostMapping("/downfile")
    @ResponseBody
    public String downData(String exceldatas, String fileName, String echartImg) {
        String fileDirNew = "D:/data/";//保存文件夹名
        String fileNameNew = "test" + "_" + PublicUtils.getNowDataByFormat("yyyyMMdd_HHmmss") + ".xlsx";//保存的文件名

        ExcelUtils.exportLuckySheetXlsxByPOI(echartImg, fileDirNew,fileName,exceldatas);
        return fileDirNew+fileName;

    }



     /**
     * 將文件轉化為字符串
     * @param file
     * @return
     */
    private String txtToString(File file) {
        StringBuilder result = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String s = null;
            while ((s = br.readLine()) != null) {
                result.append(s);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }

}
