package com.lmc.excel.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lmc.excel.model.SummaryVo;
import com.lmc.excel.model.TCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @ClassName: RetestExcel
 * @author: Leemon
 * @Description: TODO
 * @date: 2021/6/12 15:31
 * @version: 1.0
 */
public class RetestExcel {

    public static Map<String, Object> getData() {
        Map<String, Object> map = new HashMap<>();

        List<SummaryVo> summaryVos = new ArrayList<>();
        summaryVos.add(new SummaryVo("R-MUST", 0.0115, 0.05));
        summaryVos.add(new SummaryVo("F-MUST", 0.0215, 0.05));
        summaryVos.add(new SummaryVo("MIDGARD", 0.0144, 0.04));
        summaryVos.add(new SummaryVo("Pearl Cal", 0.005, 0.06));
        summaryVos.add(new SummaryVo("RACK1", 0.0042, 0.04));
        summaryVos.add(new SummaryVo("Pearl test 20cm", 0.0017, 0.03));
        summaryVos.add(new SummaryVo("Pearl test 60cm", 0.0076, 0.03));
        summaryVos.add(new SummaryVo("RACK2", 0.0058, 0.03));
        summaryVos.add(new SummaryVo("R-COMP Miyagi", 0.0207, 0.04));
        summaryVos.add(new SummaryVo("R-COMP Alpha", 0.014, 0.03));
        summaryVos.add(new SummaryVo("VENT2", 0.046, 0.03));
        summaryVos.add(new SummaryVo("SW-DOWNLOAD", 0.0321, 0.03));

        map.put("summaryVos", summaryVos);
        map.put("product", "D54");
        map.put("test",new SummaryVo("VENT2", 0.046, 0.03));
        return map;
    }

    public static void exportTemplate() throws IOException {
        JSONObject data = JSONObject.parseObject(JSON.toJSONString(getData()));

        File file = new File("D:/data/test.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(file));
        XSSFSheet sheet1 = workbook.getSheetAt(0);
        int firstRowNum = sheet1.getFirstRowNum();
        int lastRowNum = sheet1.getLastRowNum();
        Row firstRow = sheet1.getRow(firstRowNum);
        int firstCellNum = firstRow.getFirstCellNum();
        int lastCellNum = firstRow.getLastCellNum();
        //从最后一行开始遍历
        for(int i = lastRowNum; i >= firstRowNum; i--) {
            List<TCell> cellList = new ArrayList<TCell>();
            for(int j = firstCellNum; j < lastCellNum; j++) {
                Cell cell = sheet1.getRow(i).getCell(j);
                //包含list表達式
                if (cell != null && cell.getStringCellValue() != null && !"".equals(cell.getStringCellValue())) {
                    if (cell.getStringCellValue().contains("${")) {
                        cellList.add(new TCell(cell, i, j, cell.getStringCellValue().substring(2, cell.getStringCellValue().length() - 1)));
                    }
                    //包含值填充表達式
                    if (cell.getStringCellValue().contains("#{")) {
                        valueFill(cell, data);
                    }
                }
            }
            //是否包含表達式，是則創建行
            if (cellList.size() > 0) {
                //包含${}，进行插入行与映射数据
                listFill(cellList, data, sheet1);
            }
        }

        //将workbook生成文件
        String newFileDir = "D:/data/";
        String newFileName = "retest_01.xlsx";
        exportFile(newFileDir, newFileName, workbook);
    }

    /**
     * 包含${}，进行插入行与映射数据
     * @param cellList
     * @param data
     * @param sheet
     */
    public static void listFill(List<TCell> cellList, JSONObject data, XSSFSheet sheet) {
        String listName = cellList.get(0).getExpressName().get(0);//获取要遍历的数据名称
        JSONArray jsonArray = data.getJSONArray(listName);//通过名称获取json数据
        int rowIndex = cellList.get(0).getRowIndex();//获取开始遍历的行数
        //迁移行
        sheet.shiftRows(rowIndex+1, sheet.getLastRowNum(), jsonArray.size());
        for (int m = 0; m < cellList.size(); ++m) {
            for (int n = 0; n < jsonArray.size(); ++n) {
                TCell tcell = cellList.get(m);
                if (n == 0) {
                    //往Excel填充數據
                    JSONObject object = jsonArray.getJSONObject(0);
                    sheet.getRow(rowIndex)
                            .getCell(tcell.getCellIndex())
                            .setCellValue(getValue(tcell, object));
                } else {
                    //獲取或創建當前行
                    XSSFRow row = null;
                    if (m == 0) {
                        row = sheet.createRow(rowIndex + n);//创建行
                        row.setRowStyle(sheet.getRow(rowIndex).getRowStyle());//设置样式
                        row.setHeight(sheet.getRow(rowIndex).getHeight());//设置行高度
                    }else {
                        row = sheet.getRow(rowIndex + n);
                    }
                    JSONObject object = jsonArray.getJSONObject(n);
                    int index = tcell.getCellIndex();//获取单元格索引
                    XSSFCell cell = row.createCell(index);//创建单元格
                    cell.setCellStyle(tcell.getCell().getCellStyle());
                    cell.setCellValue(getValue(tcell, object));
                }
            }
        }
    }

    /**
     * 包含#{}， 进行值的插入
     * 只支持 #{a}, #{a.b}
     * @param cell
     * @param data
     */
    public static void valueFill(Cell cell, JSONObject data) {
        String value = cell.getStringCellValue();// hhh#{lmc}xxx#{hh.a} hhh
        List<String> values = PatternUtils.getKeyListByContent(value);
        if (values != null && values.size() > 0) {
            for (int i = 0; i < values.size(); ++i) {
                if (values.get(i).contains(".")) {
                    JSONObject object = data.getJSONObject(values.get(i).split("\\.")[0]);
                    value = value.replace("#{"+values.get(i)+"}", object.getString(values.get(i).split("\\.")[1]));
                    cell.setCellValue(value);
                }else {
                    value = value.replace("#{"+values.get(i)+"}", data.getString(values.get(i)));
                    cell.setCellValue(value);
                }
            }
        }
    }

    /**
     * 对单元格的值进行格式化
     * @param tCell
     * @param jsonObject
     * @return
     */
    public static String getValue(TCell tCell, JSONObject jsonObject) {
        /**
         * 1 字符串
         * 2 整型
         * 3 浮点类型
         */
        if (tCell.getType() == 1) {
            return jsonObject.getString(tCell.getExpressName().get(1));
        }else if (tCell.getType() == 2) {
            if (tCell.gettemplate() == null) {
                return jsonObject.getString(tCell.getExpressName().get(1));
            }else {
                DecimalFormat format = new DecimalFormat(tCell.gettemplate());
                return format.format(jsonObject.getInteger(tCell.getExpressName().get(1)));
            }
        }else if (tCell.getType() == 3) {
            if (tCell.gettemplate() == null) {
                return jsonObject.getString(tCell.getExpressName().get(1));
            }else {
                DecimalFormat format = new DecimalFormat(tCell.gettemplate());
                return format.format(jsonObject.getDoubleValue(tCell.getExpressName().get(1)));
            }
        }
        return "";
    }

    /**
     * 将workbook生成文件
     * @param dictory
     * @param fileName
     * @param workbook
     */
    public static void exportFile(String dictory, String fileName, XSSFWorkbook workbook) {
        File dir = new File(dictory);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        OutputStream out = null;
        try {
            out = new FileOutputStream(dictory + fileName);
            workbook.write(out);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
//        String lmc = "${summaryVos.retestR1}";
//        String key = lmc.substring(2, lmc.length() - 1);
//        System.out.println("key = " + key);
//        List<String> keys = Arrays.asList(key.split("\\."));
//        System.out.println("keys = " + keys.toString());
//        JSONObject data = JSONObject.parseObject(JSON.toJSONString(getData()));
//        JSONArray jsonArray = data.getJSONArray(keys.get(0));
//        System.out.println(jsonArray.toString());

        RetestExcel.exportTemplate();

//        Double d = 0.0215;
//        DecimalFormat format = new DecimalFormat("#.##%");
//        System.out.println(new DecimalFormat("#.##%").format(d));

//        String name = "test.station";
//        System.out.println(name.contains("."));


    }

}
