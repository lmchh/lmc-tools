package com.lmc.excel.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PublicUtils {

    public static String getNowDataByFormat(String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Date date = new Date();
        return dateFormat.format(date);
    }

}
