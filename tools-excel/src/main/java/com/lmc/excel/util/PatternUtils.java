package com.lmc.excel.util;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternUtils {

    public static Pattern dynamic = Pattern.compile(".*\\$\\{([a-z]+)\\}.*");
    public static Pattern dynamicLimitCount = Pattern.compile("#\\{([a-zA-Z.0-9]+)\\}");

    /**
     * 按照动态内容的参数出现顺序,将参数放到List中
     * @param content
     * @return
     */
    public static List<String> getKeyListByContent(String content) {
        Set<String> paramSet = new LinkedHashSet<>();
        Matcher m = dynamicLimitCount.matcher(content);
        while (m.find()) {
            paramSet.add(m.group(1));
        }
        return new ArrayList<>(paramSet);
    }

    public static void main(String[] args) {
        //测试代码
        String content = "尊敬的#{Name}客户您好，请于${lmc.time}前到达";
        List<String> keyListByContent = getKeyListByContent(content);
        System.out.println("内容中的动态参数为:");
        keyListByContent.forEach(System.out::println);
    }

}
