package com.lmc.excel.model;


import java.util.List;

/**
* @author:  Nice 
* @Description : 每个工栈的测试结果 --> 封装成Summary对象  (第一张sheet所需要的数据)
* @date: 创建时间：2019年6月24日 下午4:16:59 
* @version: 1.0 
*/

public class SummaryVo {

	/**
	 * @author: Nice
	 * @Description:
	 * @version:
	 * @date: 2019年11月20日上午11:19:20
	 */
	private static final long serialVersionUID = 1L;
	private String station;           //工站名	
	private Double retestR1;          //工站總重測率R1 （n2/n1）
	private Double target;            //目標重測率列表L2	
	private Integer input;                //工站總投入量N1（不重复的sn数）
	private Integer retestN2;             //工站總重測量N2 （先fail后pass的sn数）	
	List<RetestItemVo> retestItem;

	public SummaryVo(String station, Double retestR1, Double target) {
		this.station = station;
		this.retestR1 = retestR1;
		this.target = target;
	}

	public String getStation() {
		return station;
	}
	public void setStation(String station) {
		this.station = station;
	}

	
	public Double getRetestR1() {
		return retestR1;
	}
	public void setRetestR1(Double retestR1) {
		this.retestR1 = retestR1;
	}
	public Double getTarget() {
		return target;
	}
	public void setTarget(Double target) {
		this.target = target;
	}
	public Integer getInput() {
		return input;
	}
	public void setInput(Integer input) {
		this.input = input;
	}
	public Integer getRetestN2() {
		return retestN2;
	}
	public void setRetestN2(Integer retestN2) {
		this.retestN2 = retestN2;
	}
	public List<RetestItemVo> getRetestItem() {
		return retestItem;
	}
	public void setRetestItem(List<RetestItemVo> retestItem) {
		this.retestItem = retestItem;
	}
	
	
	
	
}
