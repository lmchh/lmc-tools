package com.lmc.excel.model;

/**
* @author:  Nice 
* @Description :每个工栈含有的测试项的测试结果 (第二张sheet需要的数据)
* @date: 创建时间：2019年7月1日 上午8:48:00 
* @version: 1.0 
*/
public class RetestItemVo {

	/**
	 * @author: Nice
	 * @Description:
	 * @version:
	 * @date: 2019年11月20日上午9:36:04
	 */
	private static final long serialVersionUID = 1L;
	private String issueDes;          //重測測試項列表L1
	private Double rateR2;            //測試項重測率R2
	private Integer retstCount;           //測試項的重測次數N3
	public String getIssueDes() {
		return issueDes;
	}
	public void setIssueDes(String issueDes) {
		this.issueDes = issueDes;
	}
	public Double getRateR2() {
		return rateR2;
	}
	public void setRateR2(Double rateR2) {
		this.rateR2 = rateR2;
	}
	public Integer getRetstCount() {
		return retstCount;
	}
	public void setRetstCount(Integer retstCount) {
		this.retstCount = retstCount;
	}
	
	
	
}
