package com.lmc.excel.model;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName: TCell
 * @author: Leemon
 * @Description: TODO
 * @date: 2021/6/12 17:02
 * @version: 1.0
 */
public class TCell {

    private Cell cell;

    private int rowIndex;

    private int cellIndex;

    private String expression;


    private List<String> expressions;
    private List<String> expressName;

    public TCell(Cell cell, int rowIndex, int cellIndex) {
        this.cell = cell;
        this.rowIndex = rowIndex;
        this.cellIndex = cellIndex;
    }

    public TCell(Cell cell, int rowIndex, int cellIndex, String expression) {
        this.cell = cell;
        this.rowIndex = rowIndex;
        this.cellIndex = cellIndex;
        this.expression = expression;
        //将表达式以 , 切割 (名称，类型，模板)
        if (expression != null && !"".equals(expression)) {
            expressions = Arrays.asList(expression.split(","));
            expressions.forEach(e -> e.trim());
        }
        //获取名称
        if (expressions.get(0) != null && !"".equals(expressions.get(0))) {
            expressName = Arrays.asList(expressions.get(0).split("\\."));
        }
    }

    /**
     * 获取类型
     * @return
     */
    public Integer getType() {
        if (expressions.size() >= 2) {
            return Integer.valueOf(expressions.get(1));
        }
        return 1;
    }

    /**
     * 获取模板
     * @return
     */
    public String gettemplate() {
        if (expressions.size() == 3) {
            return expressions.get(2);
        }
        return null;
    }

    public List<String> getExpressName() {
        return this.expressName;
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(XSSFCell cell) {
        this.cell = cell;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public int getCellIndex() {
        return cellIndex;
    }

    public void setCellIndex(int cellIndex) {
        this.cellIndex = cellIndex;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
        if (expression != null && !"".equals(expression)) {
            expressions = Arrays.asList(expression.split(","));
            expressions.forEach(e -> e.trim());
        }
        //获取名称
        if (expressions.get(0) != null && !"".equals(expressions.get(0))) {
            expressName = Arrays.asList(expressions.get(0).split("\\."));
        }
    }
}
