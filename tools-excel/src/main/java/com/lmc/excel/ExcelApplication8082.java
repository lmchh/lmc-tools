package com.lmc.excel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExcelApplication8082 {

    public static void main(String[] args) {
        SpringApplication.run(ExcelApplication8082.class, args);
    }

}
