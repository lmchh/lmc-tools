package com.lmc.gateway.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-10-16 16:32
 * @version: 1.0
 */
@Data
public class GatewayRouteDefinition {

    /**
     * 路由的ID
     */
    private String id;
    /**
     * 路由断言集合配置
     */
    private List<GatewayPredicateDefinition> predicates = new ArrayList<>();
    /**
     * 路由过滤器集合配置
     */
    private List<GatewayFilterDefinition> filters = new ArrayList<>();
    /**
     * 路由规则转发的目标uri
     */
    private String uri;
    /**
     * 路由执行的顺序
     */
    private int order = 0;

}
