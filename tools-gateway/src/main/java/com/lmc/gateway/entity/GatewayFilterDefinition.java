package com.lmc.gateway.entity;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-10-16 16:33
 * @version: 1.0
 */
@Data
public class GatewayFilterDefinition {

    /**
     * 过滤器名称
     */
    private String name;
    /**
     * 对应的路由规则
     */
    private Map<String, String> args = new LinkedHashMap<>();

}
