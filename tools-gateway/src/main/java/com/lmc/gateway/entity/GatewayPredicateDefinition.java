package com.lmc.gateway.entity;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-10-16 16:32
 * @version: 1.0
 */
@Data
public class GatewayPredicateDefinition {

    /**
     * 断言对应的name
     */
    private String name;
    /**
     * 配置的断言规则
     */
    private Map<String, String> args = new LinkedHashMap<String, String>();

}
