package com.lmc.gateway.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-10-17 16:25
 * @version: 1.0
 */
@RestController
@RequestMapping("test")
public class TestController {

    @Value("${gateway-key:xxx}")
    String key;

    @RequestMapping("api")
    public String test() {
        return "gateway test";
    }

    @RequestMapping("key")
    public String key() {
        return key;
    }
}
