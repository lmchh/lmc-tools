package com.lmc.gateway.controller;

import com.lmc.gateway.entity.GatewayFilterDefinition;
import com.lmc.gateway.entity.GatewayPredicateDefinition;
import com.lmc.gateway.entity.GatewayRouteDefinition;
import com.lmc.gateway.service.DynamicRouteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.FilterDefinition;
import org.springframework.cloud.gateway.handler.predicate.PredicateDefinition;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-10-16 16:39
 * @version: 1.0
 */
@RestController
@RequestMapping("route")
public class RouteController {

    @Autowired
    private DynamicRouteServiceImpl dynamicRouteService;

    /**
     * 添加路由
     **/
    @PostMapping("/add")
    public String add(@RequestBody GatewayRouteDefinition gwdefinition) {
        System.out.println("add = " + gwdefinition.toString());
        String flag = "fail";
        try {
            RouteDefinition definition = assembleRouteDefinition(gwdefinition);
            flag = this.dynamicRouteService.add(definition);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 删除路由
     **/
    @PostMapping("/routes/{id}")
    public Mono<ResponseEntity<Object>> delete(@PathVariable String id) {
        try {
            return this.dynamicRouteService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 修改路由
     **/
    @PostMapping("/update")
    public String update(@RequestBody GatewayRouteDefinition gwdefinition) {
        System.out.println("update = " + gwdefinition.toString());
        RouteDefinition definition = assembleRouteDefinition(gwdefinition);
        return this.dynamicRouteService.update(definition);
    }

    private RouteDefinition assembleRouteDefinition(GatewayRouteDefinition gwdefinition) {
        RouteDefinition definition = new RouteDefinition();
        definition.setId(gwdefinition.getId());
        definition.setOrder(gwdefinition.getOrder());
        //设置断言
        List<PredicateDefinition> pdList = new ArrayList<PredicateDefinition>();
        List<GatewayPredicateDefinition> gatewayPredicateDefinitionList = gwdefinition.getPredicates();
        for (GatewayPredicateDefinition gPDefinition : gatewayPredicateDefinitionList) {
            PredicateDefinition predicate = new PredicateDefinition();
            predicate.setArgs(gPDefinition.getArgs());
            predicate.setName(gPDefinition.getName());
            pdList.add(predicate);
        }
        definition.setPredicates(pdList);

        //设置过滤器
        List<FilterDefinition> fdList = new ArrayList<>();
        List<GatewayFilterDefinition> gatewayFilterDefinitionList = gwdefinition.getFilters();
        for (GatewayFilterDefinition gFDefinition : gatewayFilterDefinitionList) {
            FilterDefinition filter = new FilterDefinition();
            filter.setArgs(gFDefinition.getArgs());
            filter.setName(gFDefinition.getName());
            fdList.add(filter);
        }
        definition.setFilters(fdList);
        System.out.println("gatewayFilterDefinitionList = " + gatewayFilterDefinitionList.toString());
        System.out.println("fdList = " + fdList.toString());
        URI uri = null;
        if (gwdefinition.getUri().startsWith("http")) {
            uri = UriComponentsBuilder.fromHttpUrl(gwdefinition.getUri()).build().toUri();
        } else {
            uri = URI.create(gwdefinition.getUri());
        }
        definition.setUri(uri);
        return definition;
    }

}
