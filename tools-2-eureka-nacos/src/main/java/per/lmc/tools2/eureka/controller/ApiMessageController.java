package per.lmc.tools2.eureka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import per.lmc.tools2.eureka.config.MyNacosProperties;
import com.lmc.common.enums.LanguageEnum;
import com.lmc.common.enums.SplitEnum;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lmc
 * @Description: TODO API返回参数接口
 * @Create 2022-05-29 13:45
 * @version: 1.0
 */
@RestController
@RequestMapping("/apiMessages")
public class ApiMessageController {

    @Autowired
    private MyNacosProperties myNacosProperties;

    /**
     * 获取所有apiMessages
     * @param language 语言
     * @return 数据集合
     */
    @GetMapping("/getAll")
    public Map<String, String> apiMessages(String language) {
        List<String> apiMessages = myNacosProperties.getApiMessages();
        Map<String, String> result = new HashMap<>(20);
        if (!CollectionUtils.isEmpty(apiMessages)) {
            if (LanguageEnum.English.value().equalsIgnoreCase(language)) {
                apiMessages.forEach(a -> result.put(a.split(SplitEnum.API_MESSAGE_SPLIT.value())[0], a.split(SplitEnum.API_MESSAGE_SPLIT.value())[2]));
            }else {
                apiMessages.forEach(a -> result.put(a.split(SplitEnum.API_MESSAGE_SPLIT.value())[0], a.split(SplitEnum.API_MESSAGE_SPLIT.value())[1]));
            }
        }
        return result;
    }

    /**
     * 通过code获取APIMessage
     * @param language 语言
     * @param code 参数代码
     * @return 参数代码指定信息
     */
    @GetMapping("/getOne")
    public String getMessage(String language, String code) {
        // 获取指定code的记录
        List<String> apiMessages = myNacosProperties.getApiMessages().stream().filter(a -> a.startsWith(code)).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(apiMessages)) {
            return String.format("不存在code为%s的记录", code);
        }
        if (LanguageEnum.English.value().equalsIgnoreCase(language)) {
            return apiMessages.get(0).split(SplitEnum.API_MESSAGE_SPLIT.value())[2];
        }else {
            return apiMessages.get(0).split(SplitEnum.API_MESSAGE_SPLIT.value())[1];
        }

    }

}
