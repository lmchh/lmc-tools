package per.lmc.tools2.eureka.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author lmc
 * @Description: TODO Nacos配置映射类
 * @Create 2022-05-29 14:05
 * @version: 1.0
 */
@Configuration
@ConfigurationProperties(prefix = "tools")
@RefreshScope
@Data
public class MyNacosProperties {

    List<String> apiMessages;

}
