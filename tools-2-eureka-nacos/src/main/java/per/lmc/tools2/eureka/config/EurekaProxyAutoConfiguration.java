//package per.lmc.tools2.eureka.config;
//
//import com.alibaba.nacos.api.NacosFactory;
//import com.alibaba.nacos.api.exception.NacosException;
//import com.alibaba.nacos.api.naming.NamingMaintainService;
//import com.alibaba.nacos.api.naming.NamingService;
//import com.netflix.eureka.registry.PeerAwareInstanceRegistry;
//import net.nacos.eureka.NacosDiscoveryProperties;
//import net.nacos.eureka.sync.EurekaSynchronizer;
//import net.nacos.eureka.sync.NacosSynchronizer;
//import org.springframework.boot.autoconfigure.AutoConfigureAfter;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.cloud.client.serviceregistry.AutoServiceRegistrationAutoConfiguration;
//import org.springframework.cloud.client.serviceregistry.AutoServiceRegistrationConfiguration;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Import;
//
///**
// * @author lmc
// * @Description: TODO
// * @Create 2022-05-29 10:57
// * @version: 1.0
// */
//@Configuration
//@EnableConfigurationProperties(NacosDiscoveryProperties.class)
//@AutoConfigureAfter({AutoServiceRegistrationConfiguration.class,
//        AutoServiceRegistrationAutoConfiguration.class, PeerAwareInstanceRegistry.class})
//@Import({EurekaSynchronizer.class, NacosSynchronizer.class})
//@ComponentScan("per.lmc.tools2.eureka.config")
//public class EurekaProxyAutoConfiguration {
//
//    @Bean
//    public NamingService namingService(NacosDiscoveryProperties nacosDiscoveryProperties) throws NacosException {
//        return NacosFactory.createNamingService(nacosDiscoveryProperties);
//    }
//
//    @Bean
//    public NamingMaintainService namingMaintainService(NacosDiscoveryProperties nacosDiscoveryProperties) throws NacosException {
//        return NacosFactory.createMaintainService(nacosDiscoveryProperties);
//    }
//}