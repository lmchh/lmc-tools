package per.lmc.tools2.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-05-28 19:39
 * @version: 1.0
 */
@SpringBootApplication//(exclude = {AutoServiceRegistrationAutoConfiguration.class})
@EnableDiscoveryClient
public class EurekaProxyApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaProxyApplication.class, args);
    }
}
