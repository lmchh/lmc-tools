package com.lmc.config.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-03-14 20:34
 * @version: 1.0
 */
@Data
public class ToolsConfiguration {

    private Integer id;
    private String key;
    private String application;
    private String label;
    private String profile;
    private String value;
    private Date createTime;
    private Date updateTime;

}
