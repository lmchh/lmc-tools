package com.lmc.kafka;

import com.lmc.common.entity.WebResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-10-10 12:30
 * @version: 1.0
 */
@RestController
@RequestMapping("kafka")
public class KafkaController {

    @Autowired
    private KafkaSender kafkaSender;

    @RequestMapping("send/{msg}")
    public WebResult send(@PathVariable("msg") String msg) {
        if (StringUtils.isNoneEmpty(msg)) {
            try {
                kafkaSender.send(msg);
                return WebResult.ok("发送消息成功");
            }catch (Exception e) {
                return WebResult.fail("发送消息失败：" + e.getMessage());
            }
        }else {
            return WebResult.result(false, 404, "消息内容为空，拒绝发送");
        }

    }

}
