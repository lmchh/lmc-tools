package com.lmc.kafka;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-09-26 23:03
 * @version: 1.0
 */
@Component
public class KafkaSender {

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    public void send(String msg) {
        kafkaTemplate.send("test", "this is my message:" + msg);
    }

}
