package com.lmc.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-09-26 23:11
 * @version: 1.0
 */
@Component
public class KafkaConsumer {

    public static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    @KafkaListener(topics = {"test"})
    public void consumer(String message) {
        logger.info("test topic message: {}", message);
    }

}
