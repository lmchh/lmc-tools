package com.lmc.rabbitmq.spring.consumer;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-09-18 19:32
 * @version: 1.0
 */
@Component
public class LmcTestConsumer {

    public static final Logger logger = LoggerFactory.getLogger(LmcTestConsumer.class);


    @RabbitHandler
    @RabbitListener(queues = "lmc-test")
    public void handler(@Payload Message message, Channel channel) {
        try {
            String msg = new String(message.getBody(), "UTF-8");
            MqMessageDispatcher.doDispatch(msg, channel, message.getMessageProperties().getDeliveryTag());
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (NullPointerException e1) {
            logger.error(e1.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

}
