package com.lmc.rabbitmq.spring.consumer;

import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-09-07 22:45
 * @version: 1.0
 */
public class MqMessageDispatcher {

    public static final Logger logger = LoggerFactory.getLogger(MqMessageDispatcher.class);

    public static ExecutorService msgHandleService = Executors.newFixedThreadPool(5);

    public static Map<String, Integer> cacheMap = new HashMap(5);

    static {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                msgHandleService.shutdown();
            }
        });
    }

    public static void doDispatch(String message, Channel channel, long envelopeTag) {
        msgHandleService.execute(new MessageHandleTask(message, channel, envelopeTag));
    }

    private static class MessageHandleTask implements Runnable {

        String message;
        Channel channel;
        long envelopeTag;

        public MessageHandleTask(String message, Channel channel, long envelopeTag) {
            this.message = message;
            this.channel = channel;
            this.envelopeTag = envelopeTag;
        }

        @Override
        public void run() {

            int currentTimes = 0; // 当前重试次数
            boolean isSuccess = false; // 消息是否处理成功
            // 获取当前消息重试次数，（这种情况适合每条消息内容不一样，最好每条消息都有唯一标识）
            if (cacheMap.containsKey(message)) {
                currentTimes = cacheMap.get(message);
            }else {
                cacheMap.put(message, 0);
            }

            long start = System.currentTimeMillis();
            logger.info("Received message: " + message);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                if (isSuccess) {
                    // 手动确认消息
                    logger.info("message[" + message + "] consumer success.（Ack）");
                    cacheMap.put(message, 0);
                    channel.basicAck(envelopeTag, false);
                }else {
                    if (currentTimes >= 5) {
                        // 手动确认消息，若自动确认则不需要写以下该行
                        logger.warn("message[" + message + "] consumer fail，have retry 5 times.（Ack）");
                        cacheMap.put(message, 0);
                        channel.basicAck(envelopeTag, false);
                    }else {
                        // 处理失败，重试未5次，重新处理
                        cacheMap.put(message, ++currentTimes);
                        logger.warn("message[" + message + "] consumer fail，prepare to retry " + currentTimes + " times...（Nack）");
                        channel.basicNack(envelopeTag, false, true);
                    }
                }

            } catch (IOException e) {
                System.err.println("fail to confirm message:" + message);
            }
        }
    }


}
