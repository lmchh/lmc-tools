package com.lmc.rabbitmq.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-09-18 19:29
 * @version: 1.0
 */
@SpringBootApplication
public class Message8085Application {

    public static void main(String[] args) {
        SpringApplication.run(Message8085Application.class);
    }
}
