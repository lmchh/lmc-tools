# lmc-tools相关内容

历史文件关于luckysheet的内容，已经转移到模块tools-excel内部的readme了，地址： https://gitee.com/lmchh/lmc-tools/tree/master/tools-excel

tools-common 公共模块，引入eureka，springcloud config，actuator，swagger，sleuth依赖，创建了响应自定义类

tools-config 配置中心
t
ools-echart 尝试magical而创建，已废弃

tools-excel Java使用LuckySheet实现线上Excel，通过后台导出Echart图片

tools-gateway 网关

tools-task 定时任务

tools-2-eureka-nacos 微服务同时使用nacos和eureka
tools-2-provider 统一异常处理，接口参数验证