package pers.lmc.tools2.consumer.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-04-13 22:09
 * @version: 1.0
 */
@FeignClient(value = "tools2-provider")
public interface ProviderFeign {

    @RequestMapping("/provider/test/sample01")
    @ResponseBody
    public String sample01();

    @RequestMapping("/provider/test/value01")
    @ResponseBody
    public String value01();

}
