package pers.lmc.tools2.consumer.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.lmc.tools2.consumer.feign.ProviderFeign;

import javax.annotation.Resource;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-04-11 23:34
 * @version: 1.0
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Resource
    private ProviderFeign providerFeign;


    @RequestMapping("/sample01")
    public String sample01() throws InterruptedException {
        System.out.println("=====");
        Thread.sleep(10000);
        return providerFeign.sample01();
    }

    @RequestMapping("/value01")
    public String value01() {
        return providerFeign.value01();
    }

}
