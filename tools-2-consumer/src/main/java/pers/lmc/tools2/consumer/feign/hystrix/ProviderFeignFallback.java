//package pers.lmc.tools2.consumer.feign.hystrix;
//
//import feign.hystrix.FallbackFactory;
//import org.springframework.stereotype.Component;
//import pers.lmc.tools2.consumer.feign.ProviderFeign;
//
///**
// * @author lmc
// * @Description: TODO
// * @Create 2022-04-13 22:11
// * @version: 1.0
// */
//@Component
//public class ProviderFeignFallback implements FallbackFactory<ProviderFeign> {
//
//    @Override
//    public ProviderFeign create(Throwable cause) {
//        return new ProviderFeign() {
//            @Override
//            public String sample01() {
//                return "服务报错，导致服务降级：" + cause.getMessage();
//            }
//
//            @Override
//            public String value01() {
//                return "服务报错，导致服务降级：" + cause.getMessage();
//            }
//        };
//    }
//}
