package pers.lmc.tools2.consumer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-04-16 22:05
 * @version: 1.0
 */
@Configuration
public class MyConfiguration {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
