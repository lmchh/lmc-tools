package com.lmc.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-03-25 23:08
 * @version: 1.0
 */
@RestController
public class DemoController {

    @RequestMapping("test")
    public String test() {
        return "demo  test";
    }
}
