package pers.lmc.tools.admin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2022-04-07 20:51
 * @version: 1.0
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableAdminServer
public class Admin2Applicatin {

    public static void main(String[] args) {
        SpringApplication.run(Admin2Applicatin.class, args);
    }

}
