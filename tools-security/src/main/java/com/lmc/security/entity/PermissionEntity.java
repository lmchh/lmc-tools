package com.lmc.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Date;

/**
 * @author lmc
 * @Description: TODO 角色实体类
 * @Create 2021-11-01 22:15
 * @version: 1.0
 */
@Data
@TableName(value = "sys_permission")
public class PermissionEntity implements GrantedAuthority {

    private Integer id;
    private String authority;
    private String permissionDesc;
    private Date created;
    private Date updated;

}
