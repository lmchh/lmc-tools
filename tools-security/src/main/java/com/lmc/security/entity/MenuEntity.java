package com.lmc.security.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author lmc
 * @Description: TODO 菜单实体类
 * @Create 2021-11-01 22:47
 * @version: 1.0
 */
@Data
public class MenuEntity {

    private Integer id;
    private String menuName;
    private String menuPath;
    private String imagesUrl;
    private MenuEntity parent;
    private Date created;
    private Integer creator;
    private Date updated;
    private Integer updator;

}
