package com.lmc.security.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;
import java.util.List;

/**
 * @author lmc
 * @Description: TODO 用户实体类
 * @Create 2021-11-01 22:11
 * @version: 1.0
 */
@Data
@TableName(value = "sys_user")
public class UserEntity implements UserDetails {

    private Integer id;
    private String username;
    private String password;
    private Date created;
    private Date updated;
    private Boolean enabled;
    @TableField(exist = false)
    private List<PermissionEntity> authorities;

    /**
     * 用户是否不过期
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * 账号是否不被锁
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * 该凭证是否可用
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * 该用户是否可用
     * @return
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}
