package com.lmc.security.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-10-23 16:15
 * @version: 1.0
 */
@Controller
public class PageController {

    /**
     * 登录页面
     * @return
     */
    @RequestMapping("login.html")
    public String login(String message, HttpServletRequest request) {
        if (StringUtils.isNotEmpty(message)) {
            request.setAttribute("message", message);
        }else {
            request.setAttribute("message", "");
        }
        return "login";
    }

    @RequestMapping("home.html")
    public String index() {
        return "indexHome";
    }

    @RequestMapping("403")
    public String accessError() {
        return "403";
    }

    @RequestMapping("404")
    public String notFound() {
        return "404";
    }

    @RequestMapping("500")
    public String serverError() {
        return "500";
    }

}
