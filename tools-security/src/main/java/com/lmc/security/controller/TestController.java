package com.lmc.security.controller;

import com.lmc.security.entity.UserEntity;
import com.lmc.security.mapper.UserEntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-10-23 15:44
 * @version: 1.0
 */
@RestController
public class TestController {

    @Autowired
    private UserEntityMapper userEntityMapper;

    @RequestMapping("test/t01")
    public String test01() {
        return "test01";
    }

    @RequestMapping("lmc")
    public String lmc() {
        UserEntity userEntity = userEntityMapper.selectById(1);

        return "lmc = " + userEntity.toString();
    }

}
