package com.lmc.security.controller;

import com.lmc.common.entity.WebResult;
import com.lmc.security.service.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-11-03 20:55
 * @version: 1.0
 */
@RestController
@RequestMapping("/user")
public class UserEntityController {

    @Autowired
    private UserEntityService userEntityService;

    /**
     * 判断该用户是否存在
     * @param username
     * @return
     */
    @RequestMapping("/isExistUser/{username}")
    public WebResult isExistUser(@PathVariable("username") String username) {
        if (userEntityService.isExistName(username)) {
            return WebResult.ok("该用户存在").withKeyValue("isExist", true);
        }else {
            return WebResult.ok("该用户名尚未注册").withKeyValue("isExist", false);
        }
    }

}
