package com.lmc.security.service;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-11-03 20:46
 * @version: 1.0
 */
public interface UserEntityService{

    /**
     * 获取该用户名是否存在
     * @param username
     * @return
     */
    Boolean isExistName(String username);

}
