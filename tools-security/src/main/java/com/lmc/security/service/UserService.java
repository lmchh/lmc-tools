package com.lmc.security.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lmc.security.entity.PermissionEntity;
import com.lmc.security.entity.UserEntity;
import com.lmc.security.mapper.PermissionEntityMapper;
import com.lmc.security.mapper.UserEntityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-11-01 23:12
 * @version: 1.0
 */
@Service
public class UserService implements UserDetailsService {

    private final static Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserEntityMapper userEntityMapper;
    @Autowired
    private PermissionEntityMapper permissionEntityMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserEntity userEntity = userEntityMapper.selectOne(new QueryWrapper<UserEntity>().eq("username", s).last("limit 1"));
        if (Objects.nonNull(userEntity)) {
            List<PermissionEntity> permissionEntityList = permissionEntityMapper.getAuthoritiesByUserId(userEntity.getId());
            userEntity.setAuthorities(permissionEntityList);
        }
        logger.info("userEntity:{}", userEntity);
        return userEntity;
    }
}
