package com.lmc.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lmc.security.entity.UserEntity;
import com.lmc.security.mapper.UserEntityMapper;
import com.lmc.security.service.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-11-03 20:48
 * @version: 1.0
 */
@Service
public class UserEntityServiceImpl implements UserEntityService {

    @Autowired
    private UserEntityMapper userEntityMapper;

    @Override
    public Boolean isExistName(String username) {
        UserEntity userEntity = userEntityMapper.selectOne(new QueryWrapper<UserEntity>().eq("username", username).last("limit 1"));
        if (Objects.nonNull(userEntity)) {
            return true;
        }
        return false;
    }
}
