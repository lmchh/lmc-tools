package com.lmc.security.conf;

import com.lmc.security.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-10-23 19:55
 * @version: 1.0
 */
@Configuration
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final static Logger logger = LoggerFactory.getLogger(WebSecurityConfiguration.class);

    @Autowired
    private UserService userService;

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/assets/**")
                .antMatchers("/403", "/404", "/500")
                .antMatchers("/login.html", "/user/isExistUser/*");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth
//                .inMemoryAuthentication()   // 在内存存储用户信息
////                .withUser("lmc").password("{noop}123") // 用户名为lmc,密码为123，{noop}表示密码不加密
//                .passwordEncoder(new BCryptPasswordEncoder()) // 使用BC加密方式
//                .withUser("lmc").password(new BCryptPasswordEncoder().encode("123"))
//                .roles("USER");  // 给用户lmc赋予USER角色
        auth
                .userDetailsService(userService).passwordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
//                .antMatchers("/403", "/404", "/500").permitAll()    // 错误页面：访问403/404/500时不需要认证
//                .antMatchers("/login.html").permitAll() // 登录页面不需要认证
//                .antMatchers("/static/**", "/assets/**").permitAll() // 静态资源不需要认证
                .antMatchers("/home.html").hasRole("DEFAULT")
                .anyRequest().authenticated()   // 其他请求需要认证
                .and()
                .formLogin()    // 通过form进行登录
                .loginPage("/login.html")   // 登陆界面
                .loginProcessingUrl("/login")   // 登陆访问路径：提交表单之后跳转的地址,可以看作一个中转站，这个步骤就是验证user的一个过程
//                .successHandler(myLoginSuccessHandler)
                .defaultSuccessUrl("/home.html")    // 默认登录成功访问页面
                .failureHandler((HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) -> {
                    logger.error("登录失败");
                    response.sendRedirect("/login.html?message=error");
                })
                .and()
                .csrf().disable().exceptionHandling()   // 关闭跨站请求伪造保护
                .accessDeniedHandler(getAccessDeniedHandler());  // 用户权限不足时处理
    }

    /**
     * 权限不足时处理
     * @return
     */
    @Bean
    AccessDeniedHandler getAccessDeniedHandler() {
        return (httpServletRequest, httpServletResponse, e) -> {
            httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
            httpServletResponse.sendRedirect("/403");
//            httpServletResponse.setCharacterEncoding("UTF-8");
//            httpServletResponse.setContentType("text/html;charset=utf-8");
//            PrintWriter pw = httpServletResponse.getWriter();
//            pw.write("<h1>权限不足，请联系管理员</h1>");
//            pw.flush();
//            pw.close();
        };
    }

    public static void main(String[] args) {
        System.out.println(new BCryptPasswordEncoder().encode("c21f969b5f03d33d43e04f8f136e7682"));
    }
}
