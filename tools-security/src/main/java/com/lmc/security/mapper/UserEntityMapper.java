package com.lmc.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lmc.security.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-11-01 22:52
 * @version: 1.0
 */
@Mapper
public interface UserEntityMapper extends BaseMapper<UserEntity> {
}
