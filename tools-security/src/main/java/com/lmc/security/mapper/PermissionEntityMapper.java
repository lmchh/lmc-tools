package com.lmc.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lmc.security.entity.PermissionEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-11-02 23:07
 * @version: 1.0
 */
@Mapper
public interface PermissionEntityMapper extends BaseMapper<PermissionEntity> {

    List<PermissionEntity> getAuthoritiesByUserId(Integer userId);

}
