package com.lmc.security;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lmc
 * @Description: TODO
 * @Create 2021-10-23 15:40
 * @version: 1.0
 */
@SpringBootApplication
@MapperScan("com.lmc.security.mapper")
public class SecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityApplication.class, args);
    }

}
